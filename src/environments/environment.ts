// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  dev: true,
  env: 'dev',
  RAZORPAY_API_KEY: "rzp_live_1K7ay6wAMdzD5y",
  url: 'http://recognizer.multitvsolution.com',
  // baseUrl: 'http://35.154.240.36:7000',
  // baseUrl: 'https://www.tbbmedialive.com',
  baseUrl: 'https://goapi.multitvsolution.com:7000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
