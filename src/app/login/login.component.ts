import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token;
  msg;
  loginForm: FormGroup
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  blockLogin = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');
    this.loginForm = this.formBuilder.group({
      // email: ['', [Validators.email, Validators.pattern("[^ @]*@[^ @]*"), emailDomainValidator]],
      email: ['', [Validators.required]],

    });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else {
      this.potrait = false;
    }

    this.onload();
  }
  showme = false;
  clickhere() {
    this.showme = !this.showme!
    let abc: any = document.getElementById('notAllowLogin');
    if (this.showme) {
      abc.style.transition = '.5s';
      abc.style.transform = 'translateY(75%)';
    } else {
      abc.style.transition = '.5s';
      abc.style.top = '-2%';
    }
  }
  onload() {
    function getTimeRemaining(endtime) {
      let date: any = new Date();
      const total: any = Date.parse(endtime) - Date.parse(date);
      const seconds = Math.floor((total / 1000) % 60);
      const minutes = Math.floor((total / 1000 / 60) % 60);
      const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
      const days:any = Math.floor(total / (1000 * 60 * 60 * 24));
      if (total <= 0) {
        // console.log('testin', total, seconds, minutes, hours, days);
        const endClock = document.getElementById('clockdiv');
        endClock.querySelector('.days').innerHTML='00';
        endClock.querySelector('.hours').innerHTML='00';
        endClock.querySelector('.minutes').innerHTML='00';
        endClock.querySelector('.seconds').innerHTML='00';
      }else{
        return {
          total,
          days,
          hours,
          minutes,
          seconds
        };
      }
      
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const daysSpan: any = clock.querySelector('.days');
      const hoursSpan = clock.querySelector('.hours');
      const minutesSpan = clock.querySelector('.minutes');
      const secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        const t = getTimeRemaining(endtime);
        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }

    //const deadline = new Date(Date.parse(new Date()) + 29 * 24* 60 * 60 * 1000);
    const deadline = new Date('December 17, 2020 17:00:00');
    initializeClock('clockdiv', deadline);
  }

  loggedIn() {
    const user = {
      email: this.loginForm.get('email').value,
      event_id: 64,
      role_id: 1
    };
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }

    };
    let currentTime: any = new Date().getTime();
    let eventTime: any = new Date('December 17, 2020 11:00:00').getTime();
    // console.log(currentTime, eventTime);
    if (currentTime >= eventTime) {
      this.blockLogin = false;
      if (this.loginForm.valid) {
        this.auth.loginMethod(user).subscribe((res: any) => {
          if (res.code === 1) {
            if (isMobile.iOS()) {
              this.videoPlay = false;
              this.router.navigateByUrl('/lobby');
            }
            this.videoPlay = true;
            localStorage.setItem('virtual', JSON.stringify(res.result));
            this.videoPlay = true;
            let vid: any = document.getElementById('myVideo');
            vid.play();
            // this.router.navigateByUrl('/lobby');

            if (window.innerHeight > window.innerWidth) {
              this.potrait = true;
            } else {
              this.potrait = false;
            }
          } else {
            this.msg = 'Invalid Login';
            this.videoPlay = false;
            this.loginForm.reset();
          }
        }, (err: any) => {
          this.videoPlay = false;
          console.log('error', err)
        });
      }
    }
    else {
      this.blockLogin = true;
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else {
      this.potrait = false;
    }
  }

  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/lobby');
    // let welcomeAuido: any = document.getElementById('myAudio');
    // welcomeAuido.play();
  }
}
