import { Component, OnInit, HostListener } from '@angular/core';
import { fadeAnimation } from './shared/animation/fade.animation';
declare let ga: Function;

import { Router,RouterEvent, ActivationEnd,NavigationStart, ActivationStart, ActivatedRoute, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { FetchDataService } from './services/fetch-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit {
  title = 'virtualEvent';
  landscape = true;
  router: string;
  tokensave:any;
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  constructor(private _router: Router, private route: ActivatedRoute,private _fd: FetchDataService) {
    this.router = _router.url; 
    // this._router.events.subscribe((event:RouterEvent)=>{
    //   switch(true){
    //     case event instanceof NavigationStart:{
    //       break;
    //     }
    //     case event instanceof NavigationEnd:
    //     case event instanceof NavigationCancel:
    //     case event instanceof NavigationError: {
    //       let urlEvent:any;
    //       urlEvent=event;
    //       let url:any=urlEvent.url;
    //       url=url.replace('/', '');
    //       let spliturl:any=url.split('/');
    //       this._router.navigate([spliturl[0]+'/login']);
    //       localStorage.setItem('selectedTheme', spliturl[0]);
    //       localStorage.setItem('event_id', spliturl[1]);
    //     }
    //   }
    // });
  }

  ngOnInit(){
    this._router.events.subscribe((event:RouterEvent)=> {
      // console.log('events', event.url)
      // if(event.url==='/login?token=123'){
      //   this._router.navigate(['/tOneLogin']);
      // }
      if(event instanceof NavigationStart){
        //alert(event.url);
        ga('set', 'page', event.url);
        ga('send', 'pageview');
      }
     });

    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else{
      this.landscape = true;
    }
  }
}
