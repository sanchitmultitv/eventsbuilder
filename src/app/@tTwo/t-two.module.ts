import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TTwoRoutingModule } from './t-two-routing.component';
import { TTwoComponent } from './t-two.component';


@NgModule({
  declarations: [TTwoComponent],
  imports: [
    CommonModule,
    TTwoRoutingModule
  ]
})
export class TTwoModule { }
