import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoB2bComponent } from './t-two-b2b.component';


const routes: Routes = [
  {path:'', component:TTwoB2bComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoB2bRoutingModule { }
