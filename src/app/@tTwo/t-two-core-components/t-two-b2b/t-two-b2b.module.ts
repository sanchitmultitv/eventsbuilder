import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoB2bRoutingModule } from './t-two-b2b-routing.module';
import { TTwoB2bComponent } from './t-two-b2b.component';


@NgModule({
  declarations: [TTwoB2bComponent],
  imports: [
    CommonModule,
    TTwoB2bRoutingModule
  ]
})
export class TTwoB2bModule { }
