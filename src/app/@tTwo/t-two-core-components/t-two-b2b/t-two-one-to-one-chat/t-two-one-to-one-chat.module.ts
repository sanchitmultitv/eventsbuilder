import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoOneToOneChatRoutingModule } from './t-two-one-to-one-chat-routing.module';
import { TTwoOneToOneChatComponent } from './t-two-one-to-one-chat.component';


@NgModule({
  declarations: [TTwoOneToOneChatComponent],
  imports: [
    CommonModule,
    TTwoOneToOneChatRoutingModule
  ]
})
export class TTwoOneToOneChatModule { }
