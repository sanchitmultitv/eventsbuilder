import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoAgendaRoutingModule } from './t-two-agenda-routing.module';
import { TTwoAgendaComponent } from './t-two-agenda.component';


@NgModule({
  declarations: [TTwoAgendaComponent],
  imports: [
    CommonModule,
    TTwoAgendaRoutingModule
  ]
})
export class TTwoAgendaModule { }
