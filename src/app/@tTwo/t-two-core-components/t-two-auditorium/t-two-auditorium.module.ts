import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoAuditoriumRoutingModule } from './t-two-auditorium-routing.module';
import { TTwoAuditoriumComponent } from './t-two-auditorium.component';
import { TTwoAudiComponent } from './components/t-two-audi/t-two-audi.component';
import { TTwoDynamicAudiComponent } from './components/t-two-dynamic-audi/t-two-dynamic-audi.component';


@NgModule({
  declarations: [TTwoAuditoriumComponent, TTwoAudiComponent, TTwoDynamicAudiComponent],
  imports: [
    CommonModule,
    TTwoAuditoriumRoutingModule
  ]
})
export class TTwoAuditoriumModule { }
