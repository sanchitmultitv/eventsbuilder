import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import * as _ from 'underscore';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-t-two-dynamic-audi',
  templateUrl: './t-two-dynamic-audi.component.html',
  styleUrls: ['./t-two-dynamic-audi.component.scss']
})
export class TTwoDynamicAudiComponent implements OnInit, AfterViewInit {
    videoEnd = false;
    textMessage = new FormControl('');
    videoPlayer = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';
    player: any;
    auditorium;
    bgImage;
    imgid;
    constructor(private _fd: FetchDataService, private route:ActivatedRoute,) { }
  
    ngOnInit(): void {
      this.route.params.subscribe((params:Params) => {
        this.bgImage='assets/sampleimages/audi'+params['id']+'.png';
        this.imgid=params['id'];
        console.log('image', this.imgid, window.innerWidth);
      })    
    }
    
    ngAfterViewInit(){
      this.setPlayer();
      this.playVideo();
    }
    playerStl:any = {
      position: 'absolute',
    };
    setPlayer(){
      let id = this.imgid;
      id = parseInt(id);
      console.log()      
      switch (id) {
        case 1:
          this.playerStl.top='20.1%';
          this.playerStl.left='25.7%';
          this.playerStl.height=window.innerWidth/4.32;
          this.playerStl.width='48.5%';
          this.playerStl.divisible=window.innerWidth / this.playerStl.height;
          break;
        case 2:
          this.playerStl.top='41.3%';
          this.playerStl.left='40.5%';
          this.playerStl.height=window.innerWidth/11;
          this.playerStl.width='18%';
          this.playerStl.divisible=window.innerWidth / this.playerStl.height;
          break;
      }      
    }
    playVideo() {
      var playerElement = document.getElementById("player-wrapper");
      this.player = new Clappr.Player({
        parentId: 'player-wrapper',
        source: this.videoPlayer,
        poster: '../../../../../assets/asian_img/asianPoster.jpeg',
        // height: 390,
        height: this.playerStl.height,
        maxBufferLength: 30,
        width: '100%',
        autoPlay: true,
        hideMediaControl: false,
        hideVolumeBar: true,
        hideSeekBar: true,
        persistConfig: false,
        // chromeless: true,
        // mute: true,
        visibilityEnableIcon: false,
        disableErrorScreen: true,
        playback: {
          playInline: true,
          // recycleVideo: Clappr.Browser.isMobile,
          recycleVideo: true
        },
      });
      this.player.attachTo(playerElement);
      // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
      // if (window.innerWidth <= 572) {
      //   this.player.play();
      //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
      // } else {
      //   this.player.play();
      //   // $('#player-wrapper video').css({ 'object-fit': 'fill'});
      //   this.player.resize({ width: '100%', height: window.innerHeight - 41 });
      //   $('#player-wrapper .container').find('.player-poster').css('background-size', '84%');
      //   // $('#player-wrapper > div > .media-control').css('background', 'red');
      // }
      this.player.resize({ width: '100%', height: this.playerStl.height});
    }
    fullscreen() {
      this.player.core.mediaControl.toggleFullscreen();
    }
    @HostListener('window:resize', ['$event']) onResize(event) {
      this.player.resize({ width: '100%', height: window.innerWidth / this.playerStl.divisible});
    }
  }
  
