import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-t-two-audi',
  templateUrl: './t-two-audi.component.html',
  styleUrls: ['./t-two-audi.component.scss']
})
export class TTwoAudiComponent implements OnInit {
  eid=localStorage.getItem('eid');
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  gotoAudi(id){
    this.router.navigate([`/theme2/${this.eid}/ttwolayout/auditorium/d-audi/${id}`]);
  }
}
