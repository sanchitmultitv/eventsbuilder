import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoAudiComponent } from './components/t-two-audi/t-two-audi.component';
import { TTwoDynamicAudiComponent } from './components/t-two-dynamic-audi/t-two-dynamic-audi.component';
import { TTwoAuditoriumComponent } from './t-two-auditorium.component';


const routes: Routes = [
  {
    path:'', 
    component:TTwoAuditoriumComponent,
    children:[
      {
        path:'',
        redirectTo:'audi',
        pathMatch:'full'
      },
      {
        path:'audi',
        component: TTwoAudiComponent
      },
      {
        path:'d-audi/:id',
        component:TTwoDynamicAudiComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoAuditoriumRoutingModule { }
