import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoLobbyComponent } from './t-two-lobby.component';


const routes: Routes = [
  {path:'', component:TTwoLobbyComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoLobbyRoutingModule { }
