import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoLobbyRoutingModule } from './t-two-lobby-routing.module';
import { TTwoLobbyComponent } from './t-two-lobby.component';


@NgModule({
  declarations: [TTwoLobbyComponent],
  imports: [
    CommonModule,
    TTwoLobbyRoutingModule
  ]
})
export class TTwoLobbyModule { }
