import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoExhibitionComponent } from './t-two-exhibition.component';


const routes: Routes = [
  {path:'', component:TTwoExhibitionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoExhibitionRoutingModule { }
