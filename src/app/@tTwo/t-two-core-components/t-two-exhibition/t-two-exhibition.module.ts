import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoExhibitionRoutingModule } from './t-two-exhibition-routing.module';
import { TTwoExhibitionComponent } from './t-two-exhibition.component';


@NgModule({
  declarations: [TTwoExhibitionComponent],
  imports: [
    CommonModule,
    TTwoExhibitionRoutingModule
  ]
})
export class TTwoExhibitionModule { }
