import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoPhotoboothRoutingModule } from './t-two-photobooth-routing.module';
import { TTwoPhotoboothComponent } from './t-two-photobooth.component';


@NgModule({
  declarations: [TTwoPhotoboothComponent],
  imports: [
    CommonModule,
    TTwoPhotoboothRoutingModule
  ]
})
export class TTwoPhotoboothModule { }
