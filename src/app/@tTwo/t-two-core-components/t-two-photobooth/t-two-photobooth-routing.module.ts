import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoPhotoboothComponent } from './t-two-photobooth.component';


const routes: Routes = [
  {path:'', component:TTwoPhotoboothComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoPhotoboothRoutingModule { }
