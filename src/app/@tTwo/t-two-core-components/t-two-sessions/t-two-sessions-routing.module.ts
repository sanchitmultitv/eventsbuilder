import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoSessionsComponent } from './t-two-sessions.component';


const routes: Routes = [
  {path:'', component:TTwoSessionsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoSessionsRoutingModule { }
