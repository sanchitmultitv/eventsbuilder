import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoSessionsRoutingModule } from './t-two-sessions-routing.module';
import { TTwoSessionsComponent } from './t-two-sessions.component';


@NgModule({
  declarations: [TTwoSessionsComponent],
  imports: [
    CommonModule,
    TTwoSessionsRoutingModule
  ]
})
export class TTwoSessionsModule { }
