import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { fadeAnimation } from 'src/app/shared/animation/fade.animation';
import { TTwoMenuItems, TTwoMenuRightItems } from './model/t-two.menu';

@Component({
  selector: 'app-t-two-layout',
  templateUrl: './t-two-layout.component.html',
  styleUrls: ['./t-two-layout.component.scss'],
  animations:[fadeAnimation]
})
export class TTwoLayoutComponent implements OnInit {
  landscape=true;
  leftMenuItems=TTwoMenuItems;
  rightMenuItems=TTwoMenuRightItems;
  sidenavs=[
    {name:'left',menu:this.leftMenuItems},
    {name:'right',menu:this.rightMenuItems},
  ];
  constructor(public router: Router) { }

  ngOnInit(): void {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  logout(){

  }
 
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else{
      this.landscape = true;
    }
  } 
  stepUpAnalytics(action){}

}
