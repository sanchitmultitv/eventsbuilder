import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoLayoutComponent } from './t-two-layout.component';
import { TTwoOuterAnimationComponent } from './t-two-outer-animation/t-two-outer-animation.component';


const routes: Routes = [
  {
    path:'',
    component:TTwoLayoutComponent,
    children:[
      { path:'outer', component:TTwoOuterAnimationComponent },
      { path:'', redirectTo:'lobby', pathMatch: 'prefix' },
      { path:'lobby', loadChildren:()=>import('../t-two-core-components/t-two-lobby/t-two-lobby.module').then(m=>m.TTwoLobbyModule) },
      { path:'auditorium', loadChildren:()=>import('../t-two-core-components/t-two-auditorium/t-two-auditorium.module').then(m=>m.TTwoAuditoriumModule) },
      { path:'photobooth', loadChildren:()=>import('../t-two-core-components/t-two-photobooth/t-two-photobooth.module').then(m=>m.TTwoPhotoboothModule) },
      { path:'sessions', loadChildren:()=>import('../t-two-core-components/t-two-sessions/t-two-sessions.module').then(m=>m.TTwoSessionsModule) },
      { path:'exhibition', loadChildren:()=>import('../t-two-core-components/t-two-exhibition/t-two-exhibition.module').then(m=>m.TTwoExhibitionModule) },
      { path:'b2b', loadChildren:()=>import('../t-two-core-components/t-two-b2b/t-two-b2b.module').then(m=>m.TTwoB2bModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoLayoutRoutingModule { }
