import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoLayoutRoutingModule } from './t-two-layout-routing.module';
import { TTwoLayoutComponent } from './t-two-layout.component';
import { TTwoSliderComponent } from './t-two-slider/t-two-slider.component';
import { TTwoOuterAnimationComponent } from './t-two-outer-animation/t-two-outer-animation.component';
import { TTwoAgendaComponent } from './modals/t-two-agenda/t-two-agenda.component';
import { TTwoShowModalComponent } from './modals/t-two-show-modal/t-two-show-modal.component';


@NgModule({
  declarations: [TTwoLayoutComponent, TTwoSliderComponent, TTwoOuterAnimationComponent, TTwoAgendaComponent, TTwoShowModalComponent],
  imports: [
    CommonModule,
    TTwoLayoutRoutingModule
  ]
})
export class TTwoLayoutModule { }
