var id:any = localStorage.getItem('eid');
var menu=[
    {
        name:'reception', 
        icon:'../../../assets/icons/Reception.png',
        path:'/theme2/'+id+'/ttwolayout/lobby'
    },
    {
        name:'agenda', 
        icon:'../../../assets/icons/Agenda.png',
        data_target:".t_one_agendaModal"
    },
    {
        name:'auditorium', 
        icon:'../../../assets/icons/Auditorium.png',
        path:"/theme2/"+id+"/ttwolayout/auditorium"
    },
    {
        name:'photobooth', 
        icon:'../../../assets/icons/camera.png',
        path:"/theme2/"+id+"/ttwolayout/photobooth"
    },
    {
        name:'exhibition', 
        icon:'../../../assets/icons/Exhibition.png',
        path:"/theme2/"+id+"/ttwolayout/exhibition"
    },
    {
        name:'B2B', 
        icon:'../../../assets/icons/Exhibition.png',
        path:"/theme2/"+id+"/ttwolayout/photobooth"
    },
];
// menu[2]['path']="/theme2/ttwolayout/auditorium/1";
export const TTwoMenuItems = menu;

var rMenu=[
    {
        name:'reception', 
        icon:'../../../assets/icons/Reception.png',
        path:'/theme2/'+id+'/ttwolayout/lobby'
    },
    {
        name:'auditorium', 
        icon:'../../../assets/icons/Auditorium.png',
        path:"/theme2/"+id+"/ttwolayout/auditorium"
    },
    {
        name:'photobooth', 
        icon:'../../../assets/icons/camera.png',
        path:"/theme2/"+id+"/ttwolayout/photobooth"
    },
    {
        name:'exhibition', 
        icon:'../../../assets/icons/Exhibition.png',
        path:"/theme2/"+id+"/ttwolayout/exhibition"
    },
    {
        name:'B2B', 
        icon:'../../../assets/icons/B2B.png',
        path:"/theme2/"+id+"/ttwolayout/b2b"
    },
];
export const TTwoMenuRightItems = rMenu;
