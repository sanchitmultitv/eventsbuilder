import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-t-two-outer-animation',
  templateUrl: './t-two-outer-animation.component.html',
  styleUrls: ['./t-two-outer-animation.component.scss']
})
export class TTwoOuterAnimationComponent implements OnInit, AfterViewInit {
  e_id;
  constructor(private router:Router, private ar:ActivatedRoute) { }

  ngOnInit(): void {
    this.e_id=localStorage.getItem('eid');  
  }
  ngAfterViewInit(){
    let vid:any = document.getElementById('myVideo');
    vid.play();
  }
  endOuterVideo(){
    this.router.navigateByUrl('/theme2/'+this.e_id+'/ttwolayout/lobby');
  }
}
