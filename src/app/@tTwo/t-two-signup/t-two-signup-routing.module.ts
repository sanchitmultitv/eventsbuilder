import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoSignupComponent } from './t-two-signup.component';


const routes: Routes = [
  {path:'', component:TTwoSignupComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoSignupRoutingModule { }
