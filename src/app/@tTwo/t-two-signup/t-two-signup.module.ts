import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoSignupRoutingModule } from './t-two-signup-routing.module';
import { TTwoSignupComponent } from './t-two-signup.component';


@NgModule({
  declarations: [TTwoSignupComponent],
  imports: [
    CommonModule,
    TTwoSignupRoutingModule
  ]
})
export class TTwoSignupModule { }
