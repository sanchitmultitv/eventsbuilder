import { AfterViewInit, Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { ActiveLoginComponent } from './components/active-login/active-login.component';

@Component({
  selector: 'app-t-two-type2-login',
  templateUrl: './t-two-type2-login.component.html',
  styleUrls: ['./t-two-type2-login.component.scss']
})
export class TTwoType2LoginComponent implements OnInit, AfterViewInit {
  submitted = false;
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute, private vcRef: ViewContainerRef, private cResolver: ComponentFactoryResolver) { }
  ngOnInit() {
    console.log('height', window.scrollBy());
    this.loginForm = this.formBuilder.group({});
    this.getform();
  }
  ngAfterViewInit(){
  }

  dynamicFormArray = [];
  pageInfo:any = {};
  param;
  getform() {
    this.route.parent.parent.params.subscribe((params:Params) =>{
      this.http.get(`/assets/@test/${params.eid}/sample.json`).subscribe((res: any) => {
        this.submitted = false;
        this.loginForm = this.formBuilder.group({});
        this.param = params.eid;
        // this.loadactivecomponent();
        this.dynamicFormArray = res.result;
        this.pageInfo = res['loginPage'];
        const root:any = document.querySelector(':root');
        root.style.setProperty('--lbg', 'url('+this.pageInfo['image']+')');
        root.style.setProperty('--vbg', 'url(/assets/sampleimages/mob_bg.png)');
        this.dynamicFormArray.forEach((element:any) => {
          if (element.type === 'email') {
            this.loginForm.addControl(element.label, new FormControl('', [Validators.required, Validators.email]));
          }
          if ((element.type === 'text')||(element.type === 'number')) {
            this.loginForm.addControl(element.label, new FormControl('', Validators.required));
          }
          if (element.type === 'select') {
            let option:any = element.options;
            this.loginForm.addControl(element.label, new FormControl(''));
            this.loginForm.controls[element.label].setValue(option[0]);
          }
          if (element.type === 'radio') {
            this.loginForm.addControl(element.label, new FormControl(''));
          }
          if (element.type === 'checkbox') {
            // hobbies.addControl('names', new FormArray([]));
            // (hobbies.get('names') as FormArray).push(new FormControl('pooka', Validators.required));
            this.loginForm.addControl(element.label, new FormArray([], minSelectedCheckboxes(1)));
            for (let i = 0; i < element.options.length; i++) {
              (this.loginForm.get(element.label) as FormArray).push(new FormControl(false));              
            }
          }
        });
        console.log('form', this.loginForm.controls);
        // this.loginForm.reset();
      });
    });
  }
  onSubmit() {
    this.loadactivecomponent();
    console.log('testing', this.loginForm.value);
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
  }
  async loadactivecomponent() {
    this.vcRef.clear();
    if(this.param==='134'){
      const { LazytestComponent } = await import('./components/lazytest.component');
      this.vcRef.createComponent(this.cResolver.resolveComponentFactory(LazytestComponent));
    }
    if(this.param!=='134'){
      const { ActiveLoginComponent } = await import('./components/active-login/active-login.component');
      this.vcRef.createComponent(this.cResolver.resolveComponentFactory(ActiveLoginComponent));
    }
  }
}


function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
