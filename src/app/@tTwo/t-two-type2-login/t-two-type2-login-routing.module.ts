import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoType2LoginComponent } from './t-two-type2-login.component';


const routes: Routes = [
  {path:'', component:TTwoType2LoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoType2LoginRoutingModule { }
