import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoType2LoginRoutingModule } from './t-two-type2-login-routing.module';
import { TTwoType2LoginComponent } from './t-two-type2-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazytestComponent } from './components/lazytest.component';
import { ActiveLoginComponent } from './components/active-login/active-login.component';


@NgModule({
  declarations: [TTwoType2LoginComponent, LazytestComponent, ActiveLoginComponent],
  imports: [
    CommonModule,
    TTwoType2LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents:[LazytestComponent, ActiveLoginComponent]
})
export class TTwoType2LoginModule { }
