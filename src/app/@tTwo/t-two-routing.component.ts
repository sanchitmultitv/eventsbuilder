import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoComponent } from './t-two.component';


const routes: Routes = [
  {path:'', 
  component:TTwoComponent,
  children: [
    { path: '', redirectTo: 'login', pathMatch: 'prefix' },
    // {path: 'login', loadChildren: ()=> import('./t-two-login/t-two-login.module').then(m => m.TTwoLoginModule)},
    {path: 'login', loadChildren: ()=> import('./t-two-type2-login/t-two-type2-login.module').then(m => m.TTwoType2LoginModule)},
    {path: 'signup', loadChildren: ()=> import('./t-two-signup/t-two-signup.module').then(m => m.TTwoSignupModule)},
    {path: 'ttwolayout', loadChildren: ()=> import('../@tTwo/t-two-layout/t-two-layout.module').then(m => m.TTwoLayoutModule)},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoRoutingModule { }
