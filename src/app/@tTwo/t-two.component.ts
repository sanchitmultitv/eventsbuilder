import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-t-two',
  templateUrl: './t-two.component.html',
  styleUrls: ['./t-two.component.scss']
})
export class TTwoComponent implements OnInit {
  id;
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:Params)=>{
      this.id=params.eid;
      localStorage.setItem('eid', params.eid);
    });
  }

}

