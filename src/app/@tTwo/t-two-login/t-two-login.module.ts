import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TTwoLoginRoutingModule } from './t-two-login-routing.module';
import { TTwoLoginComponent } from './t-two-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TTwoLoginComponent],
  imports: [
    CommonModule,
    TTwoLoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TTwoLoginModule { }
