import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FetchDataService } from '../../services/fetch-data.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-t-two-login',
  templateUrl: './t-two-login.component.html',
  styleUrls: ['./t-two-login.component.scss']
})
export class TTwoLoginComponent implements OnInit, AfterViewInit {  
    token;
    msg;
    loginForm: FormGroup;
    videoPlay = false;
    potrait = false;
    bgImage;
    e_id;
    constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder: FormBuilder, private ar:ActivatedRoute) { }
  
    ngOnInit(): void {
      localStorage.setItem('user_guide', 'start');
      this.loginForm = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', Validators.required],
        business_vertical: ['', Validators.required],
        country: ['', Validators.required],
      });
      this.getbg();
    }
    ngAfterViewInit(){
      
    }
    getbg(){
      // change from api response
      this.ar.parent.parent.params.subscribe((par:Params)=>{
        let eid=par.eid;
        this.e_id = par.eid;
        if(eid==='134'){
          this.bgImage='assets/sampleimages/bg.jpg';
        }else{
          this.bgImage='assets/sampleimages/loginbg.jpg';
        }
      });
    }

    loggedIn() {
      const user = {
        name:  this.loginForm.get('name').value,
        // password: this.loginForm.get('password').value,
        event_id: 134,
        role_id: 1
      };
      var isMobile = {
        Android: function () {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function () {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
  
      };
      const formData=new FormData();
      formData.append('name',this.loginForm.get('name').value);
      formData.append('email',this.loginForm.get('email').value);
      formData.append('company', this.loginForm.get('business_vertical').value);
      formData.append('country', this.loginForm.get('country').value);      
      if(this.loginForm.valid){
        this.auth.loggedInMethod(formData).subscribe((res: any) => {
          if (res.code === 1) {
            if (isMobile.iOS()) {
              this.videoPlay = false;
              this.router.navigateByUrl('/theme2/'+this.e_id+'/ttwolayout/lobby');
            }
            // this.videoPlay = true;
            this.router.navigateByUrl('/theme2/'+this.e_id+'/ttwolayout/outer');
            localStorage.setItem('virtual', JSON.stringify(res.result));   
            if (window.innerHeight > window.innerWidth) {
              this.potrait = true;
            } else {
              this.potrait = false;
            }
          } else {
            this.msg = 'Invalid Login';
            this.videoPlay = false;
            this.loginForm.reset();
          }
        }, (err: any) => {
          this.videoPlay = false;
          console.log('error', err)
        });
      }
    }
    @HostListener('window:resize', ['$event']) onResize(event) {
      if (window.innerHeight > window.innerWidth) {
        this.potrait = true;
      } else {
        this.potrait = false;
      }
    }
  
  }
  
