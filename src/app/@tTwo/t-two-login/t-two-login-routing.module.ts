import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTwoLoginComponent } from './t-two-login.component';


const routes: Routes = [
  {path:'', component:TTwoLoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTwoLoginRoutingModule { }
