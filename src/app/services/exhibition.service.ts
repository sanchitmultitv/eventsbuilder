import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
var baseUrl = 'https://goapi.multitvsolution.com:7002';
@Injectable({
  providedIn: 'root'
})
export class ExhibitionService {
  token = localStorage.getItem('token') || '';
  hallsSubject = new BehaviorSubject<any>(null);
  totalHalls = this.hallsSubject.asObservable();
  currExbhitionSubject = new BehaviorSubject<any>(null);
  currExbhitions = this.currExbhitionSubject.asObservable();
  currExb=[];
  constructor(private http:HttpClient) { 
    this.getHallsList();
  }
  getHallsList(){
    return this.http.get(`${baseUrl}/builderapi/v1/get/exhibition_hall/token/${this.token}`).pipe(map((res:any)=>{
      this.hallsSubject.next(res);
      return this.totalHalls;
    })).subscribe();
  }
  
}
