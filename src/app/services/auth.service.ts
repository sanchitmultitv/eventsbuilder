import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from './apiConfig/api.constants';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;
  signup = ApiConstants.signup;
  login = ApiConstants.login;
  constructor(private http: HttpClient) { }

  register(token,data: any){
    // const token = 64;
    // return this.http.post(`${this.baseUrl}/${this.signup}/${token}`, data);
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/auth/registration/token/${token}`,data)
  }

  // loginMethod(loginObj:any){
  //   return this.http.get(`${this.baseUrl}/${this.login}/event_id/${loginObj.event_id}/email/${loginObj.email}`);
  // }
  loginOTP(token,number){
      return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/send/otp/token/${token}/mobile/${number}`);
    }
  loginMethod(loginObj:any){
     const event_id=134;
     return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/auto/login/event_id/${event_id}`,loginObj);
  }
  loginwithOTP(token,mobile,otp,name){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/verify/otp/token/${token}/mobile/${mobile}/otp/${otp}/name/${name}`);
 }
  loggedInMethod(loginObj:any){
    //  const event_id=134;
    const event_id=localStorage.getItem('eid');
     return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/auto/login/event_id/${event_id}`,loginObj);
  }
  logout(user_id):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.logout}/user_id/${user_id}`);
  }
  acmeLoggedinMethod(loginObj:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.acmeLogin}/event_id/${loginObj.event_id}/email/${loginObj.email}/registration_number/${loginObj.registration_number}`);
  }
}
