import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  // getAttendees(event_id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  // }
  getAttendees(token): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/allattendees/token/${token}`);
  }
  search(token,query): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/allattendees/token/${token}/name/${query}`);
  }
  emaillogin(token,email){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/auth/login/token/${token}/email/${email}`);
  }
  getLoginDetails(token){ 
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/eventBuilder/get/token/${token}`);
  }
  getmenuToken(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/menu/get/token/${token}`);
  }
  getExhibitionToken(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/exhibitions/token/${token}`);
  }
  getStallsData(token,data){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/stall/detail/token/${token}/id/${data}`);
  }
  // totalTimeSlots(ex_id,date):Observable<any> {
  //   return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/calender.php?exhibition_id=${ex_id}&date=${date}`) 
  // }
  totalTimeSlots(ex_id,stall_id,date):Observable<any> {
    return this.http.get(`  https://virtualapi.multitvsolution.com/upload_photo/calender-builder.php?exhibition_id=${ex_id}&stall_id=${stall_id}&date=${date}
    `) 
  }
  schdeuleAcall(dates):Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.scheduleaCall}`,dates);
  }
  scheduleCallGet(locid,dates):Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/user/schedule/call/token/${locid}`,dates);
  }
  getBrochureList(token,data){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/brochure/list/token/${token}/stall/${data}`);
  }
  getProductList(token,data){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/product/list/token/${token}/stall/${data}`);
  }
  cardDrop(token,data :any){
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/card/drop/post/token/${token}`, data)
  }
  getAudis(token,data){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/auditorium/token/${token}/id/${data}`);
  }
  multiAudi(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/auditorium/token/${token}`);
  }
  getLounge(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/lounge/token/${token}`)
  }
  getAttendeesbyName(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  // getOne2oneChatList(token): Observable<any> {
  // return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/allattendees/token/${token}`)
  // }
  enterTochatList(token,receiver_id, sender_id): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/one2one/chat/get/token/${token}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  // postOne2oneChat(comment: any): Observable<any> {
  //   return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  // }
  postOne2oneChat(token,comment: any): Observable<any> {
       return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/one2one/chat/post/token/${token}
       `, comment);
     }
  getRating(): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/rating/master/list/event_id/64`);
  }
  getFeedback(): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/feedback/master/list/event_id/64`);
  }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/rating/post`, rating);
  }
  getExhibition(title): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}/${title}`);
  }
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }
  heartbeat(token,params): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/analytics/user/heartbeat/token/${token}`, params);
  }

  askQuestions(id,name, value,exhibit_id, exhibit_name,token): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('sender_id', id);
    params = params.set('sender_name', name);
    params = params.set('receiver_id', exhibit_id);
    params = params.set('receiver_name', exhibit_name);
    params = params.set('msg', value);
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/one2one/chat/post/token/${token}
    `, params);
  }
  helpdesk(id,value,token): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    // params = params.set('event_id', '64');
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/ask/question/post/token/${token}`, params);
  }
  gethelpdeskanswers(uid,token): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/asked/question/answer/get/user_id/${uid}/token/${token}`)
  }
  askLiveQuestions(id, value, audi_id): Observable<any> {
    let token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('event_id', '64');
    params = params.set('audi_id', audi_id);
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/ask/live/question/token/${token}`, params);
    // return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(token,uid,exhibit_id): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/one2one/chat/get/token/${token}/receiver_id/${uid}/sender_id/${exhibit_id}`)
  }
  // Liveanswers(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  // }
  // getPollList(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  getPollList(token): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/poll/token/${token}`)
  }
  // getPollListTwo(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListThree(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListFour(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  postPoll(id, data, value, token): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', data);
    params = params.set('answer', value);
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/poll/answer/post/token/${token}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }

  // getQuizList(event_id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}`);
  // }
  getQuizList(token): Observable<any> {
    return this.http.get(`  https://goapi.multitvsolution.com:7002/builderapi/v1/get/quiz/token/${token}`);
  }
  // postSubmitQuiz(quiz): Observable<any> {
  //   return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  // }
  postSubmitQuiz(token,quiz): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/quiz/answer/post/token/${token}`, quiz);
    // return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/quiz/answer/post/json/token/${token}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    let token = localStorage.getItem('token');
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/quiz/summary/token/${token}/user_id/${user_id}`)
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/upload/pic`, pic);
  }
  uploadsample(pic): Observable<any> {
    // return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
    return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
    // return this.http.post(`https://virtualapi.multitvsolution.com/mayanmar_upload_photo/uploadblob.php`, pic);
  }
  groupchating(data): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/mongo/groupchat/room/${data}`);
  }
  groupchatingtwo(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_17`);
  }
  groupchatingthree(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_18`);
  }
  groupchatingfour(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_19`);
  }
  submitWheelScore(wheel: any) {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id) {
    return this.http.get(`${this.baseUrl}/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc: any) {
    return this.http.post(`${this.baseUrl}/${ApiConstants.postBriefcase}`, doc);
  }
  analyticsPost(analytics: any): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/analytics/user/history/add`, analytics);
  }
  analyticsHistory(token,analytics: any): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/analytics/user/history/add/token/${token}`, analytics);
  }
  activeAudi(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.activeInactive}`)
  }
  disconnectCall(disc:any){
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/call/update`, disc);
  }
  postGroupChat(token, chat:any){
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/groupchat/post/token/${token}`, chat);
  }
  multiExhibition(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/exhibitions/token/${token}`);
  }
  getSponsorList(token) {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/sponsors/token/${token}`);
  }
  getTicketTypeListing(token){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/ticket/list/token/${token}`);
  }
  // multiExhibition(token){
  //   return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/exhibitions/token/${token}`);
  // }
  createPaymentOrderForTicket({ticket_id,user_id,amount}){
    return this.http.get(`https://virtualapi.multitvsolution.com/virtual_event_payment/create_order_beb.php?ticket_id=${ticket_id}&user_id=${user_id}&amount=${amount}`);
  }
  updateSuccessPaymentForTicket({razorpay_payment_id,razorpay_order_id,razorpay_signature,user_id}){
    return this.http.get(`https://virtualapi.multitvsolution.com/virtual_event_payment/verify/complete_subscription_beb.php?razorpay_payment_id=${razorpay_payment_id}&razorpay_order_id=${razorpay_order_id}&razorpay_signature=${razorpay_signature}&user_id=${user_id}`);
  }
  getAllScheduledAppointments({exhibition_id,stall_id,date}) {
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/calender-builder.php?exhibition_id=${exhibition_id}&stall_id=${stall_id}&date=${date}`)
  }
  getScheduledAppointmentsForUser() {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/schedule/call/list/user_id/166038`)
  }
  getAttendeesList(offset:number) {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/139/offset/${offset}`)
  }
  totalTimeSlotsForAttendees(ex_id,date):Observable<any> {
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/calender.php?exhibition_id=${ex_id}&date=${date}`); 
  }
  getReinvestAttendees(event_id, core_sector, core_business){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/core_business/${core_business}/country/india`);
  }
  ReinvestAttendeesCoreSectorSearch(event_id, core_sector, name){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/country/india/name/${name}`);
  }
  ReinvestAttendeesCoreBusinessSearch(event_id, core_business, name){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_business/${core_business}/country/india/name/${name}`);
  }
  ReinvestAttendees(event_id, core_sector, core_business, name){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/core_business/${core_business}/country/india/name/${name}`);
  }
  getReinvestAttendeesCoreSector(event_id, core_sector){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/country/india`);
  }
  getReinvestAttendeesCoreBusiness(event_id, core_business){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/attendees/event_id/${event_id}/core_business/${core_business}/country/india`);
  }
  schdeuleAcallattende(postObject:any,token:string):Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/attendee/schedule/call/token/${token}`,postObject);
  }
  getAttendees_ListForAttendeesModal(token,offset=undefined) {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/allattendees/token/${token}`);
  }
  getSessionList(token) {
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/get/breakout/session/token/${token}`);
  }
  postClap(clap){
    let token = localStorage.getItem('token');
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/clap/token/${token}`, clap);
  }
  postReaction(react){
    let token = localStorage.getItem('token');
    return this.http.post(`https://goapi.multitvsolution.com:7002/builderapi/v1/user/reaction/add/token/${token}`, react);
  }
}
