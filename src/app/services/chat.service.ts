import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
token:any;
  constructor(private socket: Socket) {
    this.token = localStorage.getItem('token')
   }
  
  public socketConnection(token){
    this.socket.emit('bigboy', {
      token: token
    });
  }
  public getSocketConnectedMessages = (token)=>{
    return Observable.create((observer)=>{
      this.socket.on(token, (message) => {
        observer.next(message);
      });    
    });
  }
  //   public sendMessage() {
  //     this.socket.emit('start_quiz');
  // }
  public getconnect(token) {
    this.socket.emit('bigboy', {
      token: token,
    });
  }
  public getconnectAuditorium(token) {
    this.socket.emit('bigboyget', {
      token: token,
    });
  }
  public getIframeConnection = () => {
    return Observable.create((observer) => {
      this.socket.on('5fb4b1b07f9d8', (message) => {
        observer.next(message);
      });
    });
  }
  public getb2bMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('toujeo-60', (message) => {
        // console.log('tesing', message);
        observer.next(message);
      });
    });
  }
  public getStatusMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('toujeo-61', (message) => {
        // console.log('tesing', message);
        observer.next(message);
      });
    });
  }
  public getMessagesQnA = () => {
    return Observable.create((observer) => {
      this.socket.on('toujeo-64', (message) => {
        console.log('msg', message);
        observer.next(message);
      });
    });
  }
  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on(this.token, (message) => {

        observer.next(message);
      });
    });
  }

  public sendMessage(message, usr, video_id) {
    //console.log('servicce',message);
    this.socket.emit('sendchat', message);
    this.socket.emit('notify_user', usr, video_id);
    // console.log(usr, message, video_id);
  }
  public addUser(usr, video_id) {
    this.socket.emit('adduser', {
      username: usr,
     // avatarId: avatar_id,
      video_id: video_id,
      video_name: 'demo test',
    });
  }


  public receiveMessages = (room) => {
    return Observable.create((observer) => {
      this.socket.on('updatechat', (user_name, roomId, chat_data) => {
        let meessageObject = { chat_data, user_name, room, roomId };
        if (user_name === 'SERVER') {
          console.log(roomId + '=====' + user_name );
          // observer.next(chat_data);
        } else if (localStorage.getItem('username') == user_name) {
          console.log(roomId + '=====' + user_name );
          observer.next(meessageObject);
        } else {
          console.log(roomId + '=====' + user_name );
          observer.next(meessageObject);
        }
        //console.log('sevrc',message);
      });
    });
  };
  getgroupMessage() {
    return Observable.create((obs) => {
      this.socket.on('toujeo-52', (msg) => {
        obs.next(msg);
        console.log('msg', msg);
      });
    });
  }

  public getGroupChatMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('updatechat', (user_name, avatarId, chat_data) => {
       let meessageObject = {chat_data,user_name};
       if (user_name === 'SERVER') {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
         // observer.next(chat_data);
        } else if (localStorage.getItem('username') == user_name) {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
          observer.next(meessageObject);
        } else {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
          observer.next(meessageObject);
        }
        //console.log('sevrc',message);
      });
    });
  };
  disconnect(){
    this.socket.on('disconnect', ()=>{
      console.log('disconnn')

    }); 
  }
}
