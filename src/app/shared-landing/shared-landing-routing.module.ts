import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedLandingComponent } from './shared-landing/shared-landing.component';


const routes: Routes = [
  {path:'', component:SharedLandingComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedLandingRoutingModule { }
