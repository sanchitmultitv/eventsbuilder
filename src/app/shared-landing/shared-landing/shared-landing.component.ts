import { Compiler, Component, OnInit } from '@angular/core';
import { Route, Router, RouterEvent } from '@angular/router';
import { TOneModule } from 'src/app/@tOne/t-one.module';

@Component({
  selector: 'app-shared-landing',
  templateUrl: './shared-landing.component.html',
  styleUrls: ['./shared-landing.component.scss']
})
export class SharedLandingComponent implements OnInit {
  selectThemes=['theme1', 'theme2'];
  constructor(private router: Router, private compiler: Compiler) { }

  ngOnInit(): void {
    this.router.events.subscribe((events: RouterEvent)=>{
      if(events.url=== '/virtual/1'){
        this.router.navigate([`${this.selectThemes[0]}`]);
      }
      if(events.url=== '/virtual/2'){
        this.router.navigate([`${this.selectThemes[1]}`]);
      }
    });
  }

  loadMore() {
    console.log('*********', this.router.config);
    const route: Route= {
      path: 'theme1',
      // loadChildren: () => {
      //   return ngModuleFactory;
      // }
      loadChildren:()=>import('../../@tOne/t-one.module').then(m=>m.TOneModule)
    };
    this.router.resetConfig([route, ...this.router.config]);
    console.log('*routes', this.router.config);
    // this.compiler.compileModuleAsync(TOneModule).then(ngModuleFactory => {
    //   const route: Route= {
    //     path: 'theme1',
    //     // loadChildren: () => {
    //     //   return ngModuleFactory;
    //     // }
    //     loadChildren:()=>import('../../@tOne/t-one.module').then(m=>m.TOneModule)
    //   };
    //   this.router.resetConfig([route, ...this.router.config]);
    //   console.log('*routes', this.router.config);

    // });
  }

  ngAfterViewChecked() {
    // this.loadMore()
  }
}
