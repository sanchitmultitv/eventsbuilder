import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedLandingRoutingModule } from './shared-landing-routing.module';
import { SharedLandingComponent } from './shared-landing/shared-landing.component';


@NgModule({
  declarations: [SharedLandingComponent],
  imports: [
    CommonModule,
    SharedLandingRoutingModule
  ]
})
export class SharedLandingModule { }
