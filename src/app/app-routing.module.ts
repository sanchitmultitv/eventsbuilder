import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'theme1', pathMatch: 'prefix' },
  { path: 'virtual', loadChildren: () => import('./shared-landing/shared-landing.module').then(m => m.SharedLandingModule) },
  { path: 'theme1', loadChildren: () => import('./@tOne/t-one.module').then(m => m.TOneModule) },
  { path: 'theme2/:eid', loadChildren: () => import('./@tTwo/t-two.module').then(m => m.TTwoModule) },
  // { path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  // { path: '', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {'useHash': true})],
  exports: [RouterModule]
})
export class AppRoutingModule implements OnInit { 
constructor(private route:ActivatedRoute){}
  ngOnInit(){
    console.log('activated route', this.route);
  }
}
