import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneRoutingModule } from './t-one-routing.component';
import { TOneComponent } from './t-one.component';
import { TOneMultiAuditoriumComponent } from './t-one-core-components/t-one-multi-auditorium/t-one-multi-auditorium.component';
import { TOneMultiExhibitionComponent } from './t-one-core-components/t-one-multi-exhibition/t-one-multi-exhibition.component';
import { TOneSponsorsComponent } from './t-one-sponsors/t-one-sponsors.component';
import { TOneMultiAuditoriumCardComponent } from './t-one-core-components/t-one-multi-auditorium/t-one-multi-auditorium-card/t-one-multi-auditorium-card.component';
// import { TOneFeedbackComponent } from './t-one-shared/components/t-one-feedback/t-one-feedback.component';


@NgModule({
  declarations: [TOneComponent, TOneMultiAuditoriumComponent,TOneSponsorsComponent, TOneMultiExhibitionComponent,TOneMultiAuditoriumCardComponent],
  imports: [
    CommonModule,
    TOneRoutingModule
  ]
})
export class TOneModule { }
