import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TOneFeedbackComponent } from './t-one-feedback.component';



@NgModule({
  declarations: [TOneFeedbackComponent],
  imports: [
    CommonModule
  ],
  exports:[CommonModule, TOneFeedbackComponent]
})
export class TOneFeedbackModule { }
