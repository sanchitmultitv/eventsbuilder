import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;

@Component({
  selector: 'app-t-one-feedback',
  templateUrl: './t-one-feedback.component.html',
  styleUrls: ['./t-one-feedback.component.scss']
})
export class TOneFeedbackComponent implements OnInit, AfterViewInit {
  getData: any=[];
  feedback_url:any;
  token:any
  trustedUrl:any
  constructor(private sanitizer: DomSanitizer,private _fd : FetchDataService,private router: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
  }
  ngAfterViewInit(){
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.feedback_url = this.getData.result.helpdesk[0].feedback_url
      this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.feedback_url);
      // console.log(this.feedback_url)
    });
  } 
  closePopup(){
    $('.t_one_feedbackModal').modal('hide');
  }
}
