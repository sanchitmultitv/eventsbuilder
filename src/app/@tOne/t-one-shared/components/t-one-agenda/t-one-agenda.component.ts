import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { any } from 'underscore';
import { DomSanitizer } from '@angular/platform-browser';

declare var $:any;
@Component({
  selector: 'app-t-one-agenda',
  templateUrl: './t-one-agenda.component.html',
  styleUrls: ['./t-one-agenda.component.scss']
})
export class TOneAgendaComponent implements OnInit {
  getData: any=[];
  main_banner:any;
  main_banner2:any;
  massTimingsHtml:any;
  token:any
  trustedUrl:any

  constructor(private sanitizer: DomSanitizer,private _fd : FetchDataService,private router: Router) {


   }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.main_banner = this.getData.result.others[0].agenda_img
      this.main_banner2 = this.getData.result.others[0].agenda_pdf
      // alert(this.main_banner2)
      // this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.main_banner2);
      // alert(this.main_banner2)
    });
  }
  // ngAfterViewInit(){
  //   this._fd.getmenuToken(this.token).subscribe(res => {
  //     this.getData = res;
  //     this.main_banner = this.getData.result.others[0].agenda_img
  //     this.main_banner2 = this.getData.result.others[0].agenda_pdf
  //     // this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.main_banner2);
  //     alert(this.main_banner2)
  //   });
  // }
  
  closePopup(){
    $('.t_one_agendaModal').modal('hide');
  }
}
