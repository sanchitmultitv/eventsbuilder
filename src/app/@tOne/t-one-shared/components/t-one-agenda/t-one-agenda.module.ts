import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TOneAgendaComponent } from './t-one-agenda.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [TOneAgendaComponent],
  imports: [
    CommonModule,
    PdfViewerModule
  ],
  exports:[CommonModule, TOneAgendaComponent]
})
export class TOneAgendaModule { }
