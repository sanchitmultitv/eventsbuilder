import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneType2LoginComponent } from './t-one-type2-login.component';


const routes: Routes = [
  {path:'', component:TOneType2LoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneType2LoginRoutingModule { }
