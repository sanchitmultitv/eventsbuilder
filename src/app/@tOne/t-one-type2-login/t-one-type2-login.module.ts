import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneType2LoginRoutingModule } from './t-one-type2-login-routing.module';
import { TOneType2LoginComponent } from './t-one-type2-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TOneType2LoginComponent],
  imports: [
    CommonModule,
    TOneType2LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneType2LoginModule { }
