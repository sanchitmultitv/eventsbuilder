import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FetchDataService } from '../../services/fetch-data.service';
import { AuthService } from '../../services/auth.service';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-t-one-type2-login',
  templateUrl: './t-one-type2-login.component.html',
  styleUrls: ['./t-one-type2-login.component.scss']
})
export class TOneType2LoginComponent implements OnInit {
  token;
  msg; main_data; main_logo;
  form; form2
  signin_color: any = [];
  loginForm: FormGroup;
  loginForm2: FormGroup;
  loginForm3: FormGroup;

  button_color
  need_reg;
  otpverify = false;
  videoPlay = false;
  potrait = false;
  sentotp = true;
  main_logo_width: any;
  logo_position: any;
  form_position: any;
  left_logo_position: any;
  top_logo_position: any;
  left_form_position: any;
  top_form_position: any;
  form_width: any;
  constructor(private route: ActivatedRoute, private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    // this.token = this.route.snapshot.queryParamMap.get('token');
    // alert(this.token)
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
    });
    this._fd.getLoginDetails(this.token).subscribe((res: any) => {
      if (res.code === 1) {
        localStorage.setItem('login_data', JSON.stringify(res.result));
        localStorage.setItem('token', this.token);
      } else {
        window.location.reload()
      }
    });

    this.main_data = JSON.parse(localStorage.getItem('login_data'))[0].config.login_data.image
    // alert(this.main_data)
    this.main_logo = JSON.parse(localStorage.getItem('login_data'))[0].logo
    this.main_logo_width = JSON.parse(localStorage.getItem('login_data'))[0].logo_width
    this.signin_color = JSON.parse(localStorage.getItem('login_data'))[0].config.general.font_color
    this.button_color = JSON.parse(localStorage.getItem('login_data'))[0].config.general.button_color
    this.form = JSON.parse(localStorage.getItem('login_data'))[0].email_login
    this.need_reg = JSON.parse(localStorage.getItem('login_data'))[0].need_reg
    this.form2 = JSON.parse(localStorage.getItem('login_data'))[0].otp_login
    this.form_width = JSON.parse(localStorage.getItem('login_data'))[0].config.login_data.form_width
    this.logo_position = JSON.parse(localStorage.getItem('login_data'))[0].config.login_data.logo_position
    this.form_position = JSON.parse(localStorage.getItem('login_data'))[0].config.login_data.form_position
    
    // alert(this.logo_position)
    // alert(this.form_position)
    if (this.logo_position != null || undefined || '') {
      let check = this.logo_position.split('x')
      this.left_logo_position = check[0]
      this.top_logo_position = check[1]
    } 
    if (this.form_position != null || undefined || ''){
      let check1 = this.form_position.split('x')
      this.left_form_position = check1[0]
      this.top_form_position = check1[1]
    }

      // let check = this.logo_position.split('x')
      // this.left_logo_position = check[0]
      // this.top_logo_position = check[1]
      // let check1 = this.form_position.split('x')
      // this.left_form_position = check1[0]
      // this.top_form_position = check1[1]

    // alert(this.form);
    localStorage.setItem('user_guide', 'start');
    this.loginForm = this.formBuilder.group({
      // name: ['', Validators.required],
      // email: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('^[6-9][0-9]{9}$')])],
      // business_vertical: ['', Validators.required],
      // country: ['', Validators.required],
    });
    this.loginForm2 = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)])],
    });
    this.loginForm3 = this.formBuilder.group({
      name: ['', Validators.required],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('^[6-9][0-9]{9}$')])],
      otp: ['', Validators.required],
    });

    this.loginForm2.controls.email.valueChanges.pipe(distinctUntilChanged()).subscribe(e => {
      this.loginForm2.controls.email.setValue(e.trim().toLowerCase());
    });
  }


  loggedIn(loginType) {
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }

    };
    if (loginType == 'emaillogin') {

      const formData = new FormData();
      formData.append('email', this.loginForm2.get('email').value);

      if (this.loginForm2.valid) {
        this._fd.emaillogin(this.token, this.loginForm2.get('email').value).subscribe((res: any) => {
          if (res.code === 1) {
            localStorage.setItem('userId',res?.result?.id)
            if (isMobile.iOS()) {
              this.videoPlay = false;
              this.router.navigateByUrl('/lobby');
            }
            this.videoPlay = true;
            localStorage.setItem('virtual', JSON.stringify(res.result));
            this.videoPlay = true;
            this.router.navigateByUrl('/theme1/tonelayout/outer');
            if (window.innerHeight > window.innerWidth) {
              this.potrait = true;
            } else {
              this.potrait = false;
            }
          } else {
            this.msg = 'Invalid Login';
            this.videoPlay = false;
            this.loginForm2.reset();
          }
        }, (err: any) => {
          this.videoPlay = false;
          console.log('error', err)
        });
      }
    }
     if (loginType == 'otplogin') {
       if(this.loginForm.invalid) {
         return;
       }
       this.loginForm3.controls.mobile.setValue(this.loginForm.get('mobile').value);
       
      const formData = new FormData();
      // formData.append('name', this.loginForm.get('name').value);
      // formData.append('email', this.loginForm.get('email').value);
      formData.append('mobile', this.loginForm.get('mobile').value);
      // formData.append('company', this.loginForm.get('business_vertical').value);
      // formData.append('category', this.loginForm.get('country').value);
      // alert(a)

      // if (this.loginForm.valid) {
        this.auth.loginOTP(this.token, this.loginForm.get('mobile').value).subscribe((res: any) => {
          if (res.code === 1) {
          this.sentotp = false;
          this.otpverify = true;
            // if (isMobile.iOS()) {
            //   this.videoPlay = false;
            //   this.router.navigateByUrl('/lobby');
            // }
            // this.videoPlay = true;
            // localStorage.setItem('virtual', JSON.stringify(res.result));
            // this.videoPlay = true;
            // this.router.navigateByUrl('/theme1/tonelayout/outer');
            // if (window.innerHeight > window.innerWidth) {
            //   this.potrait = true;
            // } else {
            //   this.potrait = false;
            // }
          } else {
            this.msg = 'Invalid Login';
            this.videoPlay = false;
            this.loginForm.reset();
          }
        }, (err: any) => {
          this.videoPlay = false;
          console.log('error', err)
        });
      // }
    }

  }
  otpLogin() {
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }

    };

    const formData = new FormData();
    formData.append('name', this.loginForm3.get('name').value);
    formData.append('mobile', this.loginForm3.get('mobile').value);
    // formData.append('company', this.loginForm3.get('business_vertical').value);
    formData.append('otp', this.loginForm3.get('otp').value);
    if (this.loginForm3.valid) {
      this.auth.loginwithOTP(this.token,this.loginForm3.get('mobile').value,this.loginForm3.get('otp').value,this.loginForm3.get('name').value).subscribe((res: any) => {
        if (res.code === 1) {
          localStorage.setItem('userId',res.result.id);
          // alert('poprt')
          if (isMobile.iOS()) {
            this.videoPlay = false;
            this.router.navigateByUrl('/lobby');
          }
          this.videoPlay = true;
          localStorage.setItem('virtual', JSON.stringify(res.result));
          this.videoPlay = true;
          this.router.navigateByUrl('/theme1/tonelayout/outer');
          if (window.innerHeight > window.innerWidth) {
            this.potrait = true;
          } else {
            this.potrait = false;
          }
        } else {
          this.msg = 'Invalid Login';
          this.videoPlay = false;
          this.loginForm3.reset();
        }
      }, (err: any) => {
        this.videoPlay = false;
        console.log('error', err)
      });
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else {
      this.potrait = false;
    }
  }

  getFormValidation(formName: 'loginForm' | 'loginForm2' | 'loginForm3', formControlName: string, formDirSubmitted: boolean) {
    if (formName === 'loginForm2') {
      if (formControlName === 'email') {
        if (this.loginForm2.controls?.email?.errors?.pattern) {
          return "Email is invalid";
        } else if (this.loginForm2.controls?.email?.errors?.required && formDirSubmitted) {
          return "Email is required";
        }
      }
    } else if (formName === 'loginForm') {
      if (formControlName === 'mobile') {
        if (this.loginForm.controls?.mobile?.errors?.pattern) {
          return "Mobile is invalid";
        } else if (this.loginForm.controls?.mobile?.errors?.required && formDirSubmitted) {
          return "Mobile is required";
        }
      }
    } else if (formName === 'loginForm3') {
      if (formControlName === 'mobile') {
        if (this.loginForm3.controls?.mobile?.errors?.pattern) {
          return "Mobile is invalid";
        } else if (this.loginForm3.controls?.mobile?.errors?.required && formDirSubmitted) {
          return "Mobile is required";
        }
      }
    }
  }
}


