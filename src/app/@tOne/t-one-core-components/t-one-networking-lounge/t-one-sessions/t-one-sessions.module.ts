import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TOneSessionCardComponent } from './t-one-session-card/t-one-session-card.component';
import { TOneSessionsComponent } from './t-one-sessions.component';
import { TOneSessionsRoutingModule } from './t-one-sessions-routing.module';
import { TOneJoinSessionsComponent } from './t-one-join-sessions/t-one-join-sessions.component';

@NgModule({
  declarations: [TOneSessionCardComponent,TOneSessionsComponent, TOneJoinSessionsComponent],
  imports: [
    CommonModule,
    TOneSessionsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneSessionsModule { }
