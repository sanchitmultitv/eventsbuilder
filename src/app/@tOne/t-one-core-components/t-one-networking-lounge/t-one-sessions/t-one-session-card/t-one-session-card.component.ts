import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-t-one-session-card',
  templateUrl: './t-one-session-card.component.html',
  styleUrls: ['./t-one-session-card.component.scss']
})
export class TOneSessionCardComponent implements OnInit {
  @Input('cardDetail') cardDetail: any;
  @Input('color') color: string;
  mouseData;
  linkToOpen: string;
  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  mousein(){
    let x:any = document.querySelector('.tooltiptext');
    x.style.visibility='visible';
  }
  mouseleft(){
    let x:any = document.querySelector('.tooltiptext');
    x.style.visibility='hidden';
  }
  openSession(channelId:string) {
    this.router.navigate(['/theme1/tonelayout/sessions/',channelId]);
    // const linkToOpen = `https://virtualapi.multitvsolution.com/meetingvirtual/build/home.html?channel_name=${channelName}&role=publisher`;
    // var iframe =
    //   '<iframe width="100%" height="550"  src="' +
    //   linkToOpen +
    //   '" frameborder="0" allowfullscreen=""></iframe>';
    // $('#maincall').html(iframe);
    // return false;
  }
}
