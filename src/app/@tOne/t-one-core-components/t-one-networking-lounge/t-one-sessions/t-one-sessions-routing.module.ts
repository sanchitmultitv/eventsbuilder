import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TOneJoinSessionsComponent } from "./t-one-join-sessions/t-one-join-sessions.component";
import { TOneSessionsComponent } from "./t-one-sessions.component";

const routes: Routes = [
  {
    path: "",
    component: TOneSessionsComponent,
  },
  {
    path: ":sessionId",
    component: TOneJoinSessionsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TOneSessionsRoutingModule {}
