import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
declare var $: any;
@Component({
  selector: "app-t-one-join-sessions",
  template: `<div
    class="row"
    style="height: 89vh;width: 84%;position: relative;top: 34px;left: 136px;"
    id="maincall"
  >
    <div
      class="col-md-12 col-sm-12 col-xs-12"
      style="text-align: center;"
      id="loader"
    >
      <i class="fa fa-spinner fa-spin" style="font-size:44px"></i>
    </div>
  </div>`,
})
export class TOneJoinSessionsComponent implements OnInit {
  sessionId: string;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.sessionId = params.get("sessionId");
    });

    const linkToOpen = `https://virtualapi.multitvsolution.com/meetingvirtual/build/home.html?channel_name=${this.sessionId}&role=audience`;
    $("#maincall").html(
      `<iframe src="${linkToOpen}" width="100%" height="100%" frameborder="0" allowfullscreen=""></iframe>`
    );
  }
}
