import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-sessions',
  templateUrl: './t-one-sessions.component.html',
  styleUrls: ['./t-one-sessions.component.scss']
})
export class TOneSessionsComponent implements OnInit {
  sessionList:any[]=[];
  token:any;

  constructor(private _fd : FetchDataService,private router: Router) { }

  ngOnInit(): void {
    // this.token = "60f161dd8786d"||localStorage.getItem('token');
    this.token = localStorage.getItem('token');
    this.getSessionListingData();
  }
  getSessionListingData() {
    // this.token = '60a5030450af6';
    this._fd.getSessionList(this.token).subscribe((sessions: any) => {
      this.sessionList = sessions?.result;
      console.log(this.sessionList,'this.sessionList')
    });
  }
  getColor(index) {
    return index%2==0?"red":"blue";
  }
  openSession(channelName:string) {
    const linkToOpen = `https://virtualapi.multitvsolution.com/meetingvirtual/build/home.html?channel_name=${channelName}&role=publisher`;
    window.open(linkToOpen, '_self');
  }

}
