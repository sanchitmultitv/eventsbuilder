import { AfterViewInit, Component, HostListener, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ToastrService } from 'ngx-toastr';
declare var $:any;
@Component({
  selector: 'app-one-to-one-chat',
  templateUrl: './one-to-one-chat.component.html',
  styleUrls: ['./one-to-one-chat.component.scss']
})
export class OneToOneChatComponent implements OnInit, OnDestroy, AfterViewInit {
  storage:any=JSON.parse(localStorage.getItem('virtual'));
  videoEnd = false;
  liveMsg = false;
  ChatMsg = false;
  senderName;
  resetLINKS: any;
  sender_id: any;
  sender_name;
  searchChatList = [];
  receiver_id: any;
  chatMessage = [];
  allChatList = [];
  timer;
  messageList: any=[];
  roomName= 'milaap';
  datas:any;
  commentsList = [];
  commentsListing = [];
  textMessage = new FormControl('');
  type = new FormControl('');
  interval;
  typ = 'normal';
  oneToOneChatList = [];
  allChatIndex:any;
  chatUser: any;
  receiver_name;
  token:any;
  data: any;
  isUserSelected=false;

  constructor(private router: Router, private chatservice:ChatService,private _fd: FetchDataService, private chat: ChatService, private route: ActivatedRoute, private toastr: ToastrService, private ngzone: NgZone) { }

  ngOnInit(): void {
    $('.t_one_chat_one_to_one').modal('show');
    this.token = localStorage.getItem('token')
    // this.searchElement();
    this.allAttendees();
    this.initChatSection();
  }

  ngAfterViewInit(){
    

  }
  initChatSection(){
    const connection=this.token;
    this.chatservice.socketConnection(connection);
    this.chatservice.getSocketConnectedMessages(connection).subscribe(data=>{
      if(typeof data == 'string'){
        if(data.includes('one2one')){
          let splitData = data.split('_');          
          // for (let i = 1; i < splitData.length; i++) {
          //   if(this.storage.id != splitData[i]){
          //     let index: number = this.allChatList.findIndex(res=>res.id==splitData[i]);
          //     this.ngzone.runOutsideAngular(() => {
          //       this.allChatIndex = index;
          //       setTimeout(() => {
          //         this.selectedChat(this.allChatList[index],index);
                  
          //       }, 500);
          //     });
          //   }
          // }          
          this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
            this.chatMessage = res.result;
          });
        }
      }
    });
  }
  selectedChat(chat, ind) {
    // alert(chat)
    // alert(ind)
    this.isUserSelected = true;
    this.allChatIndex = ind;
    let event_id = 168;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.id;
    this.receiver_name = chat.name;
    this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }
  allAttendees(){
    this._fd.getAttendees(this.token).subscribe(res => {
      this.allChatList = res.result;
    });
  }
  searchElement(query) {
    // let event_id = 168;
    this._fd.search(this.token,query).subscribe(res => {
      this.allChatList = res.result;
    });
  }
  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null) {
      this._fd.postOne2oneChat(this.token,formData).subscribe(data => {
        this.textMessage.reset();
        // this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
        //   this.chatMessage = res.result;
        // });
      });
    }
    setTimeout(() => {
      $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
    }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }
  closeChat(){
    this.router.navigate(['/theme1/tonelayout/lounge']);
    $('.t_one_chat_one_to_one').modal('hide');
  }
  ngOnDestroy(){
    console.log('destroyed');
  }

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let one_to_one:any=document.getElementById('chat_one_to_one_Modal');
    if(targetElement===one_to_one){
      this.router.navigate(['/theme1/tonelayout/lounge']);
    }
  }
}
