import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneNetworkingLoungeRoutingModule } from './t-one-networking-lounge-routing.module';
import { TOneNetworkingLoungeComponent } from './t-one-networking-lounge.component';
import { OneToOneChatComponent } from './one-to-one-chat/one-to-one-chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupChatComponent } from './group-chat/group-chat.component';


@NgModule({
  declarations: [TOneNetworkingLoungeComponent, OneToOneChatComponent, GroupChatComponent],
  imports: [
    CommonModule,
    TOneNetworkingLoungeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneNetworkingLoungeModule { }
