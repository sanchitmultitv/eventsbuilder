import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GroupChatComponent } from "./group-chat/group-chat.component";
import { OneToOneChatComponent } from "./one-to-one-chat/one-to-one-chat.component";
import { TOneNetworkingLoungeComponent } from "./t-one-networking-lounge.component";

const routes: Routes = [
  {
    path: "",
    component: TOneNetworkingLoungeComponent,
    children: [
      {
        path: "chat",
        component: OneToOneChatComponent,
      },
      {
        path: "groupchat",
        component: GroupChatComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TOneNetworkingLoungeRoutingModule {}
