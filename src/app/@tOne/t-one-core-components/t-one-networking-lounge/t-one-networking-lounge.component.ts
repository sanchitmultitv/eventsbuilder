import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-t-one-networking-lounge',
  templateUrl: './t-one-networking-lounge.component.html',
  styleUrls: ['./t-one-networking-lounge.component.scss']
})
export class TOneNetworkingLoungeComponent implements OnInit {
  counter =0;
  getData: any=[];
  main_banner:any;
  photo:any;download_click:any
  main_audis:any=[];groupchat:any
  top_groupchat: any;
  left_groupchat: any;
  etc: any=[];
  onetoone :any;groupchat_position:any
  information_video_position: any;
  left_video: any;
  top_video: any;
  top_onetoone: any;
  left_onetoone: any;
  onetoone_position: any;
  top_photobooth: any;
  left_photobooth: any;
  photo_booth_position: any;
  token:any;
  sessionList:any[]=[];
  socialLinks=[
    {icon:'facebook', url: ''},
    {icon:'twitter', url: ''},
    {icon:'youtube', url: ''},
    {icon:'instagram', url: ''},
  ];
  
  constructor(private _fd : FetchDataService,private router: Router, private chatservice: ChatService, private ad:ActivatedRoute) { }

  ngOnInit(): void {
    const user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.token = localStorage.getItem('token');
    this.counter = this.counter +1;
    const connection=this.token;
    this.chatservice.socketConnection(connection);
    this.chatservice.getSocketConnectedMessages(connection).subscribe(data=>{
      if(typeof data == 'string'){
        if(data.includes('one2one')){
          this.counter = this.counter+1;
          if(this.counter >= 1){  
            let splitData = data.split('_');            
            let url = '/theme1/tonelayout/lounge/chat';
            let splitUrl=this.router.url.split('?')[0];
            if(url == splitUrl){
              return
            } else{   
              for (let i = 1; i < splitData.length; i++) {
                if(user_id == splitData[i]){
                  this.router.navigate([url], {queryParams:{count: this.counter}});
                }              
              }           
            }          
            console.log(splitUrl, url)
            this.counter = 0;
          }
        }
      }
    });
  }
  ngAfterViewInit(){
    this._fd.getLounge(this.token).subscribe(res => {
      this.getData = res;
      // console.log()
       this.main_banner = this.getData.result[0].lounge_img
       this.photo = this.getData.result[0].photo_booth
       this.onetoone = this.getData.result[0].onetoone
       this.groupchat = this.getData.result[0].groupchat
      this.download_click = this.getData.result[0].information_video
      this.information_video_position = this.getData.result[0].information_video_position
      let check1 = this.information_video_position.split('x')
      this.left_video = check1[0]
      this.top_video = check1[1]
      this.groupchat_position = this.getData.result[0].groupchat_position
      let check = this.groupchat_position.split('x');
      this.left_groupchat = check[0];
      this.top_groupchat = check[1];
      this.onetoone_position = this.getData.result[0].onetoone_position
      let check2 = this.onetoone_position.split('x')
      this.left_onetoone = check2[0]
      this.top_onetoone = check2[1]
      this.photo_booth_position = this.getData.result[0].photo_booth_position
      let check3 = this.photo_booth_position.split('x')
      this.left_photobooth = check3[0]
      this.top_photobooth = check3[1]
      this.main_audis = this.getData.result.audilist; 
      //  this.etc =Object.values(this.main_audis)
      //  console.log(this.main_audis)
      // this.top = this.getData.result.pos_top
      // this.left = this.getData.result.pos_left
      let dataObj = this.getData.result[0];
      for (const key in dataObj) {
        this.socialLinks.map(res=>{
          if(res.icon == key){
            res.url = dataObj[key];
          }
        })
      }
      
    });
  }
  showrequired(request){
    // $(`.${request}`).modal('show');
    if(typeof request == 'string'){
      if(request.includes('groupchat')){
        this.router.navigate(['/theme1/tonelayout/lounge/groupchat']);
      }
    }

  }
  closeSessionListingModal() {
    $('.sessionListingModal').modal('hide');
  }
  getSessionListingData() {
    // this.token = '60a5030450af6';
    this._fd.getSessionList(this.token).subscribe((sessions: any) => {
      this.sessionList = sessions?.result;
    });
  }
  openSession(channelName:string) {
    const linkToOpen = `https://virtualapi.multitvsolution.com/meetingvirtual/build/home.html?channel_name=${channelName}&role=publisher`;
    window.open(linkToOpen, '_self');
  }
} 
