import { Component, ElementRef, EventEmitter, OnInit, Output, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { coreBusinessArr, coreSelectorArr } from 'src/app/shared/attendeesforsearch';
declare var $: any;
@Component({
  selector: 'app-t-one-attendees',
  templateUrl: './t-one-attendees.component.html',
  styleUrls: ['./t-one-attendees.component.scss']
})
export class TOneAttendeesComponent implements OnInit {
  @Output('toggleModal') toggleModal: EventEmitter<any> = new EventEmitter();
  token;
  attendees = [];
  model: NgbDateStruct;
  date: { year: number, month: number };
  newTimeSlots: any = [];
  timeVal;
  attendee_To:any;
  names: string;
  offset = 50;
  coreSelectorArr: any = coreSelectorArr;
  coreBusinessArr: any = coreBusinessArr;
  core_sector = new FormControl('');
  core_business = new FormControl('');
  coreSectorvalue = null;
  coreBusinessvalue = null;
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  constructor(private _fd: FetchDataService, private calendar: NgbCalendar, private renderer: Renderer2, private toastr: ToastrService) { }

  form = new FormGroup({
    core_sector: new FormControl(''),
    core_business: new FormControl('')
  });

  ngOnInit(): void {
    this.token = JSON.parse(localStorage.getItem('token'));
    this.model = this.calendar.getToday();
    let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
    this.core_sector.setValue(this.coreSelectorArr[0].name);
    this.core_business.setValue(this.coreBusinessArr[0].name);

    this.form.reset();
    this._fd.getAttendees_ListForAttendeesModal(this.token, this.offset).subscribe((res: any) => {
      this.attendees = res.result;
    });
    this._fd.totalTimeSlotsForAttendees(this.attendee_To, myDate).subscribe(res => {
      this.newTimeSlots = res.result;
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    } else {
      this._fd.getReinvestAttendees(139, this.form.value.core_sector, this.form.value.core_business).subscribe((res: any) => {
        this.attendees = res.result;
        this.form.reset();
      });
    }

  }
  getUserid(id) {
    this.attendee_To = id;
  }
  closeCall() {
    $('.scheduleCallmodal').modal('hide');
  }

  loadmore() {
    this.offset = this.offset + 50;
    
    this._fd.getAttendees_ListForAttendeesModal(this.token, this.offset).subscribe((res: any) => {
      let arr: any = res.result;
      for (let i = 0; i < arr.length; i++) {
        console.log(arr[i]);
        this.attendees.push(arr[i]);
      }
    });
  }

  filterData(query) {
    let event_id = 139;
    if (query !== '') {
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
        this._fd.getAttendeesbyName(event_id, query).subscribe(res => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
        this._fd.ReinvestAttendeesCoreSectorSearch(event_id, this.coreSectorvalue, query).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
        this._fd.ReinvestAttendeesCoreBusinessSearch(event_id, this.coreBusinessvalue, query).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
        this._fd.ReinvestAttendees(event_id, this.coreSectorvalue, this.coreBusinessvalue, query).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
    } else {
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
        this._fd.getAttendees_ListForAttendeesModal(this.token, this.offset).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
        this._fd.getReinvestAttendeesCoreSector(event_id, this.coreSectorvalue).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
        this._fd.getReinvestAttendeesCoreBusiness(event_id, this.coreBusinessvalue).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
        this._fd.getReinvestAttendees(event_id, this.coreSectorvalue, this.coreBusinessvalue).subscribe((res: any) => {
          this.attendees = res.result;
        });
      }
    }
  }
  onSelectChange(value, core) {
    let event_id = 139;
    if (core === 'csect') {
      this.coreSectorvalue = this.core_sector.value;
    }
    if (core === 'cbusi') {
      this.coreBusinessvalue = this.core_business.value;
    }
    console.log('vlaue', value, core, this.coreSectorvalue, this.coreBusinessvalue);
    if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
      this._fd.getReinvestAttendeesCoreSector(event_id, this.coreSectorvalue).subscribe((res: any) => {
        this.attendees = res.result;
      });
    }
    if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
      this._fd.getReinvestAttendeesCoreBusiness(event_id, this.coreBusinessvalue).subscribe((res: any) => {
        this.attendees = res.result;
      });
    }
    if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
      this._fd.getReinvestAttendees(event_id, this.coreSectorvalue, this.coreBusinessvalue).subscribe((res: any) => {
        this.attendees = res.result;
      });
    }
    this.names = '';
  }
  clearAll() {
    this.coreSectorvalue = null;
    this.coreBusinessvalue = null;
    this.ngOnInit();
  }
  closePopup() {
    $('.attendeesModal').modal('hide');
  }
  getTime(event: any, valClass, time) {
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    this.renderer.addClass(event.target, valClass);
  }
  confirm() {
    const userId=localStorage.getItem('userId');
    if(userId) {

    }else {
      console.log('userId is not present in storage, cannot call the attendee :::: ')
    }

    const formattedDate = this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal;
    const requestObject = {
      from_id:userId,
      to_id:this.attendee_To,
      time:formattedDate
    }
    this._fd.schdeuleAcallattende(requestObject,this.token).subscribe(res => {
      console.log(res);
      if (res.code == 1) {
        this.toastr.success('Call scheduled succesfully!');

        let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
        this._fd.totalTimeSlotsForAttendees(this.attendee_To, myDate).subscribe(res => {
          this.newTimeSlots = res.result;
        })
        setTimeout(() => {
          $('.scheduleCallmodal').modal('hide');
        }, 2000);
      }

    });
  }
  onDateSelect(dates) {
    let myDate = dates.year + '-' + dates.month + '-' + dates.day;
    this._fd.totalTimeSlotsForAttendees(this.attendee_To, myDate).subscribe(res => {
      this.newTimeSlots = res.result;
    })
  }
  toggleModal_(val: boolean) {
    this.toggleModal.emit(val);
  }
}
