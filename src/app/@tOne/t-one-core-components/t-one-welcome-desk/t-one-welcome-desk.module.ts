import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneWelcomeDeskRoutingModule } from './t-one-welcome-desk-routing.module';
import { TOneWelcomeDeskComponent } from './t-one-welcome-desk.component';
import { TOneTechSupportComponent } from './t-one-tech-support/t-one-tech-support.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TOneWelcomeDeskComponent, TOneTechSupportComponent],
  imports: [
    CommonModule,
    TOneWelcomeDeskRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneWelcomeDeskModule { }
