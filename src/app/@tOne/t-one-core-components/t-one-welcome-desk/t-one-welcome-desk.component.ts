import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { TOneTechSupport } from './model/index';
declare var $:any;
@Component({
  selector: 'app-t-one-welcome-desk',
  templateUrl: './t-one-welcome-desk.component.html',
  styleUrls: ['./t-one-welcome-desk.component.scss']
})
export class TOneWelcomeDeskComponent implements OnInit, AfterViewInit {
  elements:any = TOneTechSupport;
  getData: any=[];
  main_banner:any;
  token:any
  agenda_enable:any
  feedback_enable:any;
  agenda_position: any;
  left_agenda: any;
  top_agenda: any;
  techsupport: any;
  tech_position: any;
  left_tech: any;
  top_tech: any;
  feedback_position: any;
  left_feedback: any;
  top_feedback: any;
  video: any;
  constructor(private router:Router,private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
  }
  ngAfterViewInit(){
    this._fd.getmenuToken(this.token).subscribe(res => {

      this.getData = res;
      this.main_banner = this.getData.result.helpdesk[0].image
      this.video = this.getData.result.helpdesk[0].video

      // alert(this.video)
      this.techsupport = this.getData.result.helpdesk[0].enable
      this.tech_position = this.getData.result.helpdesk[0].position
      let check2 = this.tech_position.split('x');
      this.left_tech = check2[0];
      this.top_tech = check2[1];
      
      this.agenda_enable = this.getData.result.helpdesk[0].agenda_enable
      this.agenda_position = this.getData.result.helpdesk[0].agenda_position
      let check = this.agenda_position.split('x');
      this.left_agenda = check[0];
      this.top_agenda = check[1];

      this.feedback_enable = this.getData.result.helpdesk[0].feedback_enable
      this.feedback_position = this.getData.result.helpdesk[0].feedback_position
      let check3 = this.feedback_position.split('x');
      this.left_feedback = check3[0];
      this.top_feedback = check3[1];


    });
  }
  showModals(data){
    if(data == 'techsupport'){
      this.router.navigate(['/theme1/tonelayout/welcomedesk/techsupport']);
    }
    if(data == 'agenda'){
       $('.t_one_agendaModal').modal('show');
    }
    // $('.t_one_techsupport').modal('show');
    if(data == 'feedback'){
      // alert(data)
      $('.t_one_feedbackModal').modal('show');
    }
  }
}
