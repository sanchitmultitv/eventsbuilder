import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneTechSupportComponent } from './t-one-tech-support/t-one-tech-support.component';
import { TOneWelcomeDeskComponent } from './t-one-welcome-desk.component';


const routes: Routes = [
  {
    path:'', 
    component:TOneWelcomeDeskComponent,
    children:[
      {
        path:'techsupport',
        component:TOneTechSupportComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneWelcomeDeskRoutingModule { }
