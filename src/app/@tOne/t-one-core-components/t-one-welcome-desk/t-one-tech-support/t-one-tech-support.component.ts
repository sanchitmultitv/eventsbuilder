import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-t-one-tech-support',
  templateUrl: './t-one-tech-support.component.html',
  styleUrls: ['./t-one-tech-support.component.scss']
})
export class TOneTechSupportComponent implements OnInit, OnDestroy {
    textMessage = new FormControl('');
    msg;
    qaList;
    interval;
    token:any;
    exhibiton:any=[];
    documents:any=[];
    storage:any=JSON.parse(localStorage.getItem('virtual'));
    exhibition_id = '0';
    constructor(private _fd: FetchDataService, private chat: ChatService, private router:Router) { }
  
    ngOnInit(): void {
      this.token = localStorage.getItem('token')
      $('.t_one_techsupport').modal('show');
     this.getQA();
    //  let token='toujeo-'+this.storage.event_id;
    

     this.chat.socketConnection(this.token);
     this.chat.getSocketConnectedMessages(this.token).subscribe((data=>{
        console.log('socketdata', data);
        if(typeof data == 'string'){
          if(data.includes('question_reply')){
            this.getQA();
          }
        }
      }));
      }
    
    getQA(){
        let data = JSON.parse(localStorage.getItem('virtual'));
        this._fd.gethelpdeskanswers(data.id,this.token).subscribe((res=>{
          this.qaList = res.result;
        }))
    
      }
    postQuestion(value){
      let data = JSON.parse(localStorage.getItem('virtual'));
      this._fd.helpdesk(data.id,value,this.token).subscribe((res=>{
        if(res.code == 1){
          this.textMessage.reset();
        }
        this.getQA();
      }));     
    }

  closetsPopup(){
    $('.t_one_techsupport').modal('hide');
    this.router.navigate(['/theme1/tonelayout/welcomedesk']);
  }
    @HostListener('document:click', ['$event', '$event.target'])
      onClick(event: MouseEvent, targetElement: HTMLElement): void {
        let t_one_aq:any=document.getElementById('t_one_ts');
          if(targetElement===t_one_aq){
            this.closetsPopup();
          }
      }
    ngOnDestroy() {
    }
  }
  
