export const TOneTechSupport = [
    {
        title:'agenda',
        data_target:".t_one_agendaModal",
        t: 60+'%',
        l: 17.1+'%',
        w: 9+'%',
        h: 21+'%',
    },
    {
        title:'helpdesk',
        path:'/theme1/tonelayout/welcomedesk/techsupport',
        t: 54+'%',
        l: 44.1+'%',
        w: 9+'%',
        h: 9+'%',
    },
    {
        title:'feedback',
        data_target:".t_one_agendaModal",
        t: 43+'%',
        l: 75+'%',
        w: 9+'%',
        h: 21+'%',
    },
    
];