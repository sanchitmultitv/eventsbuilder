import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter, HostListener, AfterViewInit } from '@angular/core';
import { FetchDataService } from '../../../../services/fetch-data.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { ChatService } from 'src/app/services/chat.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-selfie',
  templateUrl: './selfie.component.html',
  styleUrls: ['./selfie.component.scss']
})
export class SelfieComponent implements OnInit, AfterViewInit {
  getData: any=[];
  hell; 
  videoWidth = 0;
    videoHeight = 0;
    showImage = false;
    liveMsg = false; 
    camstream: any;
    img;
    token:any
    dataurl;
    bgimgs = [
      '../../../assets/philips/morningPhotoboth.jpg',
      '../../../assets/philips/eveningPhotoboth.jpg',
    ]
    bgImage;
    takeSnapBg;
    bgmorning:boolean;
    constructor(private renderer: Renderer2, private _fd: FetchDataService, private chat: ChatService, private router:Router) { }
    @ViewChild('video', { static: true }) videoElement: ElementRef;
    @ViewChild('canvas', { static: true }) canvas: ElementRef;
    @Output('myOutputVal') myOutputVal = new EventEmitter();
  
    ngOnInit(): void {
      this.token = localStorage.getItem('token')
      this.showCamera();
      // $('.videoData').show();    
      let modal = document.getElementById("camModal");
      window.onclick = (event) => {
        if (event.target == modal) {
          this.closeCamera();
        }
      }
    }
    ngAfterViewInit(){
      this._fd.getmenuToken(this.token).subscribe(res => {
        this.getData = res;
        this.hell = this.getData.result.others[0].photobooth_trans_img;
        console.log(this.hell)
      });
   
    }
  
    showCamera(){
      $('.t_one_captureModal').modal('show');
      this.startCamera();
    }
    constraints = {
      facingMode: { exact: 'environment' },
      video: {
        width: { ideal: 720 },
        height: { ideal: 480 }
      }
    };
  
    startCamera() {
      if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
        navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
      } else {
        alert('Sorry, camera not available.');
      }
  
    }
    handleError(error) {
      console.log('Error: ', error);
    }
    stream: any;
    attachVideo(stream) {
      this.camstream = stream;
      this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', this.camstream);
      this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
        this.videoHeight = this.videoElement.nativeElement.videoHeight;
        this.videoWidth = this.videoElement.nativeElement.videoWidth;
      });
    }
    stopStream() {
      if (null != this.camstream) {
        var track = this.camstream.getTracks()[0];
        track.stop();
        this.videoElement.nativeElement.load();
        this.camstream = null;
      }
    }
    capture() {
      this.showImage = true;
      let canvasRec:any = document.getElementById('canvas');
      let preview:any = document.getElementById('vid');
      let context = canvasRec.getContext('2d');
      var cw = 640;
      var ch = 360;
      canvasRec.width = cw;
      canvasRec.height = ch;
      context.drawImage(preview, 85, 50, cw/1.3, ch/1.19 ); 
         
      // let watermark = new Image();
      let bgimg = new Image();
      context.beginPath();
      // bgimg.crossOrigin = "Anonymous";
      bgimg.src = this.hell;
      context.drawImage(bgimg, 0, 0, preview.videoWidth/1.12, preview.videoHeight/1.29);
      this.dataurl = canvasRec.toDataURL('mime');
      this.img = canvasRec.toDataURL("image/png", 0.7);
      console.log(this.img)
    }
  
    uploadVideo() {
      console.log('test');
      let user= JSON.parse(localStorage.getItem('virtual'));
      const formData = new FormData();
      formData.append('user_id', user.user_id);
      formData.append('user_name', user.name);
      formData.append('image', this.img);
      formData.append('event_id', user.event_id);
      this._fd.uploadsample(formData).subscribe(res => {
        console.log('upload', res)
      });
      this._fd.uploadsample(formData).subscribe(res => {
        console.log('upload', res)
      });
    }
  
    showCapture() {
      $('.capturePhoto').modal('show');
      this.startCamera();
    }
  
    closeCamera() {
      this.stopStream();
      $('.t_one_captureModal').modal('hide');
      this.router.navigate(['/theme1/tonelayout/photobooth']);
  
    }
    reload() {
      this.showImage = false;
      this.startCamera();
    }
    @HostListener('keydown', ['$event']) onKeyDown(key) {
      if (key.keyCode === 27) {
        this.closeCamera();
      }
    }
    @HostListener('document:click', ['$event', '$event.target'])
      onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let one_to_one:any=document.getElementById('camModal');
      if(targetElement===one_to_one){
        this.closeCamera();
      }
    }
    @HostListener('window:beforeunload', ['$event'])
    beforeunloadHandler(event) {
    var message = 'Important: Please click on \'Save\' button to leave this page.';
    if (typeof event == 'undefined') {
      event = window.event;
    }
    if (event) {
      event.returnValue = message;
    }
    alert(message)
  }
}
