import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOnePhotoboothRoutingModule } from './t-one-photobooth-routing.module';
import { TOnePhotoboothComponent } from './t-one-photobooth.component';
import { SelfieComponent } from './selfie/selfie.component';


@NgModule({
  declarations: [TOnePhotoboothComponent, SelfieComponent],
  imports: [
    CommonModule,
    TOnePhotoboothRoutingModule
  ]
})
export class TOnePhotoboothModule { }
