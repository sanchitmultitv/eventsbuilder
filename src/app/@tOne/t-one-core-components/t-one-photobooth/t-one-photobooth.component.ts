import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

declare var $:any;
@Component({
  selector: 'app-t-one-photobooth',
  templateUrl: './t-one-photobooth.component.html',
  styleUrls: ['./t-one-photobooth.component.scss']
})
export class TOnePhotoboothComponent implements OnInit, AfterViewInit{
  getData: any=[];
  hell
  token;
  constructor(private router:Router,private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
  }
  ngAfterViewInit(){
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.hell = this.getData.result.others[0].photo_booth_img
      console.log(this.hell)
    });
 
  }

}
