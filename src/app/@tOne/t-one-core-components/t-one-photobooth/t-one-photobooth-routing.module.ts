import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOnePhotoboothComponent } from './t-one-photobooth.component';
import { SelfieComponent } from './selfie/selfie.component';


const routes: Routes = [
  {
    path:'', 
    component: TOnePhotoboothComponent,
    children:[
      {
        path:'selfie',
        component:SelfieComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOnePhotoboothRoutingModule { }
