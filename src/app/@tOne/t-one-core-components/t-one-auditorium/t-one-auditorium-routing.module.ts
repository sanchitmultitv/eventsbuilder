import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneAskquestionComponent } from './t-one-askquestion/t-one-askquestion.component';
import { TOneAuditoriumComponent } from './t-one-auditorium.component';
import { TOneGroupchatComponent } from './t-one-groupchat/t-one-groupchat.component';
import { TOnePollComponent } from './t-one-poll/t-one-poll.component';


const routes: Routes = [
  {
    path:'', 
    component: TOneAuditoriumComponent,
    children:[
      {
        path:'groupchat',
        component:TOneGroupchatComponent
      },
      {
        path:'askquestion',
        component:TOneAskquestionComponent
      },
      {
        path:'quiz',
        component:TOnePollComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneAuditoriumRoutingModule { }
