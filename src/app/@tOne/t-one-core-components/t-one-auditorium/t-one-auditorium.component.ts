import { Component, HostListener, OnInit } from '@angular/core';
import * as _ from 'underscore';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Rightmenu } from './t-one-auditorium-shared/rightmenu';
import { ActivatedRoute, Params, Router } from '@angular/router';

declare var $: any;
declare var YoutubePlayback:any;
@Component({
  selector: 'app-t-one-auditorium',
  templateUrl: './t-one-auditorium.component.html',
  styleUrls: ['./t-one-auditorium.component.scss']
})
export class TOneAuditoriumComponent implements OnInit {
  videoEnd = false;
  textMessage = new FormControl('');
  // videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  videoPlayer = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';
  player: any;
  rightmenu: any = Rightmenu;
  auditorium; audi; audi_id; main_banners;
  poster: any;
  // stream: any;
  qna: any;
  showLoader: boolean = true;
  xyz: any;
  interval;
  clap: any;
  gc: any;
  poll: any;
  width_player: any;
  height_player: any;
  left_player: any;
  top_player: any;
  token: any
  pollingLIst: any = [];
  poll_id;
  // audi_id;
  showmsg = true;
  showPoll = false;
  pollForm = new FormGroup({
    polling: new FormControl(''),
  });
  curr_auditorium
  msg;
  quiz: any;
  imag: any;
  like = false;
  reaction: any;
  embedVideo: string = '';
  embedVideoStatus: 'start' | 'stop' = 'start';
  loaderTasks = {};
  imgBanner;
  stream;
  isStreamLive=false;
  videoWasStartedAndInActive: string = "“This session will start once the Administrator will start the IT”. Please stay tuned”";
  // token:any;
  constructor(private chat: ChatService, private _fd: FetchDataService, private _chat: ChatService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');

    this.interval = setInterval(() => {
      this.getHeartbeat();
    }, 60000);
    this.route.paramMap.subscribe(params => {
      this.audi_id = params.get('id');
      this._fd.getAudis(this.token, this.audi_id).subscribe((res => {
        this.audi = res;
        console.log(res);
        // this.main_banners = this.audi.result.audilist[0].banner;
        this.main_banners = this.audi.result.banner;
        // this.imgBanner = this.audi.result.banner;
        this.width_player = this.audi.result.audilist[0].player_width
        this.height_player = this.audi.result.audilist[0].player_height
        this.top_player = this.audi.result.audilist[0].player_top
        this.left_player = this.audi.result.audilist[0].player_left
        this.poster = this.audi.result.audilist[0].poster
        let stream = this.audi.result.audilist[0].stream;
        this.stream = stream;
        if(this.audi.result?.audilist[0]?.is_live==1){
          this.isStreamLive = true;
        }else{
          this.isStreamLive =false;
        }
        if (stream == "") {
          this.getSocketAudienceStartStop(this.audi.result?.audilist[0]?.channel_name);
          this.showLoader = true;
          this.embedVideo = this.getLiveVideoURL('');
        } else {
          this.showLoader = false;
        }
        this.clap = this.audi.result.audilist[0].clap
        this.qna = this.audi.result.audilist[0].qna
        this.gc = this.audi.result.audilist[0].groupchat
        this.poll = this.audi.result.audilist[0].poll
        this.quiz = this.audi.result.audilist[0].quiz
        this.reaction = this.audi.result.audilist[0].reaction

        // alert(this.stream)

        // alert(this.left_player)
        if(typeof stream == 'string'){
          if(stream.includes('youtube')||stream.includes('vimeo')){
            let showEmbed = document.getElementById('showEmbed');
            var x = document.createElement("IFRAME");
            x.setAttribute("src", stream);
            showEmbed.appendChild(x);
          }else{
            this.playVideo(stream);
          }
        }
        let constantStreamUrl=`https://virtualapi.multitvsolution.com/meetingvirtual/build/?channel_name=${this.audi.result?.audilist[0]?.channel_name}&role=audience`;
        if((stream == '')&&(this.audi.result?.audilist[0]?.is_live==1)){
          let showEmbed = document.getElementById('showEmbed');
            var x = document.createElement("IFRAME");
            x.setAttribute("src", constantStreamUrl);
            x.style.width='100%';
            x.style.height=window.innerHeight*.95+'px';
            document.getElementById('showEmbed').style.height='auto';
            showEmbed.appendChild(x);
        }
      }));
      this.chat.getconnect(this.token);

      this.chat.getMessages().subscribe((data => {

        console.log('socketdata', data);
        let poll = data;
        let polls = poll.split('_');
        console.log(data)
        if (polls[0] == 'start' && polls[1] == 'poll') {
          this.poll_id = polls[2];
          // alert(this.poll_id)
          //  this.audi_id = polls[3]
          // console.log(audi_id,"sad");
          this.getPolls();
          setTimeout(() => {
            this.showmsg = false;
            this.showPoll = true;
          }, 1000);
          //  $('.pollModal').modal('show');
        }
        else {
          this.showmsg = true;
          this.showPoll = false;
        }
        //  console.log('final',this.poll_id);
      }));
    });
    // this.getAllAudis();
  }

  getSocketAudienceStartStop(initial_channel_name: string) {
    const socketToken = `beb-${initial_channel_name}`
    this.chat.getconnectAuditorium(socketToken);
    this.chat.getSocketConnectedMessages(socketToken).subscribe((res: string) => {
      if (res.startsWith('startlive')) {
        this.embedVideo = this.getLiveVideoURL(res.split("_")[1]);
        this.showLoader = false;
      } else if (res.startsWith('stoplive')) {
        this.videoWasStartedAndInActive = "Session is inactive now"
        this.embedVideoStatus = 'stop';
        this.embedVideo = '';
        this.showLoader = true;
      }
    });
  }

  getHeartbeat() {
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    let data2 = JSON.parse(localStorage.getItem('login_data'));
    const formData = new FormData();
    formData.append('user_id', data.id);
    formData.append('event_id', data2[0].id);
    formData.append('audi', this.audi_id);
    this._fd.heartbeat(this.token, formData).subscribe(res => {
      console.log(res);
    })
  }
  // getAllAudis() {
  //   this.route.paramMap.subscribe(params => {
  //     this.audi_id = params.get('id');
  //     this._fd.getAudis(this.audi_id).subscribe((res => {
  //       this.audi = res;
  //       this.main_banners = this.audi.result.audilist[0].banner
  //       this.poster = this.audi.result.audilist[0].poster

  //       this.clap = this.audi.result.audilist[0].clap
  //       this.qna = this.audi.result.audilist[0].ask_question
  //       this.gc = this.audi.result.audilist[0].groupchat
  //       this.width_player = this.audi.result.audilist[0].player_width
  //       this.height_player = this.audi.result.audilist[0].player_height


  //     }));
  //   });

  // }
  playVideo(stream) {
    // alert(this.xyz)
    var playerElement = document.getElementById("player-wrapper");
    this.player = new Clappr.Player({
      parentId: 'player-wrapper',
      source: stream,
      poster: this.poster,
      // height: 390,
      height: '342px',
      maxBufferLength: 30,
      width: '100%',
      autoPlay: true,
      hideMediaControl: false,
      hideVolumeBar: true,
      hideSeekBar: true,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true,
      },
      // plugins: {
      //   playback: [YoutubePlayback]
      // }
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   // $('#player-wrapper video').css({ 'object-fit': 'fill'});
    //   this.player.resize({ width: '100%', height: window.innerHeight - 41 });
    //   $('#player-wrapper .container').find('.player-poster').css('background-size', '84%');
    //   // $('#player-wrapper > div > .media-control').css('background', 'red');
    // }

  }
  // getAllAudis() {
  //   this.route.paramMap.subscribe(params => {
  //     this.audi_id = params.get('id');
  //     this._fd.getAudis(this.audi_id).subscribe((res => {
  //       this.audi = res;
  //       this.main_banners = this.audi.result.audilist[0].banner
  //       this.poster = this.audi.result.audilist[0].poster
  //       this.stream = this.audi.result.audilist[0].stream
  //       this.clap = this.audi.result.audilist[0].clap
  //       this.qna = this.audi.result.audilist[0].ask_question
  //       this.gc = this.audi.result.audilist[0].group_chat

  //     }));
  //   });
  // }
  fullscreen() {
    this.player.core.mediaControl.toggleFullscreen();
  }
  openRigthMenu(name) {
    switch (name) {
      case "ask question":
        this.router.navigate(['/theme1/tonelayout/auditorium/' + this.audi_id + '/askquestion']);
        break;
      case "clapp":
        // let playaudio: any = document.getElementById('myAudioClap');
        // playaudio.play();
        this.clapPost();
        break;
      case "group chat":
        this.router.navigate(['/theme1/tonelayout/auditorium/' + this.audi_id + '/groupchat']);
        break;
      case "poll":
        // this.router.navigate(['/theme1/tonelayout/auditorium/'+this.audi_id+'/poll']);
        $('.pollModal').modal('show');
        break;
      case "quiz":
        this.router.navigate(['/theme1/tonelayout/auditorium/' + this.audi_id + '/quiz']);
      // $('.quizModal').modal('show');
    }
  }
  closePopup() {
    $('.pollModal').modal('hide');
    // this.router.navigate(['/theme1/tonelayout/auditorium/'+this.curr_auditorium]);

  }
  getPolls() {

    this._fd.getPollList(this.token).subscribe(res => {
      $('.pollModal').modal('show');
      // this.router.navigate(['/theme1/tonelayout/auditorium/'+this.audi_id+'/poll']);
      this.pollingLIst = res.result;

    })

  }
  clapPost(){
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
    let user = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', user.id);
    formData.append('audi_id', this.audi_id);
    this._fd.postClap(formData).subscribe(res=>{
      console.log(res)
    })
  }
  reactionPost(item){    			
    let user = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', user.id);
    formData.append('name', user.name);
    formData.append('comment',item);
    formData.append('type','reaction');
    this._fd.postReaction(formData).subscribe(res=>{
      console.log(res)
    })
  }
  likeopen(data) {
    
    if (data == 'like') {
      this.imag = 'assets/icons/like.png';
      this.reactionPost(data);
    }
    if (data == 'claps') {
      this.imag = 'assets/icons/claps.png';
      // this.clapPost();
      this.reactionPost(data);
      let playaudio: any = document.getElementById('myAudioClap');
      playaudio.play();
    }
    if (data == 'heart') {
      this.imag = 'assets/icons/heart.png';
      this.reactionPost(data);
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  pollSubmit(id) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(id);
    console.log(data.id);

    this._fd.postPoll(id, data.id, this.pollForm.value.polling, this.token).subscribe(res => {
      console.log(res);
      if (res.code == 1) {
        this.msg = 'Thank you for submitting your answer';
        this.showPoll = false;
        // this.showmsg = false;
        setTimeout(() => {
          // $('.pollModal').modal('hide');
          this.showmsg = true;
          this.msg = '';
          this.pollForm.reset();
        }, 2000);

      }
      this.pollingLIst = [];
      $('.pollModal').modal('toggle');
    });
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.player.resize({ width: '100%', height: window.innerWidth / 3.78885 });
  }
  getLiveVideoURL(channel_name: string) {
    return 'https://virtualapi.multitvsolution.com/meetingvirtual/build/?channel_name=' + channel_name + '&role=audience';
  }
}
