import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-t-one-groupchat',
  templateUrl: './t-one-groupchat.component.html',
  styleUrls: ['./t-one-groupchat.component.scss']
})
export class TOneGroupchatComponent implements OnInit {
  videoEnd = false;
  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session3.smil/playlist.m3u8';
  bgimgs = [
    '../../../../../assets/philips/morningStage.jpg',
    '../../../../../assets/philips/morningStage.jpg',
  ]
  bgImage;
  bgmorning: boolean;

  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  // serdia_room = 'myanmar_16';
  username = JSON.parse(localStorage.getItem('virtual')).name;
  storage:any=JSON.parse(localStorage.getItem('virtual'));
  curr_auditorium;
  token: string;
  constructor(private chatService: ChatService, private _fd: FetchDataService, private router:Router, private route:ActivatedRoute) { }
  ngOnInit(): void {
    this.token = localStorage.getItem('token')

    this.route.parent.params.subscribe((params:Params)=>{
      this.curr_auditorium=params.id;
    });
    $('.t_one_groupChat').modal('show');

  }
  closegroupchat(){
    $('.t_one_groupChat').modal('hide'); 
    // alert(this.curr_auditorium)
    this.router.navigate(['/theme1/tonelayout/auditorium/'+this.curr_auditorium]);
  }
  ngAfterViewInit() {
    this.chatGroup();
    let event_id =this.storage.event_id;
    this.chatService.socketConnection(this.token);
    this.chatService.getSocketConnectedMessages(this.token).subscribe((data: any) => {
      if(data=='group_chat'){
        this.chatGroup();
      }
    });
    
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  loadData() {
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.token);
    localStorage.setItem('username', data.name);
  }
  chatGroup() {
    this._fd.groupchating(this.token).subscribe(res => {
      this.messageList = res.result;
    });
  }
  postMessage(value) {
    let yyyy: any = new Date().getFullYear();
    let mm: any = new Date().getMonth() + 1;
    let dd: any = new Date().getDate();
    let hour: any = new Date().getHours();
    let min: any = new Date().getMinutes();
    let sec: any = new Date().getSeconds();
    if (sec.toString().length !== 2) {
      sec = '0' + sec;
    } else {
      sec = sec;
    }
    if (min.toString().length !== 2) {
      min = '0' + min;
    } else {
      min = min;
    }
    if (hour.toString().length !== 2) {
      hour = '0' + hour;
    } else {
      hour = hour;
    }
    if (dd.toString().length !== 2) {
      dd = '0' + dd;
    } else {
      dd = dd;
    }
    if (mm.toString().length !== 2) {
      mm = '0' + mm;
    } else {
      mm = mm;
    }
    let created: any = `${yyyy}-${mm}-${dd} ${hour}:${min}:${sec}`;
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    let data2: any = JSON.parse(localStorage.getItem('login_data'));
    console.log(data2[0].id)
    const formData = new FormData();
    formData.append('event_id', data2[0].id);
    formData.append('room_name', this.token);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('created', created);
    this._fd.postGroupChat(this.token, formData).subscribe(res => {
      console.log('posting', res);
      let arr = {
        'user_id': data.id,
        'user_name': data.name,
        'room_name':this.token,
        'email':data.email,
        'created': created,
        'chat_data':value,
        'event_id':data2[0].id,
        'is_approved': '1',
      };
      this.messageList.push(arr);
    });
    this.textMessage.reset();
  }

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let t_one_aq:any=document.getElementById('t_one_gc');
    if(targetElement===t_one_aq){
      this.closegroupchat();
    }
  }
}
