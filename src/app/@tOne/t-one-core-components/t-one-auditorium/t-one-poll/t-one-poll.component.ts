import { Component, OnInit, HostListener } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-t-one-poll',
  templateUrl: './t-one-poll.component.html',
  styleUrls: ['./t-one-poll.component.scss']
})
export class TOnePollComponent implements OnInit {
  interval;
  player:any
  user_name;
  newWidth;
  newHeight;
   videoPlayer;
   quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  liveMsg= false;
  curr_auditorium;
  token;
  constructor(private router:Router, private route:ActivatedRoute,private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
    
    this.token = localStorage.getItem('token')

    $('.quizModal').modal('show');
    this.route.parent.params.subscribe((params:Params)=>{
      this.curr_auditorium=params.id;
    });
    this.user_id = JSON.parse(localStorage.getItem('virtual')).id;
    // $('.quizModal').modal('show');
    this.chat.getconnect(this.token);

      this.chat.getMessages().subscribe((data) => {
        if(typeof data == 'string'){
          if (data.includes('start_quiz')){
            this.getQuizlist();
          }
          if(data.includes('stop_quiz')){
            this.quizlist = [];
            this.summaryList = [];
            this.summaryBoolean = false;
          }
        }
      })
    
  }
  styleChanger(){
    let myStyles = {
      fontSize: '16px',
      height: '300px',
      'margin-top':'45px'
    }    
    return myStyles;
  }
  getQuizlist() {
    // let event_id = 164;
    this._fd.getQuizList(this.token).subscribe((res: any) => {
      this.quizlist = res.result;
      console.log('dddd', res)
    });
  }
  loadData() {
    this._fd.getQuiz().subscribe((data: any) => {
      this.quizData = data.results;
      for (let i = 0; i < this.quizData.length; i++) {
        for (let j = 0; j < this.quizData[i]['incorrect_answers'].length; j++) {
          this.quizData[i].option = this.quizData[i]['incorrect_answers'];
        }
        this.quizData[i].option.push(this.quizData[i]['correct_answer']);
        // console.log('time', this.quizData[i])          
      }
    });
  }

  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';
  nextQuestion(index, j, quiz, option) {
    let quiz_id: any = quiz.id;
    let answer: any = quiz.correct_answer;
    this.optionIndex = j;
    this.color = 'green';
    const formData = new FormData();
    formData.append('user_id', this.user_id);
    formData.append('quiz_id', quiz_id);
    formData.append('answer', option);
    this._fd.postSubmitQuiz(this.token,formData).subscribe(res=>{
      console.log('ind', index, this.quizlist.length);
    });
    setTimeout(() => {
      this.index = index + 1;
      this.optionIndex = -1;
      this.color = '#27272d';
      if (this.quizlist.length === this.index){
        let event_id = 164;
        this._fd.getSummaryQuiz(event_id, this.user_id).subscribe(resp=>{
          this.summaryBoolean = true;
          this.summaryList = resp.result;
          this.quizStatus = 'Quiz Summary';
        });
      }
    }, 1000);
  }

  closePopup() {
    $('.quizModal').modal('hide');
    this.router.navigate(['/theme1/tonelayout/auditorium/'+this.curr_auditorium]);

  }
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let t_one_aq:any=document.getElementById('t_one_quiz');
    if(targetElement===t_one_aq){
      this.closePopup();
    }
  }
}
