import { Component, OnInit, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-t-one-askquestion',
  templateUrl: './t-one-askquestion.component.html',
  styleUrls: ['./t-one-askquestion.component.scss']
})
export class TOneAskquestionComponent implements OnInit {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  curr_auditorium;

  constructor(private router: Router, private _fd:FetchDataService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.parent.params.subscribe((params:Params)=>{
      this.curr_auditorium=params.id;
    });
    $('.t_one_askquestion').modal('show');
  }

getQA(){
  let data = JSON.parse(localStorage.getItem('virtual'));
 //  this._fd.Liveanswers().subscribe((res=>{
 //    console.log(res);
 //    this.qaList = res.result;
 //  }))
}
postQuestion(value){
  let data = JSON.parse(localStorage.getItem('virtual'));
  let audi_id = '78';
 console.log(value, data.id, audi_id);
  this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
    if(res.code == 1){
      this.msg = 'Submitted Succesfully';
    }
    this.textMessage.reset();
  }))
}
ngOnDestroy() {
  clearInterval(this.interval);
}
closeqnaPopup() {
    $('.t_one_askquestion').modal('hide');
    this.router.navigate(['/theme1/tonelayout/auditorium/'+this.curr_auditorium]);
  }
  
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let t_one_aq:any=document.getElementById('t_one_aq');
    if(targetElement===t_one_aq){
      this.router.navigate(['/theme1/tonelayout/auditorium/'+this.curr_auditorium]);
    }
  }

}
