import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneAuditoriumRoutingModule } from './t-one-auditorium-routing.module';
import { TOneAuditoriumComponent } from './t-one-auditorium.component';
import { TOneGroupchatComponent } from './t-one-groupchat/t-one-groupchat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TOneAskquestionComponent } from './t-one-askquestion/t-one-askquestion.component';
import { TOnePollComponent } from './t-one-poll/t-one-poll.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [TOneAuditoriumComponent, TOneGroupchatComponent, TOneAskquestionComponent, TOnePollComponent],
  imports: [
    CommonModule,
    TOneAuditoriumRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class TOneAuditoriumModule { }
