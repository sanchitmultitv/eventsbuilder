import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneLobbyComponent } from './t-one-lobby.component';


const routes: Routes = [
  {path:'', component:TOneLobbyComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneLobbyRoutingModule { }
