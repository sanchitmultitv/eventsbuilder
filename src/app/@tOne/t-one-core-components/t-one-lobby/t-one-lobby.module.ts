import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneLobbyRoutingModule } from './t-one-lobby-routing.module';
import { TOneLobbyComponent } from './t-one-lobby.component';


@NgModule({
  declarations: [TOneLobbyComponent],
  imports: [
    CommonModule,
    TOneLobbyRoutingModule
  ]
})
export class TOneLobbyModule { }
