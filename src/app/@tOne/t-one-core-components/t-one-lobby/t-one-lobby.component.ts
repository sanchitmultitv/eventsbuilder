import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { SetupServiceService } from 'src/app/services/setup-service.service';

@Component({
  selector: 'app-t-one-lobby',
  templateUrl: './t-one-lobby.component.html',
  styleUrls: ['./t-one-lobby.component.scss']
})
export class TOneLobbyComponent implements OnInit, AfterViewInit {
  skipItem;
  skipper = false;
  getData: any=[];
  getData2: any=[];
  hell;audiEna;welcomeEna;exhiEna;photoEna;loungeEna;audiVid;loungeVid;exhiVid;photoVid;deskVid
  receptionEnd = false;
  mainAudi = false;
  auditoriumLeft = false;
  helpDesk = false;
  auditoriumRight = false;
  networkingLounge = false;
  showVideo = false;
  relatedPdf;
  i:any;audiID:any
  token;
  back_video: any;
  audipoint: any;
  left_audipoint: any;
  top_audipoint: any;
  deskpoint: any;
  loungepoint: any;
  boothpoint: any;
  exhipoint: any;
  left_exhipoint: any;
  left_deskpoint: any;
  top_deskpoint: any;
  left_loungepoint: any;
  top_exhipoint: any;
  top_loungepoint: any;
  left_boothpoint: any;
  top_boothpoint: any;
  smallvideo_width: any;
  smallvideo_top: any;
  smallvideo_left: any;
  skipButtons:any={helpdesk:'', lounge:''};
  constructor(private _analytics: SetupServiceService,private _fd : FetchDataService,private router: Router) { }

  ngOnInit(): void {
  this.token = localStorage.getItem('token')
  }
  ngAfterViewInit(){
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.hell = this.getData.result.lobby[0].animation_video
      this.back_video = this.getData.result.lobby[0].lobby_video
      this.smallvideo_left = this.getData.result.lobby[0].pos_left
      this.smallvideo_top = this.getData.result.lobby[0].pos_top
      this.smallvideo_width = this.getData.result.lobby[0].video_width

      this.audiEna = this.getData.result.auditorium[0].enable
      this.welcomeEna = this.getData.result.helpdesk[0].enable
      // alert(this.welcomeEna)
      this.audiVid = this.getData.result.auditorium[0].animation_video
      this.exhiEna = this.getData.result.exhibition[0].enable
      this.exhiVid = this.getData.result.exhibition[0].animation_video
      this.loungeEna = this.getData.result.lounge[0].enable
      this.loungeVid = this.getData.result.lounge[0].animation_video
      this.deskVid = this.getData.result.helpdesk[0].animation_video
      this.photoEna = this.getData.result.others[0].photo_booth
      this.photoVid = this.getData.result.others[0].booth_video
       console.log(this.audiVid) 
      this.audipoint = this.getData.result.lobby[0].auditorium_pointer
      let check = this.audipoint.split('x')
      this.left_audipoint = check[0]
      this.top_audipoint = check[1]
      this.exhipoint = this.getData.result.lobby[0].exhibition_pointer
      let check1 = this.exhipoint.split('x')
      this.left_exhipoint = check1[0]
      this.top_exhipoint = check1[1]
      this.deskpoint = this.getData.result.lobby[0].helpdesk_pointer
      let check2 = this.deskpoint.split('x')
      this.left_deskpoint = check2[0]
      this.top_deskpoint = check2[1]
      this.loungepoint = this.getData.result.lobby[0].lounge_pointer
      let check3 = this.loungepoint.split('x')
      this.left_loungepoint = check3[0]
      this.top_loungepoint = check3[1]
      this.boothpoint = this.getData.result.lobby[0].photobooth_pointer
      let check4 = this.boothpoint.split('x')
      this.left_boothpoint = check4[0]
      this.top_boothpoint = check4[1];
      this.skipButtons.helpdesk = this.getData.result.helpDesk[0].skip_time;
      
      
    });
    // let vid:any = document.getElementById('myVideo');
    // vid.play()
  }
  
  checkAudi() {
    this._fd.multiAudi(this.token).subscribe(res => {
      this.getData2 = res;
      console.log(this.getData2)
      let noOfAudis = Object.keys(this.getData2.result.audilist).length
      if (noOfAudis < 2) {
        this.audiID = this.getData2.result.audilist[0].id;
        this.router.navigate(['/theme1/tonelayout/auditorium/' + this.audiID]);
      } else {
        this.router.navigate(['/theme1/tonelayout/multi-audi']);
      }
      // if (hello < 2) {
      //   this.audiID = this.getData2.result.audilist[0].id
      //   alert(this.audiID)
      // this.videoPlay('auditorium_233')
      // } else {
      // this.videoPlay('auditorium_main')
      // }
    });
  }

  videoPlay(data) {
    //  alert(data)
    //let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '18:59:59') {
    if (data === 'click_lounge') {
      // alert(data)
      let pauseVideo: any = document.getElementById("centerPlay");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
      this.skipper = false;
      let a = -1;
      let interval = setInterval(()=>{
      a+=1;
      this.skipItem='lounge';
      if(a==this.getData.result.lounge[0]['skip_time']){
        this.skipper = true;
        clearInterval(interval);
        console.log(a, )

      }
    }, 1000)
      
    }
    if (data === 'auditorium_233') {
      // alert(data)
      let pauseVideo: any = document.getElementById("bgImg");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'auditorium_main') {
      // alert(data)
      let pauseVideo: any = document.getElementById("bgImg");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.mainAudi = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_exhibition') {
      // alert(data)
      let pauseVideo: any = document.getElementById("bgImg");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_photobooth') {
      // alert(data)
      let pauseVideo: any = document.getElementById("bgImg");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_helpdesk') {
      this.skipItem='helpdesk';
      let pauseVideo: any = document.getElementById("bgImg");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
      this.helpDesk = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
      let a = -1;
      let interval = setInterval(()=>{
      a+=1;
      if(a==this.getData.result.helpdesk[0]['skip_time']){
        this.skipper = true;
        clearInterval(interval);
      }
    }, 1000)
    }
  }

  stepUpAnalytics(action) {
    this._analytics.stepUpAnalytics(action);
  }

  receptionEndVideo() {
    this.router.navigate(['/theme1/tonelayout/auditorium/'+this.audiID]);
  }
  mainaudiEndVideo() {
    this.router.navigate(['/theme1/tonelayout/multi-audi']);
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/theme1/tonelayout/photobooth']);
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/theme1/tonelayout/exhibition']);
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/theme1/tonelayout/lounge']);
  }
  gotoHelpdeskOnVideoEnd(){
    this.router.navigate(['/theme1/tonelayout/welcomedesk']);
  }
  skip(){
    let item = this.skipItem;
    if(item == 'helpdesk'){
      this.gotoHelpdeskOnVideoEnd();
    }
    if(item == 'lounge'){
      this.gotoNetworkingLoungeOnVideoEnd();
    }
  }
}
