import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-appointments',
  templateUrl: './t-one-appointments.component.html',
  styleUrls: ['./t-one-appointments.component.scss']
})
export class TOneAppointmentsComponent implements OnInit {
  token;
  appointmentList: any[] = [];
  currentMeetingLink: string;

  constructor(private _fd: FetchDataService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.token = JSON.parse(localStorage.getItem('token'));
    this.getAllScheduledAppointments();
  }

  getAllScheduledAppointments() {
    // todo userId is static for this page >> need to make it dynamic later
    this._fd.getScheduledAppointmentsForUser().subscribe((res: any) => {
      this.appointmentList = res?.result;
    });
  }

  getMeetingLinkToEmbed(channelUrl: string) {
    this.currentMeetingLink = channelUrl
  }
}
