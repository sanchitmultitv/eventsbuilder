import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-multi-exhibition',
  templateUrl: './t-one-multi-exhibition.component.html',
  styleUrls: ['./t-one-multi-exhibition.component.scss']
})
export class TOneMultiExhibitionComponent implements OnInit {

  getData: any = [];
  main_banner: any;
  main_exhibition: any = [];
  token: any;

  constructor(private _fd: FetchDataService, private router: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
  }

  ngAfterViewInit() {
    this._fd.multiExhibition(this.token).subscribe(res => {
      this.getData = res;
      this.main_banner = this.getData.result.banner
      this.main_exhibition = this.getData.result.list;
    });
  }

  gotoCommitToSucceed(exhibition, stallId) {
    this.router.navigate(['/theme1/tonelayout/auditorium', stallId]);
  }
}
