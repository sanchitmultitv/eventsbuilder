import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentsComponent } from './components/appointments/appointments.component';
import { ExhibitionComponent } from './components/exhibition/exhibition.component';
import { MultiExhibitionComponent } from './components/multi-exhibition/multi-exhibition.component';
// import { StallComponent } from './components/stall/stall.component';
import { StallsComponent } from './components/stalls/stalls.component';
import { TOneExhibitionComponent } from './t-one-exhibition.component';
import { HallsComponent } from './components/halls/halls.component'

const routes: Routes = [
  { path:'', component:TOneExhibitionComponent,
  children:[
    {path:'', redirectTo:'halls' },
    {path:'halls', component:HallsComponent},
    {path:'exhibition', component:ExhibitionComponent},
    {path: 'multi-exhibition', component: MultiExhibitionComponent},
    { path:'stalls/:id', component:StallsComponent },
    { path:'stalls/:id/appointments', component:AppointmentsComponent },
  ],
},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneExhibitionRoutingModule { }
