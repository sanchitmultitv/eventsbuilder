import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { TOneExhibitionRoutingModule } from './t-one-exhibition-routing.module';
import { TOneExhibitionComponent } from './t-one-exhibition.component';
import { StallsComponent } from './components/stalls/stalls.component';
import { ExhibitionComponent } from './components/exhibition/exhibition.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AppointmentsComponent } from './components/appointments/appointments.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MultiExhibitionComponent } from './components/multi-exhibition/multi-exhibition.component';
import { HallsComponent } from './components/halls/halls.component';
import { ExpoComponent } from './components/expo/expo.component';


@NgModule({
  declarations: [TOneExhibitionComponent,StallsComponent, ExhibitionComponent, AppointmentsComponent,MultiExhibitionComponent, HallsComponent, ExpoComponent],
  imports: [
    PdfViewerModule,
    CommonModule,
    TOneExhibitionRoutingModule,FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule
  ]
})
export class TOneExhibitionModule { }
 