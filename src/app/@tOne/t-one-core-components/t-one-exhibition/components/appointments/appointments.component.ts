import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  token;
  stalls_id;
  ExhibitionID;
  appointmentList: any[] = [];
  currentMeetingLink: string;
  constructor(private _fd: FetchDataService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.token = JSON.parse(localStorage.getItem('token'));
    this.route.paramMap.subscribe(params => {
      this.stalls_id = params.get('id');
      this.getAllScheduledAppointments(this.stalls_id);
    });
  }

  getAllScheduledAppointments(stallId) {
    this._fd.getExhibitionToken(this.token).subscribe((res: any) => {

      this.ExhibitionID = res?.result?.list[0].id;
      const date = moment(new Date()).format("YYYY-MM-DD")
      const exhibition_id = this.ExhibitionID;
      const stall_id = stallId;

      this._fd.getAllScheduledAppointments({ exhibition_id, stall_id, date }).subscribe((res: any) => {
        if (res?.result) {
          this.appointmentList = res.result;
          console.log('this.appointmentList  :::: ', this.appointmentList)
        }
      })
    });
  }

  getCurrentMeetingLink(meetlingLink: string) {
    if (meetlingLink) {
      this.currentMeetingLink = meetlingLink
    } else {
      this.currentMeetingLink = 'https://virtualapi.multitvsolution.com/videomeet/audience.html?channel_name=205_85187';
    }
  }

  closeVideoMeeting() {
    $('.videoMeetingModal').modal('hide');
  }
}
