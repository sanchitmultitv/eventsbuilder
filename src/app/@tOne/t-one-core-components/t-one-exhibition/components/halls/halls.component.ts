import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExhibitionService } from 'src/app/services/exhibition.service';

@Component({
  selector: 'app-halls',
  templateUrl: './halls.component.html',
  styleUrls: ['./halls.component.scss']
})
export class HallsComponent implements OnInit {
  totalHalls = [];
  hall_banner;
  currentHall = [];
  hallIndx =0;
  constructor(private _ES:ExhibitionService, private router:Router) { }

  ngOnInit(): void {
    this._ES.totalHalls.subscribe((res:any)=>{
      this.totalHalls = res?.result;
      this.hall_banner = res?.result[0]['banner'];
      if(this.totalHalls?.length >=1){
        this.currentHall = this.totalHalls[this.hallIndx]['exhibitions'];
        if(this.totalHalls?.length>1){
        document.getElementById('rightarr').style.display='block';
          
        }
      }
    });
  }
  gotoExhibition(id){
    const indx = this.currentHall.findIndex(res=>res.id==id);
    console.log(this.hallIndx, indx);
    this.router.navigate(['theme1/tonelayout/exhibition/multi-exhibition'], {queryParams:{"h_indx":this.hallIndx,"exb_indx":indx}});

  }
  gotoPreviousHall(){
    this.hallIndx--;
    if(this.hallIndx === 0){
      this.hall_banner = this.totalHalls[this.hallIndx]['banner'];
      this.currentHall = this.totalHalls[this.hallIndx]['exhibitions'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='none';
    }else{
      this.hall_banner = this.totalHalls[this.hallIndx]['banner'];
      this.currentHall = this.totalHalls[this.hallIndx]['exhibitions'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='block';
    }
  }
  gotoNextHall(){
    this.hallIndx++;
    if(this.totalHalls.length === (this.hallIndx+1)){
      this.hall_banner = this.totalHalls[this.hallIndx]['banner'];
      this.currentHall = this.totalHalls[this.hallIndx]['exhibitions'];
      this.hallIndx = this.totalHalls.length - 1;
      document.getElementById('rightarr').style.display='none';
      document.getElementById('leftarr').style.display='block';
    } else{
      this.hall_banner = this.totalHalls[this.hallIndx]['banner'];
      this.currentHall = this.totalHalls[this.hallIndx]['exhibitions'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='block';
    }    
  }
}
