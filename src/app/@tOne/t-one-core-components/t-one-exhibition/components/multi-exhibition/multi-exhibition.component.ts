import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ExhibitionService } from 'src/app/services/exhibition.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-multi-exhibition',
  templateUrl: './multi-exhibition.component.html',
  styleUrls: ['./multi-exhibition.component.scss']
})
export class MultiExhibitionComponent implements OnInit {
 
  getData: any = [];
  main_banner: any;
  main_exhibition: any = [];
  token: any;
  exbindx = 0;
  exhbition_hall=[];
  halIndx = 0;
  isExhbitionShow = false;
  currentExhibition = [];
  
  constructor(private _fd: FetchDataService, private router: Router, private _ES:ExhibitionService, private ad:ActivatedRoute) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');
    this.getExhibitionData();
  }

  ngAfterViewInit() {
    // this._fd.multiExhibition(this.token).subscribe((res:any) => {
    //   this.getData = res;
    //   this.main_exhibition = this.getData.result.list;      
    //   // this.main_banner = this.main_exhibition[this.exbindx]['banner'];
    //   // console.log('***', this.main_exhibition);
    //   this.main_banner = res.result.banner;
      
    // });
  }
  getExhibitionData(){
    this.ad.queryParams.subscribe((params:Params)=>{
      this._ES.totalHalls.subscribe((res:any)=>{
        this.main_exhibition = res?.result[params['h_indx']]['exhibitions'][params['exb_indx']];
        this.main_banner = this.main_exhibition?.banner;
      });
    });
    
  }
  gotoExhibiton(exb){ 
    this.isExhbitionShow = true;
    this.exbindx = this.main_exhibition.findIndex(res=>res.id == exb.id);
    console.log(exb, this.exbindx)   
    this.main_banner = this.main_exhibition[this.exbindx]['banner'];
    this.exhbition_hall = this.main_exhibition[this.exbindx]['stalls'];
    if(this.main_exhibition.length >=1){
      document.getElementById('rightarr').style.display='block';
    }
  }
  gotoPreviousHall(){
    this.exbindx--;
    if(this.exbindx === 0){
      this.main_banner = this.main_exhibition[this.exbindx]['banner'];
      this.exhbition_hall = this.main_exhibition[this.exbindx]['stalls'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='none';
    }else{
      this.main_banner = this.main_exhibition[this.exbindx]['banner'];
      this.exhbition_hall = this.main_exhibition[this.exbindx]['stalls'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='block';
    }
  }
  gotoNextHall(){
    this.exbindx++;
    if(this.main_exhibition.length === (this.exbindx+1)){
      this.main_banner = this.main_exhibition[this.exbindx]['banner'];
      this.exhbition_hall = this.main_exhibition[this.exbindx]['stalls'];
      this.exbindx = this.main_exhibition.length - 1;
      document.getElementById('rightarr').style.display='none';
      document.getElementById('leftarr').style.display='block';
    } else{
      this.main_banner = this.main_exhibition[this.exbindx]['banner'];
      this.exhbition_hall = this.main_exhibition[this.exbindx]['stalls'];
      document.getElementById('rightarr').style.display='block';
      document.getElementById('leftarr').style.display='block';
    }    
  }
  gotoStall(stallId) {
    this.router.navigate(['/theme1/tonelayout/exhibition/stalls', stallId]);
  }
}
