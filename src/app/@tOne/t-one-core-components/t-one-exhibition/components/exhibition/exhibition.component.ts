import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-exhibition',
  templateUrl: './exhibition.component.html',
  styleUrls: ['./exhibition.component.scss']
})
export class ExhibitionComponent implements OnInit {
  getData: any=[];
  main_banner:any;main_stalls:any=[];
  top: any;
  left: any;
  token:any
  etc: any=[];
  constructor(private _fd : FetchDataService,private router: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
  } 
  ngAfterViewInit(){
    this._fd.getExhibitionToken(this.token).subscribe(res => {
      this.getData = res;
      this.main_banner = this.getData.result.banner
      this.main_stalls = this.getData.result.list; 
      if (this.main_stalls.length > 1) {
        this.router.navigate(['/theme1/tonelayout/exhibition/multi-exhibition']);
      }
      //  this.etc =Object.values(this.main_stalls)
      // console.log(this.main_stalls)
      this.top = this.getData.result.list[0].pos_top
      this.left = this.getData.result.list[0].pos_left
    });
  }
  gotoCommitToSucceed(id){
    this.router.navigate(['/theme1/tonelayout/exhibition/stalls', id]);
  }
}
