import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MessageService } from 'src/app/services/message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stalls',
  templateUrl: './stalls.component.html',
  styleUrls: ['./stalls.component.scss']
})
export class StallsComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  timeVal;
  msg;
  sales_guy: any = [];
  exhibiton: any = []; banner: any; card_dropped: any; chats: any;
  documents: any = [];
  scheduled: any;
  documented: any;
  textMessage = new FormControl('');
  // msg;
  uID;
  qaList;
  exhibition_id;
  pdf;
  document;
  hidechat = false;
  documentName;
  exhibitionName;
  pdfSingle;
  // banner:any;
  roomName;
  newMessage: string;
  msgs: string;
  newMSg = [];
  myid;
  uName;
  showDiv = true;
  showchat = true; brochureURL: any; productURL: any;
  brochure: any = [];
  product: any = [];
  stalls_id: string;
  exhibition_title: any;
  salesPerson: any;
  products: any;
  token: any;
  isShow = true;
  // Exhibition_ID:any=[];
newTimeSlots:any=[];

  videosource;
  getData: any=[];
  ExhibitionID: any;

  constructor(
    private calendar: NgbCalendar, 
    private renderer: Renderer2, 
    private _fd: FetchDataService, 
    private chat: ChatService, 
    private sanitiser: DomSanitizer, 
    private messageService: MessageService, 
    private route: ActivatedRoute, 
    private router:Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')

    this.getExhibit();
    this.BrochureList();
    this.ProductList();
    this.getExhibitionId();
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this.myid = virtual.id;
    setTimeout(() => {
      this.getQA();
    }, 1000);
    this.chat.getconnect(this.token);
    this.chat.getMessages().subscribe((data => {
      let check = data.split('_');
      if (check[0] == 'one2one' && check[1] == this.myid) {
        //alert(data);
        this.getQA();
      }
    }));
    let playVideo: any = document.getElementById('playVideo');
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }
  
  getExhibitionId(){
    this._fd.getExhibitionToken(this.token).subscribe(res => {
      this.getData = res;
      this.ExhibitionID = this.getData.result[0].id
      // alert(this.ExhibitionID)
    });
  }
  

  getUser() {
    let user = JSON.parse(localStorage.getItem('virtual'));
    this.uName = user.name;
    //  console.log(this.uName);
    // let room = JSON.parse(localStorage.getItem('room_id'));
    //console.log(room);
    this.roomName = 'ddbjb1bk';
    this.chat.addUser(this.uName, this.roomName);
  }
  getQA() {
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
      // console.log('uid',data.id);
      this._fd.getanswers(this.token,data.id, this.exhibitionName).subscribe((res => {
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
    });
  }
  postQuestion(value) {
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      let data = JSON.parse(localStorage.getItem('virtual'));
      console.log('value', value);
      // this.getQA();
      if (value != undefined) {
        this._fd.askQuestions(data.id, data.name, value, this.exhibitionName, this.exhibiton.result[0].title,this.token).subscribe((res => {
          if (res.code == 1) {
            this.msg = 'Submitted Succesfully';
            // var d = $('.chat_message');
            // d.scrollTop(d.prop("scrollHeight"))
          }
          this.getQA();

          setTimeout(() => {
            $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
            this.msg = '';
            //$('.liveQuestionModal').modal('hide');
          }, 2000);
          // setTimeout(() => {
          //   this.msg = '';
          //   $('.liveQuestionModal').modal('hide');
          // }, 2000);
          this.textMessage.reset();
        }))
      }
    });

  }

  sendMessage() {
    this.roomName = 'ddbjb1bk';
    if (this.msgs != '') {
      this.chat.sendMessage(this.msgs, this.uName, this.roomName);
      this.msgs = '';
    }
  }
  // getStalls(id){
  //   this._fd.getStallsData(id).subscribe(res => { });
  // }

  getExhibit() {

    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      this._fd.getStallsData(this.token, this.exhibitionName).subscribe((res => {
        console.log('exhibition', res);
        this.exhibiton = res;
        this.banner = this.exhibiton.result[0].background_image
        // alert(this.banner)
        this.sales_guy = this.exhibiton.result[0].sales
        this.videosource = this.exhibiton.result[0].video;

        console.log('exhibition', this.banner);
        // alert(this.banner)
        // alert(this.videosource)
        this.chats = this.exhibiton.result[0].chat
        this.scheduled = this.exhibiton.result[0].schedule_call
        // this.document = res.result[0].brochure;
        this.documented = this.exhibiton.result[0].is_brochure;
        this.salesPerson = this.exhibiton.result[0].is_sales;
        this.products = this.exhibiton.result[0].is_product;
        this.exhibition_title = this.exhibiton.result[0].title;
        // alert(this.exhibition_title);
        //this.graphic = res.result[0].graphics;
        // this.banner = res.result[0].banner;
        this.card_dropped = this.exhibiton.result[0].card_drop;
        // this.product = res.result[0].product[0].url;
        //this.pord_desc =res.result[0].product[0].desc;
        // this.product = res.result[0].product[0].url
        //console.log(this.banner, 'vauvduyvuduv')

      }));
      // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
      //   console.log(res.result);
      //   this.brouchers = res.result;
      // })

    });





    // this._fd.getExhibition('Commit to Suceed').subscribe(res => {
    //   console.log('exhibition', res);
    //   this.exhibiton = res.result[0];
    //   this.documents = res.result[0].document;
    //   this.exhibition_id = res.result[0].id;
    //   localStorage.setItem('exhibitData',JSON.stringify(res.result));
    // });
  }
  BrochureList() {
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      this._fd.getBrochureList(this.token, this.exhibitionName).subscribe((res => {
        this.brochureURL = res;
        console.log(this.brochureURL.result)
        this.brochure = this.brochureURL.result

      }));
    });
  }
  ProductList() {
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      this._fd.getProductList(this.token, this.exhibitionName).subscribe((res => {
        this.productURL = res;
        console.log(this.productURL.result)
        this.product = this.productURL.result
      }));
    });
  }
  cardDropped() {
    this.route.paramMap.subscribe(params => {
      this.stalls_id = params.get('id');
      let user = JSON.parse(localStorage.getItem('virtual'));
      this.uID = user.id;
      const formData = new FormData();
      formData.append('user_id', this.uID);
      formData.append('exhibition_id', this.ExhibitionID); 

      formData.append('stall_id', this.stalls_id);


      this._fd.cardDrop(this.token, formData).subscribe((res => {
        this.toastr.success('Card Dropped Successfully !!');
      }))
    });
  }
  getDocument(prof) {
    $('.pdfModal').modal('show');
    // window.open("https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/speakers_profiles/Kenichi Ayukawa/Brief Profile - Mr. Kenichi Ayukawa.pdf","viewer"); 
    // this.document = this.sanitiser.bypassSecurityTrustResourceUrl(prof);
    // this.documentName = 'Brochure';
    // this.messageService.sendMessage(this.document);
    // console.log(prof, 'fulldocss');
    // console.log(this.document, 'docs');
  }
  closepdf() {
    $('.pdfModal').modal('hide');
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  selectToday() {
    this.model = this.calendar.getToday();
    console.log(this.model);
  }
  // closePopup() {
  // }
  getTime(event: any, valClass, time) {
    // console.log(time);
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    // adding classname 'active' to current click li
    this.renderer.addClass(event.target, valClass);
    // if (hasClass) {
    //   //alert('has')
    //       this.renderer.removeClass(event.target, valClass);
    //     } else {
    //      // alert(valClass)
    //       this.renderer.addClass(event.target, valClass);
    //     }
  }
  
  confirm() {
    // alert(this.dateModel);
 //console.log('val',dp);
    
 this.route.paramMap.subscribe(params => {
  this.exhibitionName = params.get('id');
  // alert(this.exhibitionName)
  let data = JSON.parse(localStorage.getItem('virtual'));
  console.log(this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  const CallData = new FormData();
  CallData.append('exhibition_id', this.ExhibitionID);
  CallData.append('user_id', data.id);
  CallData.append('stall_id',this.exhibitionName)
  CallData.append('time', this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  this._fd.scheduleCallGet(this.token,CallData).subscribe(res=>{
   console.log(res);
   if(res.code == 1){
    this.toastr.success( 'Call scheduled succesfully!');

    let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
    //let myDate = '2020-10-25';
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.ExhibitionID,this.exhibitionName,myDate).subscribe(res=>{
       console.log('timeresponse',res);
       this.newTimeSlots=res.result;
      //  alert(this.newTimeSlots)
       console.log(this.newTimeSlots)
     })
    setTimeout(() => {
     $('.scheduleCallmodal').modal('hide');
    }, 2000);
   }
  
 });
 
});
   }
  onDateSelect(dates){
    // alert(dates)
    console.log('aycycyt',dates.year+'-'+dates.month+'-'+dates.day);
    let myDate = dates.year+'-'+dates.month+'-'+dates.day;
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      let data = JSON.parse(localStorage.getItem('virtual'));
      this._fd.totalTimeSlots(this.ExhibitionID,this.exhibitionName,myDate).subscribe(res=>{
        console.log('timeresponse',res);
        this.newTimeSlots=res.result;
      })
    });
    

  }
  // getQA() {
  //   console.log('exhibitonid', this.exhibition_id);
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //   // console.log('uid',data.id);
  //   this._fd.getanswers(data.id, this.exhibition_id).subscribe((res => {
  //     //console.log(res);
  //     this.qaList = res.result;
  //   }))
  // }

  hidediv() {
    this.showDiv = false;
    this.showchat = false;
    this.hidechat = true;
  }
  show() {
    this.showDiv = true;
    this.hidechat = false;
    this.showchat = true;
  }


  pdfOpen(data){
    this.pdfSingle = data;
    $('.t_one_productModal').modal('show');
  }


  // postQuestion(value) {
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //   //  console.log(value, data.id);
  //   this._fd.askQuestions(data.id, value, this.exhibition_id).subscribe((res => {
  //     //console.log(res);
  //     if (res.code == 1) {
  //       this.msg = 'Submitted Succesfully';
  //       this.getQA();
  //     }
  //     setTimeout(() => {
  //       this.msg = '';
  //       $('.liveQuestionModal').modal('hide');
  //     }, 2000);
  //     this.textMessage.reset();
  //   }))
  // }
  closePopupdocs() {
    $('.docsModal').modal('hide');
    
  }
  closePopup2(){
    $('.t_one_productModal').modal('hide');

  }
  closePopup() {
    $('.scheduleCallmodal').modal('hide');
    $('.salesModal').modal('hide');
    $('.productModal').modal('hide');

  }
  openCallBox() {
    $('.scheduleCallmodal').modal('show');
  }
  closeChat() {
    $('.liveQuestionModal').modal('hide');
  }

  playGrowBySharingVideo() {
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }

  navigateToAppointments() {
    this.router.navigate(['appointments'], { relativeTo: this.route });
  }
}
