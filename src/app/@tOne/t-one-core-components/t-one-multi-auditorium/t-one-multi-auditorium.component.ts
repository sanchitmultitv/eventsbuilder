import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-multi-auditorium',
  templateUrl: './t-one-multi-auditorium.component.html',
  styleUrls: ['./t-one-multi-auditorium.component.scss']
})
export class TOneMultiAuditoriumComponent implements OnInit {
  getData: any=[];
  main_banner:any;main_audis:any=[];
  top: any;
  left: any;
  etc: any=[];
  token:any
  constructor(private _fd : FetchDataService,private router: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
    
  }
  ngAfterViewInit(){
    this._fd.multiAudi(this.token).subscribe(res => {
      this.getData = res;
       this.main_banner = this.getData.result.banner
      this.main_audis = this.getData.result.audilist;
      //  this.etc =Object.values(this.main_audis)
       console.log(this.main_audis)
      this.top = this.getData.result.pos_top
      this.left = this.getData.result.pos_left
    });
  }
  getColor(index) {
    return index%2==0?"red":"blue";
  }
  getBackGroundImageUrl(url) {
    return `url(${url})`
  }
  // gotoCommitToSucceed(id){
  //   this.router.navigate(['/theme1/tonelayout/auditorium',id]);
  // }
}
