import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-t-one-multi-auditorium-card',
    templateUrl: './t-one-multi-auditorium-card.component.html',
    styleUrls: ['./t-one-multi-auditorium-card.component.scss']
})
export class TOneMultiAuditoriumCardComponent implements OnInit {
    @Input('cardDetail') cardDetail: any;
    @Input('color') color: string;

    constructor(private router:Router) {
    }

    ngOnInit(): void {
    }

   gotoCommitToSucceed(id){
    this.router.navigate(['/theme1/tonelayout/auditorium',id]);
  }

}
