import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneSponsorsComponent } from './t-one-sponsors/t-one-sponsors.component';
import { TOneComponent } from './t-one.component';


const routes: Routes = [
  {path:'', 
  component:TOneComponent,
  children: [
    {path: 'sponsors', component: TOneSponsorsComponent},
    { path: '', redirectTo: 'logintype2', pathMatch: 'prefix' },
    // {path: 'login', loadChildren: ()=> import('./t-one-login/t-one-login.module').then(m => m.TOneLoginModule)},
    {path: 'logintype2/signup', loadChildren: ()=> import('./t-one-signup/t-one-signup.module').then(m => m.TOneSignupModule)},
    {path: 'logintype2/:token', loadChildren: ()=> import('./t-one-type2-login/t-one-type2-login.module').then(m => m.TOneType2LoginModule)},
    {path: 'tonelayout', loadChildren: ()=> import('./t-one-layout/t-one-layout.module').then(m => m.TOneLayoutModule)},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneRoutingModule { }
