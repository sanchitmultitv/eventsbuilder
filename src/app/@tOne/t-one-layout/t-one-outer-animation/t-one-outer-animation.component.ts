import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-outer-animation',
  templateUrl: './t-one-outer-animation.component.html',
  styleUrls: ['./t-one-outer-animation.component.scss']
})
export class TOneOuterAnimationComponent implements OnInit, AfterViewInit {
  getData: any=[];
  hell
  skip_button:any=0;
  token:any
  constructor(private router:Router,private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
    
  }
  skip(){
    let vid: any = document.getElementById('myVideo');
    vid.pause();
    vid.currentActiveTime = 0;
    this.router.navigate(['/theme1/tonelayout/lobby']);
  }
  ngAfterViewInit(){
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.hell = this.getData.result.lobby[0].lobby_info_video
      // this.skip_button = this.getData.result.lobby[0].animation_skip
      console.log(this.hell)
    });
    let vid:any = document.getElementById('myVideo');
    vid.play();
    let a = 0;
    let interval = setInterval(()=>{
      a+=1;
      if(a==this.getData.result.lobby[0]['skip_time']){
        this.skip_button = 1;
        clearInterval(interval);
      }
    }, 1000)
  }
  endOuterVideo(){
    this.router.navigate(['/theme1/tonelayout/lobby']);
  }

}
