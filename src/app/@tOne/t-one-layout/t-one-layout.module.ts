import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneLayoutRoutingModule } from './t-one-layout-routing.module';
import { TOneLayoutComponent } from './t-one-layout.component';
import { TOneAgendaModule } from '../t-one-shared/components';
import { TOneFeedbackModule } from '../t-one-shared/components';
import { TOneOuterAnimationComponent } from './t-one-outer-animation/t-one-outer-animation.component';
import { ScheduledCallComponent } from './scheduled-call/scheduled-call.component';
import { TOneAppointmentsComponent } from '../t-one-core-components/t-one-appointments/t-one-appointments.component';
import { TOneAttendeesComponent } from '../t-one-core-components/t-one-attendees/t-one-attendees.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TOneLayoutComponent, TOneOuterAnimationComponent, ScheduledCallComponent, TOneAppointmentsComponent, TOneAttendeesComponent],
  imports: [
    CommonModule,
    TOneLayoutRoutingModule,
    TOneAgendaModule,
    TOneFeedbackModule,
    SharedModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class TOneLayoutModule { }
