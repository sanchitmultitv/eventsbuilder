import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { fadeAnimation } from 'src/app/shared/animation/fade.animation';
import { menuItems } from '../t-one-shared/t-one-menu-items';
import { SetupServiceService } from 'src/app/services/setup-service.service';
import { ChatService } from 'src/app/services/chat.service';

declare var $: any;
@Component({
  selector: 'app-t-one-layout',
  templateUrl: './t-one-layout.component.html',
  styleUrls: ['./t-one-layout.component.scss'],
  animations: [fadeAnimation]
})
export class TOneLayoutComponent implements OnInit, AfterViewInit {
  navbarOpen = false;
  landscape = true;
  menuItems: any = menuItems;
  modal;
  token: any;
  getData: any = [];
  getData2: any = [];
  audiEna: any;
  LobbyEna: any;
  helpdeskEna: any;
  exhiEna: any;
  loungeEna: any;
  boothEna: any;
  agendaEna: any;
  audiID: any;
  showAttendeesModal:boolean = false;
  isquizStarted = false;
  constructor(private _analytics: SetupServiceService, private _fd: FetchDataService, private auth: AuthService, private router: Router, private chat: ChatService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
    this.chat.getconnect(this.token);

      this.chat.getMessages().subscribe((data) => {
        if(typeof data == 'string'){
          if (data.includes('start_quiz')){
            this.isquizStarted = true;
            console.log(data)
            setTimeout(() => {
              this.isquizStarted = false;
            }, 5000);
          }
        }
      })
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else {
      this.landscape = true;
    }
  }
  ngAfterViewInit() {
    let sidebar: any = document.getElementById('sidebar');
    window.onclick = (event) => {
      if (event.target == sidebar) {
        document.getElementById("mySidenav").style.width = "0";
        sidebar.style.width = "0";
        sidebar.style.height = "0";
        sidebar.style.backgroundColor = "transparent";
      }
    }
    this._fd.getmenuToken(this.token).subscribe(res => {
      this.getData = res;
      this.LobbyEna = this.getData.result.lobby[0].enable
      this.audiEna = this.getData.result.auditorium[0].enable
      this.helpdeskEna = this.getData.result.helpdesk[0].enable
      this.exhiEna = this.getData.result.exhibition[0].enable
      this.loungeEna = this.getData.result.lounge[0].enable
      this.boothEna = this.getData.result.others[0].photo_booth
      this.agendaEna = this.getData.result.others[0].agenda_enable

    });
  }
  checkAudi() {
    this._fd.multiAudi(this.token).subscribe(res => {
      this.getData2 = res;
      console.log(this.getData2)
      let hello = Object.keys(this.getData2.result.audilist).length
      console.log(hello)
      if (hello < 2) {
        this.audiID = this.getData2.result.audilist[0].id
        this.router.navigate(['/theme1/tonelayout/auditorium/' + this.audiID]);
      } else {
        this.router.navigate(['/theme1/tonelayout/multi-audi']);
      }
    });
  }

  logout() {
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.auth.logout(user_id).subscribe(res => {
      this.router.navigate(['/' + this.token]);
      localStorage.removeItem('virtual');
      localStorage.removeItem('eid');
      localStorage.removeItem('token');

    });
  }
  gotoAudi(){
    this.router.navigate(["/theme1/tonelayout/multi-audi"]);
    setTimeout(() => {
      this.isquizStarted = false;
    }, 1000);
  }
  openSidebar() {
    document.getElementById("mySidenav").style.width = "220px";
    document.getElementById('sidebar').style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById('sidebar').style.width = "100%";
    document.getElementById('sidebar').style.height = "100%";
  }

  closeSidebar() {
    document.getElementById("mySidenav").style.width = "0";
    let sidebar: any = document.getElementById('sidebar');
    sidebar.style.width = "0";
    sidebar.style.height = "0";
    sidebar.style.backgroundColor = "transparent";
    sidebar.style.Color = "transparent";
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  showModal(modal) {
    this.modal = modal;
    $(modal).modal('show');
  }
  stepUpAnalytics(action) {
    this._analytics.stepUpAnalytics(action);
  }
  toggleAttendeesModal(event:any) {
      if (event) {
        this.showAttendeesModal = true;
        setTimeout(() => {
          // @ts-ignore
          window.$('#attendees_modal').modal('show');
        }, 500);
      } else {
        // @ts-ignore
        window.$('#attendees_modal').modal('hide');
        setTimeout(() => {
          this.showAttendeesModal = false;
        }, 500);
      }
  }
}
