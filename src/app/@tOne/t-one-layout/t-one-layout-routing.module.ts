import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneAppointmentsComponent } from '../t-one-core-components/t-one-appointments/t-one-appointments.component';
import { TOneAttendeesComponent } from '../t-one-core-components/t-one-attendees/t-one-attendees.component';
import { TOneMultiAuditoriumComponent } from '../t-one-core-components/t-one-multi-auditorium/t-one-multi-auditorium.component';
import { TOneMultiExhibitionComponent } from '../t-one-core-components/t-one-multi-exhibition/t-one-multi-exhibition.component';
import { TOneLayoutComponent } from './t-one-layout.component';
import { TOneOuterAnimationComponent } from './t-one-outer-animation/t-one-outer-animation.component';


const routes: Routes = [
  {path:'outer', component:TOneOuterAnimationComponent},
  { path: '', component: TOneLayoutComponent,
    children: [
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      {path: 'lobby', loadChildren: ()=> import('../t-one-core-components/t-one-lobby/t-one-lobby.module').then(m => m.TOneLobbyModule)},
      {path: 'auditorium/:id', loadChildren: ()=> import('../t-one-core-components/t-one-auditorium/t-one-auditorium.module').then(m => m.TOneAuditoriumModule)},
      {path: 'photobooth', loadChildren: ()=> import('../t-one-core-components/t-one-photobooth/t-one-photobooth.module').then(m => m.TOnePhotoboothModule)},
      {path: 'sessions', loadChildren: ()=> import('../t-one-core-components/t-one-networking-lounge/t-one-sessions/t-one-sessions.module').then(m => m.TOneSessionsModule)},
      {path: 'lounge', loadChildren: ()=> import('../t-one-core-components/t-one-networking-lounge/t-one-networking-lounge.module').then(m => m.TOneNetworkingLoungeModule)},
      {path: 'welcomedesk', loadChildren: ()=> import('../t-one-core-components/t-one-welcome-desk/t-one-welcome-desk.module').then(m => m.TOneWelcomeDeskModule)},
      {path: 'exhibition', loadChildren: ()=> import('../t-one-core-components/t-one-exhibition/t-one-exhibition.module').then(m=> m.TOneExhibitionModule)},
      {path: 'appointments', component:TOneAppointmentsComponent},
      {path: 'attendees', component:TOneAttendeesComponent},
      {path: 'multi-audi', component:TOneMultiAuditoriumComponent},
      {path: 'multi-exhibition', component:TOneMultiExhibitionComponent}
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneLayoutRoutingModule { }
