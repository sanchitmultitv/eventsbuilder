import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../../services/fetch-data.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-t-one-login',
  templateUrl: './t-one-login.component.html',
  styleUrls: ['./t-one-login.component.scss']
})
export class TOneLoginComponent implements OnInit {  
    token;
    msg;
    loginForm: FormGroup
    coverImage = "../../assets/img/h-about.jpg";
    videoPlay = false;
    potrait = false;
    blockLogin = false;
    constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder: FormBuilder) { }
  
    ngOnInit(): void {
      localStorage.setItem('user_guide', 'start');
      this.loginForm = this.formBuilder.group({
        // email: ['', [Validators.email, Validators.pattern("[^ @]*@[^ @]*"), emailDomainValidator]],
        email: ['', [Validators.required]],
  
      });
      if (window.innerHeight > window.innerWidth) {
        this.potrait = true;
      } else {
        this.potrait = false;
      }
    }
    showme = false;
    clickhere() {
      this.showme = !this.showme!
      let abc: any = document.getElementById('notAllowLogin');
      if (this.showme) {
        abc.style.transition = '.5s';
        abc.style.transform = 'translateY(75%)';
      } else {
        abc.style.transition = '.5s';
        abc.style.top = '-2%';
      }
    }
  
    loggedIn() {
      const user = {
        email: this.loginForm.get('email').value,
        event_id: 64,
        role_id: 1
      };
      var isMobile = {
        Android: function () {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function () {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
  
      };
      let currentTime: any = new Date().getTime();
      let eventTime: any = new Date('December 17, 2020 11:00:00').getTime();
      // console.log(currentTime, eventTime);
      if (currentTime >= eventTime) {
        this.blockLogin = false;
        if (this.loginForm.valid) {
          this.auth.loginMethod(user).subscribe((res: any) => {
            if (res.code === 1) {
              if (isMobile.iOS()) {
                this.videoPlay = false;
                this.router.navigateByUrl('/lobby');
              }
              this.videoPlay = true;
              localStorage.setItem('virtual', JSON.stringify(res.result));
              this.videoPlay = true;
              let vid: any = document.getElementById('myVideo');
              vid.play();
              // this.router.navigateByUrl('/lobby');
  
              if (window.innerHeight > window.innerWidth) {
                this.potrait = true;
              } else {
                this.potrait = false;
              }
            } else {
              this.msg = 'Invalid Login';
              this.videoPlay = false;
              this.loginForm.reset();
            }
          }, (err: any) => {
            this.videoPlay = false;
            console.log('error', err)
          });
        }
      }
      else {
        this.blockLogin = true;
      }
    }
    @HostListener('window:resize', ['$event']) onResize(event) {
      if (window.innerHeight > window.innerWidth) {
        this.potrait = true;
      } else {
        this.potrait = false;
      }
    }
  
    endVideo() {
      // this.videoPlay = false;
      // this.potrait = false;
      this.router.navigateByUrl('/lobby');
      // let welcomeAuido: any = document.getElementById('myAudio');
      // welcomeAuido.play();
    }
  }
  
