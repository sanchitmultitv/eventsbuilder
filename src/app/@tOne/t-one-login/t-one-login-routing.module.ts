import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneLoginComponent } from './t-one-login.component';


const routes: Routes = [
  {path:'', component:TOneLoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneLoginRoutingModule { }
