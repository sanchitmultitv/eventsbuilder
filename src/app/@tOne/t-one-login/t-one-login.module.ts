import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneLoginRoutingModule } from './t-one-login-routing.module';
import { TOneLoginComponent } from './t-one-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TOneLoginComponent],
  imports: [
    CommonModule,
    TOneLoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneLoginModule { }
