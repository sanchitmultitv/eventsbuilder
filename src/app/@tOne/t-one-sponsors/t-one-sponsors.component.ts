import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-t-one-sponsors',
  templateUrl: './t-one-sponsors.component.html',
  styleUrls: ['./t-one-sponsors.component.scss']
})
export class TOneSponsorsComponent implements OnInit {

  token:any;
  getSponsorList: any=[];

  constructor(private _fd : FetchDataService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');
  }

  openNewTabForSponsor(sponsorDetail:string) {
    if(sponsorDetail!==undefined && sponsorDetail!==null && sponsorDetail!=="") {
      window.open(sponsorDetail, '_blank');
    }
  }

  ngAfterViewInit(){
    this._fd.getSponsorList(this.token).subscribe((res:any) => {
      if(res?.result && res?.code==1) {
        this.getSponsorList = res.result;
      }
    });
  }
}
