import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { RazorpayService } from 'src/app/services/razorpay.service';
import { environment } from 'src/environments/environment';

declare let Razorpay: any;

@Component({
  selector: 'app-t-one-signup',
  templateUrl: './t-one-signup.component.html',
  styleUrls: ['./t-one-signup.component.scss']
})
export class TOneSignupComponent implements OnInit {
  registerForm: FormGroup;
  main_data: any;
  token: any
  successMsg: any
  msg: any
  invalidMessage: any;
  main_logo: any;
  signin_color: any;
  button_color: any;
  logo_position: any;
  form_position: any;
  left_logo_position: any;
  top_logo_position: any;
  left_form_position: any;
  form_heading:any;
  top_form_position: any;
  main_logo_width: any;
  form_width: any;
  ticketTypeList: any[] = [];
  ticketSelected: any;
  razorPayOrderId: string;
  isTicketTypeVisible: boolean = false;
  user_id: string;
  RAZORPAY_OPTIONS = {
    amount: "",
    name: "Event Builder",
    order_id: "",
    description: "Load Wallet",
    image:
      "https://i0.wp.com/www.ecommerce-nation.com/wp-content/uploads/2019/02/Razorpay-the-new-epayment-that-will-break-everything-in-2019.png?fit=1000%2C600&ssl=1",
    prefill: {
      name: "",
      email: "",
      contact: "",
      method: ""
    },
    modal: {},
    theme: {
      color: "#0096C5"
    }
  };

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef,
    private _fd: FetchDataService,
    private razorpayService: RazorpayService,
    private formBuilder: FormBuilder) {
    let self = this;
  }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')

    this.main_data = JSON.parse(localStorage.getItem('login_data'))[0].config.reg_data.image
    this.main_logo = JSON.parse(localStorage.getItem('login_data'))[0].logo
    this.main_logo_width = JSON.parse(localStorage.getItem('login_data'))[0].logo_width
    this.signin_color = JSON.parse(localStorage.getItem('login_data'))[0].config.general.font_color
    this.button_color = JSON.parse(localStorage.getItem('login_data'))[0].config.general.button_color
    this.logo_position = JSON.parse(localStorage.getItem('login_data'))[0].config.reg_data.logo_position
    this.form_width = JSON.parse(localStorage.getItem('login_data'))[0].config.reg_data.form_width
    this.form_position = JSON.parse(localStorage.getItem('login_data'))[0].config.reg_data.form_position
    this.form_heading = JSON.parse(localStorage.getItem('login_data'))[0].config.reg_data.form_heading

    if (this.logo_position != null || undefined || '') {
      let check = this.logo_position.split('x')
      this.left_logo_position = check[0]
      this.top_logo_position = check[1]
    }
    if (this.form_position != null || undefined || '') {
      let check1 = this.form_position.split('x')
      this.left_form_position = check1[0]
      this.top_form_position = check1[1]
    }

    this._fd.getTicketTypeListing(this.token).subscribe((res: any) => {
      this.ticketTypeList = res?.result;
      this.isTicketTypeVisible = res?.visibility;
    });

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      ticket: ['', Validators.required]
    });
    this.RAZORPAY_OPTIONS['key'] = environment.RAZORPAY_API_KEY;
    console.log('this.RAZORPAY_OPTIONS >> key :::: ', this.RAZORPAY_OPTIONS['key'])
    this.razorpayService.lazyLoadLibrary("https://checkout.razorpay.com/v1/checkout.js").subscribe();
  }

  getTicketType(ticketSelected: any) {
    this.ticketSelected = ticketSelected;
    (document.getElementById(`ticket_type_${ticketSelected?.id}`) as HTMLElement).click();
    this.registerForm.controls.ticket.setValue(ticketSelected.ticket_type);
  }

  createPaymentForTicket() {
    const requestObject = {
      ticket_id: this.ticketSelected.id,
      amount: this.ticketSelected.ticket_price,
      user_id: this.user_id
    };
    this._fd.createPaymentOrderForTicket(requestObject).subscribe((success: any) => {
      this.RAZORPAY_OPTIONS["amount"] = this.ticketSelected.ticket_price + "00";
      this.razorPayOrderId = success?.order_id;
      this.RAZORPAY_OPTIONS["order_id"] = success?.order_id;
      this.RAZORPAY_OPTIONS["prefill"]['name'] = this.registerForm.get('name').value;
      this.RAZORPAY_OPTIONS["prefill"]['email'] = this.registerForm.get('email').value;
      this.RAZORPAY_OPTIONS["handler"] = this.razorPaySuccessHandlerForTicket.bind(self);
      let razorpay = new Razorpay(this.RAZORPAY_OPTIONS);
      razorpay.open();
    });
  }

  razorPaySuccessHandlerForTicket = response => {
    this.cd.detectChanges();
    const razorpay_payment_id = response.razorpay_payment_id;
    const razorpay_order_id = this.razorPayOrderId;
    const razorpay_signature = response.razorpay_signature;
    const user_id = this.user_id;
    this._fd.updateSuccessPaymentForTicket({ razorpay_payment_id, razorpay_order_id, razorpay_signature, user_id }).subscribe((successPay: any) => {
      if (successPay.status === "success") {
        let url = '/theme1/logintype2/'
        this.registerForm.reset();
        this.router.navigate([url + this.token]);
      }
    });
  }

  register() {
    const ticketTypeValue = this.registerForm.get('ticket').value;
    const formData = new FormData();
    formData.append('email', this.registerForm.get('email').value);
    // formData.append('password', this.registerForm.get('password').value);
    formData.append('name', this.registerForm.get('name').value);
    formData.append('is_paid', ticketTypeValue === "Free" ? '0' : '1');
    // formData.append('mobile', this.registerForm.get('mobile').value);
    // formData.append('company_name', this.registerForm.get('company_name').value);
    // formData.append('designation', this.registerForm.get('designation').value);
    // formData.append('category', this.registerForm.get('category').value);
    // formData.append('interests', this.registerForm.get('interests').value);
    // formData.append('token', '123');
    if (this.registerForm.valid) {
      this.auth.register(this.token, formData).subscribe((res: any) => {
        if (res.code == 1) {
          if (ticketTypeValue !== "Free") {
            this.user_id = res.id;
            localStorage.setItem('userId',this.user_id);
            this.createPaymentForTicket();
          } else {
            this.successMsg = true;
            this.msg = 'Thank You For Registering, Please Sign In To Attend The Event.';
            let url = '/theme1/logintype2/'
            this.registerForm.reset();
            this.router.navigate([url + this.token]);
            // setTimeout(() => {
            //   this.router.navigate(['/login']);
            // }, 2000);
            setTimeout(() => {
              this.successMsg = false;
            }, 2000);
          }
        }
        else {
          // alert(this.msg)
          this.invalidMessage = 'E-Mail address already registered';
          this.msg = res.result;
        }
        // alert(this.msg)
        this.invalidMessage = '';
        // this.invalidphone = 'E-Mail address or Phone no. Already registered';
        // this.registerForm.reset();
      });
    }
    // else{
    //   alert("thisis msg")
    //   this.invalidBoolean=true;
    //   this.invalidphone='';
    //   this.invalidMessage = 'All Fields are required *';
    // }
  }

  getFormValidation(formName: 'registerForm', formControlName: string, formDirSubmitted: boolean) {
    if (formName === 'registerForm') {
      if (formControlName === 'email') {
        if (this.registerForm.controls?.email?.errors?.pattern) {
          return "Email is invalid";
        } else if (this.registerForm.controls?.email?.errors?.required && formDirSubmitted) {
          return "Email is required";
        }
      }
    }
  }
}
