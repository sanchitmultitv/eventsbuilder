import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TOneSignupComponent } from './t-one-signup/t-one-signup.component';


const routes: Routes = [
  {path:'', component:TOneSignupComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TOneSignupRoutingModule { }
