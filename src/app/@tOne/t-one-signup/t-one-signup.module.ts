import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TOneSignupRoutingModule } from './t-one-signup-routing.module';
import { TOneSignupComponent } from './t-one-signup/t-one-signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TOneSignupComponent],
  imports: [
    CommonModule,
    TOneSignupRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TOneSignupModule { }
