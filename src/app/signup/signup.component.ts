import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  checkMessage = 'Please click on agree to Register';
  signupForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    company_name: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),    
    email: new FormControl('', [Validators.required, Validators.email,
      Validators.pattern("[^ @]*@[^ @]*"),
      emailDomainValidator]),
    city: new FormControl('', [Validators.required]),
  });
  // signupForm = new FormGroup({
  //   first_name: new FormControl('', [Validators.required]),
  //   email: new FormControl('', [Validators.required, Validators.email,
  //     Validators.pattern("[^ @]*@[^ @]*"),
  //     emailDomainValidator]),
  //   job_title: new FormControl('', [Validators.required]),
  // });

  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  token:any
  colr;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService) { }

  ngOnInit(): void {

    function getTimeRemaining(endtime) {
      let date:any=new Date();
      const total:any = Date.parse(endtime) - Date.parse(date);
      const seconds = Math.floor((total / 1000) % 60);
      const minutes = Math.floor((total / 1000 / 60) % 60);
      const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
      const days = Math.floor(total / (1000 * 60 * 60 * 24));

      return {
        total,
        days,
        hours,
        minutes,
        seconds
      };
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const daysSpan:any = clock.querySelector('.days');
      const hoursSpan = clock.querySelector('.hours');
      const minutesSpan = clock.querySelector('.minutes');
      const secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        const t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }

    //const deadline = new Date(Date.parse(new Date()) + 29 * 24* 60 * 60 * 1000);
    const deadline = new Date('December 17, 2020 11:00:00');
    initializeClock('clockdiv', deadline);
  }

  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  isChecked(event){
    this.checked = !this.checked;
  }
  register() {
    const formData = new FormData();
    formData.append('name', this.signupForm.get('first_name').value);
    formData.append('email', this.signupForm.get('email').value);
    formData.append('headquarter', this.signupForm.get('job_title').value);
    // formData.append('token', '123');
    if (this.checked) {
      this._auth.register(this.token,formData).subscribe((res:any) => {

        if(res.code ==1){
          this.msg = 'Thank You For Registering, Please Sign In To Attend The Event.';
          // setTimeout(() => {
          //   this.router.navigate(['/login']);
          // }, 2000);
        }
        else{
          this.msg = res.result;
        }
        this.signupForm.reset();
      });
    } else {

    }
  }
}
