import { Component, OnInit, Renderer2, ViewChild, ElementRef, Output, HostListener, AfterViewInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../shared/animation/fade.animation';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';
import { AuthService } from '../services/auth.service';
import { FetchDataService } from '../services/fetch-data.service';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
import { ChatService } from '../services/chat.service';
declare var $: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimation]

})
export class LayoutComponent implements OnInit, AfterViewInit, OnDestroy {
  firstName;
  landscape = true;
  actives: any = [];
  activeAuditoriums = [];
  layoutmorning: boolean;
  notify=[];
  message: any = null;
  subscription: Subscription;
  aaa=[];
  livemsg=false;
  timer=0;
  cleartimer;
  showtimer=false;
  constructor(public router: Router, private renderer: Renderer2, private msg: MessageService, private auth: AuthService, private _fd: FetchDataService, private chat:ChatService) { 
    this.subscription = this.msg.getMessage().subscribe(message => {
      if (message) {
        let msg=message.text + ' has sent you message';
        this.notify.push(msg);
        setTimeout(() => {
          this.notify.splice(0, 1);
        }, 10000);
      } else {
        // clear messages when empty message received
        this.notify = null;
      }
    });
  }

  ngOnInit(): void {
   
    this.audiActive();
    $('.videoData').show();
    this.firstName = JSON.parse(localStorage.getItem('virtual')).name;
    this.playAudio();
    this.msg.getNotification().subscribe((data => {
      // console.log('text', data);
      this.notify = data;
    }));
    
  }
  getDatebg() {
    let date: any = new Date().getHours();
    let hour: any = date;
    if ((hour >= 6) && (hour < 18)) {
      this.layoutmorning = true;
    }
    if ((hour >= 18) && (hour < 6)) {
      this.layoutmorning = false;
    }

  }
  ngAfterViewInit() {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
    this.getDatebg();
    this.chat.getconnect('toujeo-64');
    this.chat.getMessagesQnA().subscribe((data => {
      if (data === 'start_live') {
        this.livemsg = true;
      }
      if (data === 'stop_live') {
        this.livemsg = false;
      }

    }));
    // this.getNotificationIframe();
  }
  getNotificationIframe(){
    this.chat.getconnect('5fb4b1b07f9d8');
    this.chat.getIframeConnection().subscribe(((data: any) => {
      let breakdata:any = data.split('*');
      let email:any = breakdata[0];
      let keyC:any = breakdata[1];
      localStorage.setItem('iframeKey', keyC);
      let emailCheck:any = JSON.parse(localStorage.getItem('virtual')).email;
      if ((email === emailCheck)&&(keyC !== 'exit')) {        
        this.startTimer();
      }
    }));
  }
  startTimer() {
    this.timer = 10;
    this.cleartimer = setInterval(() => {
      this.timer--;
      if (this.timer === 0) {
        clearInterval(this.cleartimer);
        this.showtimer=false;
        this.router.navigate(['/ifaudi']);
      }
    }, 1000);
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  showModal = false;
  toggleModal() {
    this.showModal = !this.showModal;
  }

  readOutputValueEmitted(event) {
    console.log('kkkk', event)
  }

  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  playAudio() {
    let abc: any = document.getElementById('myAudio');
    abc.play();
  }
  constraints = {
    video: {
      facingMode: "environment",
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };
  audis = ['Audi 1', 'Audi 2'];
  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      console.log(res, 'resssss');
      this.actives = res.result;
      this.activeAuditoriums = res.result;
      console.log(this.actives[0]);
    })
  }
  gotoAudi(audi) {
    if (audi === 1) {
      localStorage.setItem('serdia_room', 'myanmar_16');
      if (this.actives[3].status == true) {
        this.router.navigate(['/auditorium/one']);
      }
      else {
        $('.audiModal').modal('show');
      }
    }
    if (audi === 2) {
      localStorage.setItem('serdia_room', 'myanmar_17');
      if (this.actives[2].status == true) {
        this.router.navigate(['/auditorium/two']);
      }
      else {
        $('.audiModal').modal('show');
      }
    }
    if (audi === 3) {
      localStorage.setItem('serdia_room', 'myanmar_18');
      if (this.actives[1].status == true) {
        this.router.navigate(['/auditorium/three']);
      }
      else {
        $('.audiModal').modal('show');

      }
    }
    if (audi === 4) {
      localStorage.setItem('serdia_room', 'myanmar_19');
      // this.router.navigate(['/auditorium/four']);
      if (this.actives[0].status == true) {
        this.router.navigate(['/auditorium/fourth-auditorium']);
      }
      else {
        $('.audiModal').modal('show');
      }

    }
  }

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    if (virtual.company===null || virtual.company===undefined || virtual.company===''){
      formData.append('company', 'others');
    }
    else{
      formData.append('company', virtual.company);
    }
    if (virtual.designation===null || virtual.designation===undefined || virtual.designation===''){
      formData.append('designation', 'others');
    }
    else{
      formData.append('designation', virtual.designation);
    }

    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }

  stream: any;
  attachVideo(stream) {
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  img;
  capture() {
    this.showImage = true;
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 5;
    var img = document.getElementById("logo");
    context.drawImage(img, 5, 5, 120, 50);
    context.strokeRect(0, 0, canvas.width, canvas.height);
    let imgPath = '../../../assets/img/xtrem.jpeg';
    let imgObj = new Image();

    imgObj.src = imgPath;
    this.img = canvas.toDataURL("image/png");
    console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }
  stopVideo() {
    this.renderer.listen(this.videoElement.nativeElement, 'stop', (event) => {
      console.log('dsfj')
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }

  closeCamera() {
    location.reload();
    $('.capturePhoto').modal('hide');

  }

  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }

  openSidebar() {
    document.getElementById("mySidenav").style.width = "220px";
  }

  closeSidebar() {
    document.getElementById("mySidenav").style.width = "0";
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else {
      this.landscape = true;
    }
  }
  logout() {
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.auth.logout(user_id).subscribe(res => {
      this.router.navigate(['/login']);
      localStorage.clear();
    });
  }
  ngOnDestroy(){
    clearInterval(this.cleartimer);
  }
}
