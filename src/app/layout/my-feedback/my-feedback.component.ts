import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import {FetchDataService} from '../../services/fetch-data.service'

declare var $: any;

@Component({
  selector: 'app-my-feedback',
  templateUrl: './my-feedback.component.html',
  styleUrls: ['./my-feedback.component.scss']
})
export class MyFeedbackComponent implements OnInit {
  ratingForm = new FormGroup({
    agenda: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  show=false;

  ratingList:any = [];
  ratingActual = [];
  feedbackList:any = [];
  feedbackFormActualData = [];
  holy: any;
  //  event_id = 52;
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {

    this._fd.getRating().subscribe(res => {
      this.ratingList = res.result;

      console.log('response check', this.ratingList)
    })




    this._fd.getFeedback().subscribe(res => {
      this.feedbackList = res.result;
      console.log('check respons', this.feedbackList);

    })

  }


  closePopup() {
    $('.feebackModal').modal('hide');
  }


  radioValue(rateValue, radio) {
    let rateingValue = rateValue
    let ratingID = radio
    let rateingData = { 'rating_id': ratingID, 'rating': rateingValue }
    let index = this.ratingActual.findIndex(data=>data.rating_id==ratingID)
    if(index == -1){
      this.ratingActual.push(rateingData);
    }else{
      this.ratingActual.splice(index,1)
      this.ratingActual.push(rateingData);
    }
  }
  

  submitFeedback() {
    // this.holy = JSON.parse(localStorage.getItem('whatever'));
    // console.log(this.holy);
    if (localStorage.getItem('whatever')) {
      $('.feebackModal').modal('hide');
      alert('Feedback already submitted');
      this.ratingForm.reset();
    } else {
      let userID: any = JSON.parse(localStorage.getItem('virtual'));
      let agenda = (<HTMLInputElement>document.getElementById("agenda")).value;

      // let registration= (<HTMLInputElement>document.getElementById("registration")).value;
      // let event= (<HTMLInputElement>document.getElementById("event")).value;

      let formDataNew: any = this.ratingActual

      let data: any = [{ 'feedback_id': 1, 'feedback': agenda }];

      const formData = new FormData();
      formData.append('rating', JSON.stringify(formDataNew));
      formData.append('user_id', userID.id);
      console.log('rating data', formData)
      this._fd.postRating(formData).subscribe((res: any) => {
        if (res.code === 1) {
          $('.feebackModal').modal('hide');
          
        }
      })

      const newFormData = new FormData();
      newFormData.append('feedback', JSON.stringify(data));
      newFormData.append('user_id', userID.id);

      console.log('feedback check', newFormData);

      this._fd.postFeedback(newFormData).subscribe((res: any) => {
        if (res.code === 1) {
          $('.feebackModal').modal('hide');
          // this.ratingForm.reset();
          // alert('Your Feedback Successfully Submitted');
        }
      })
       this.ratingForm.reset();
       this.feedbackList=[];
       this.ratingList=[];
       this.show=true;
      localStorage.setItem('whatever', 'something');
    }
  }
}
