import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupChatThreeComponent } from './group-chat-three.component';

describe('GroupChatThreeComponent', () => {
  let component: GroupChatThreeComponent;
  let fixture: ComponentFixture<GroupChatThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupChatThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupChatThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
