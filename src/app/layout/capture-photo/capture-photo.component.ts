import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter, HostListener } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-capture-photo',
  templateUrl: './capture-photo.component.html',
  styleUrls: ['./capture-photo.component.scss']
})
export class CapturePhotoComponent implements OnInit {
  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  liveMsg = false; 
  camstream: any;
  img;
  dataurl;
  bgimgs = [
    '../../../assets/philips/morningPhotoboth.jpg',
    '../../../assets/philips/eveningPhotoboth.jpg',
  ]
  bgImage;
  takeSnapBg;
  bgmorning:boolean;
  constructor(private renderer: Renderer2, private _fd: FetchDataService, private chat: ChatService) { }
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  ngOnInit(): void {

    // this.startCamera();
    $('.videoData').show();
    this.chat.getconnect('toujeo-52');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));
    let modal = document.getElementById("myModal");
    window.onclick = (event) => {
      if (event.target == modal) {
        this.closeCamera();
      }
    }
    this.getDatebg();
  }

  getDatebg() {
    let date:any = new Date().getHours();
    let hour:any= date;
    if((hour>=6)&&(hour<16)){
      this.bgImage=this.bgimgs[0];
      this.takeSnapBg='../../../assets/philips/morningFrame.png';
      this.bgmorning=true;
    }else{
      this.bgImage=this.bgimgs[1];
      this.takeSnapBg='../../../assets/philips/eveningFrame.png';
      this.bgmorning=false;
    }

  }
  openCapturePhotoModal() {
    $('.capturePhotoModal').modal('show');
    this.startCamera();
  }
  constraints = {
    facingMode: { exact: 'environment' },
    video: {
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stream: any;
  attachVideo(stream) {
    this.camstream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', this.camstream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  stopStream() {
    if (null != this.camstream) {

      var track = this.camstream.getTracks()[0];

      track.stop();
      this.videoElement.nativeElement.load();

      this.camstream = null;
    }
  }
  capture() {
    this.showImage = true;
    let vid: any = document.getElementById("vid");

    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    if(this.bgmorning){
      this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, vid.videoWidth/3.95, vid.videoHeight/5, vid.videoWidth/2, vid.videoHeight/2);
    }else{
      this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, vid.videoWidth/3.95, vid.videoHeight/3.9, vid.videoWidth/2, vid.videoHeight/2);

    }
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 2;
    
    let watermark = new Image();
    let headerimg = new Image();
    context.beginPath();
    context.lineWidth = 4;
    context.strokeStyle = "#fff";
    context.fillStyle = "#FFFFFF";
    let btmRect = vid.videoHeight - 50;
    context.stroke();
    context.beginPath();
    // watermark.src = '../../../assets/serdia_myanmar/images/btm.png';
    // context.drawImage(watermark, 0, (btmRect));
    headerimg.src = this.takeSnapBg;
    // context.drawImage(watermark, 0, btmRect, vid.videoWidth, 52);
    context.drawImage(headerimg, 0, 0, vid.videoWidth, vid.videoHeight);
    this.dataurl = canvas.toDataURL('mime');
    // this.img = canvas.toDataURL("image/png");
    this.img = canvas.toDataURL("image/jpeg", 0.7);
    // console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }

  uploadVideo() {
    console.log('test');
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let user_name = JSON.parse(localStorage.getItem('virtual')).name;
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('user_name', user_name);
    formData.append('image', this.img);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res)
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }

  closeCamera() {
    // location.reload();
    this.stopStream();
    $('.capturePhotoModal').modal('hide');

  }
  reload() {
    this.showImage = false;
    this.startCamera();
  }
  @HostListener('keydown', ['$event']) onKeyDown(key) {
    if (key.keyCode === 27) {
      this.closeCamera();
    }
  }
}
