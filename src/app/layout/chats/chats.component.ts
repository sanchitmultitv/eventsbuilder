import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { coreSelectorArr, coreBusinessArr } from '../../shared/attendeesforsearch';
import { MessageService } from 'src/app/services/message.service';
declare var $: any;
@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit, OnDestroy {
  commentsList = [];
  commentsListing = [];
  textMessage = new FormControl('');
  type = new FormControl('');
  interval;
  typ = 'normal';
  chatMessage = [];
  oneToOneChatList = [];
  allChatList = [];
  searchChatList = [];
  allChatIndex = -1;
  sender_id: any;
  sender_name;
  receiver_id: any;
  receiver_name;
  coreSelectorArr: any = coreSelectorArr;
  coreBusinessArr: any = coreBusinessArr;
  core_sector = new FormControl('');
  core_business = new FormControl('');
  token:any;

  constructor(private _fd: FetchDataService, private chat: ChatService, private msg:MessageService) { }
  @ViewChild('inputKey', { static: true }) inputkey: ElementRef;
  ngOnInit(): void {
    this.token = localStorage.getItem('token')
    this.core_sector.setValue(this.coreSelectorArr[0].name);
    this.core_business.setValue(this.coreBusinessArr[0].name);
    this.chat.getconnect('toujeo-60');
    this.chat.getb2bMessages().subscribe(((data:any) => {
      let notify=data.split('_');
      let notifyId=parseInt(notify[1]);
      if(this.sender_id===notifyId){
        this.msg.sendMessage(notify[2]);      
      }
      this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
        console.log('socketdata', res);

      });
    }));

    this.getAllAttendees();
    
    // this.chat.getconnect('toujeo-52');
    // this.chat.getMessages().subscribe((data=>{
    //     let getMsg = data.split('_');
    //     // console.log('chats',getMsg);
    //     // if(getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one"){
    //     //   let data = JSON.parse(localStorage.getItem('virtual'));
    //     //   if(getMsg[3]== data.id){
    //     //     // this.ChatMsg = true;
    //     //     this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     //       this.chatMessage = res.result;
    //     //     });
    //     //   }
    //     // }

    // }))
this.getStatusChat();
  }

  getStatusChat(){
  this.chat.getconnect('toujeo-60');
  this.chat.getStatusMessages().subscribe(data => {
    this.chatMessage=[];
    this._fd.getAttendees(62).subscribe((res:any) => {
        this.allChatList = res.result;
      });
  });
}
  getAllAttendees() {
    let event_id = 62;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    
    this._fd.getAttendees(event_id).subscribe((res: any) => {
      // console.log('chatlist', res.result)
      this.allChatList = res.result;
      this.searchChatList = res.result;
      this.receiver_id = res.result[0].id;
      this.receiver_name = res.result[0].name;
      // this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //   this.chatMessage = res.result;
      // });

      // this.timer = setInterval(() => {
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      // }, 150000);
      // this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //   this.chatMessage = res.result;
      // });

    });
  }
  searchElement(query) {
    let event_id = 62;    
      if(query!==''){
        this._fd.getAttendeesbyName(event_id, query).subscribe(res => {
          this.allChatList = res.result;
        });
    }else {
      this._fd.getAttendees(event_id).subscribe(res => {
        this.allChatList = res.result;
      });
    }
  }

  selectedChat(chat, ind) {
    this.allChatIndex = ind;
    let event_id = 62;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.id;
    this.receiver_name = chat.name;
    this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }
  timer;
  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null) {
      this._fd.postOne2oneChat(this.token,formData).subscribe(data => {
        this.textMessage.reset();
        this._fd.enterTochatList(this.token,this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;
        });
      });
    }
    // setTimeout(() => {
    //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
    // }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }

  getComments() {
    let event_id = 62;
    let user_id = 1;
    let type = 'normal';
    this._fd.getComments(event_id, user_id, type).subscribe(res => {
      this.commentsList = res.result;
      this.commentsListing = res.result;
    });
  }
  getType(value) {
    console.log(value);
    let event_id = 62;
    let user_id = 1;
    this.typ = value;
    this._fd.getComments(event_id, user_id, value).subscribe(res => {
      this.commentsList = res.result;
    })
  }
  closePopup() {
    $('.chatsModal').modal('hide');
  }

  // postComment(text) {
  //   let data: any = JSON.parse(localStorage.getItem('virtual'));
  //   const formData = new FormData();
  //   formData.append('event_id', '123');
  //   formData.append('user_id', '1');
  //   formData.append('name', data.name);
  //   formData.append('comment', text);
  //   formData.append('type', this.typ);
  //   this._fd.postComments(formData).subscribe(res => {
  //     console.log(res);
  //   })
  //   this.textMessage.reset();
  // }

  ngOnDestroy() {
    //clearInterval(this.interval);
    clearInterval(this.timer);
  }

  form = new FormGroup({
    core_sector: new FormControl(''),
    core_business: new FormControl(''),
    country: new FormControl('')
  });
  onSubmit() {
    this.form.patchValue({
      country: 'india'
    });
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    if (this.form.invalid) {
      return;
    } else {
      // this._fd.getReinvestAttendees(61, this.form.value.core_sector, this.form.value.core_business).subscribe((res: any) => {
      //   this.allChatList = res.result;
      //   this.receiver_id = res.result[0].id;
      //   this.receiver_name = res.result[0].name;
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      //   console.log('', res.result)
      //   this.form.reset();
      // });
    }

  }
  coreSectorvalue = null;
  coreBusinessvalue = null;
  onSelectChange(value, core) {
    this.allChatIndex = -1;
    let event_id=62;
    this.chatMessage=[];
    this.allChatList=[];
    if(core==='csect'){
      this.coreSectorvalue=this.core_sector.value;
    }
    if(core==='cbusi'){
      this.coreBusinessvalue=this.core_business.value;
    }    
    console.log('vlaue', value, core, this.coreSectorvalue, this.coreBusinessvalue);
    // if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
    //   this._fd.getReinvestAttendeesCoreSector(event_id, this.coreSectorvalue).subscribe((res:any) => {
    //     this.allChatList = res.result;
    //   });
    // }
    // if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
    //   this._fd.getReinvestAttendeesCoreBusiness(event_id,this.coreBusinessvalue).subscribe((res:any) => {
    //     this.allChatList = res.result;
    //   });
    // }
    // if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
    //   this._fd.getReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue).subscribe((res:any) => {
    //     this.allChatList = res.result;
    //   });
    // }
    this.inputkey.nativeElement.value = '';
  }
  clearAll() {
    this.coreSectorvalue = null;
    this.coreBusinessvalue = null;
    this.ngOnInit();
  }
}
