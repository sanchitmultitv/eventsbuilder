import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-exhibit-life',
  templateUrl: './exhibit-life.component.html',
  styleUrls: ['./exhibit-life.component.scss']
})
export class ExhibitLifeComponent implements OnInit {
  liveMsg=false;
  constructor( private router: Router, private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
      if(data == 'start_live'){
        this.liveMsg = true;
      }
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
     
    }));
  }
  gotoCare(){
 this.router.navigate(['/exhibitionHall/care']);
  }
  gotoCommitToSucceed(id){
    this.router.navigate(['/exhibitionHall/commitToSucceed', id]);
  }
  gotoDareToInnovative(id){
    this.router.navigate(['/exhibitionHall/dareToInnovative', id]);
  }
  gotoGrowBySharing(){
    this.router.navigate(['/exhibitionHall/growBySharing']);
  }
}
