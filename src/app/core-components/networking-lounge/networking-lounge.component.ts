import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  videoEnd=false;
  liveMsg= false;
  ChatMsg = false;
  senderName;
  videoPlayer = '../assets/video/networking_lounge_video.mp4';
  allNames=[{name:'Anusha Suryanarayana'},
    {name:'Atul Srivastava'},                                                
    {name:'C Arun Kumar'},
    {name:'Dibyendu Raychaudhury'},
    {name:'G Kannan'},
    {name:'Girish Kumar Chawla'},
    {name:' Munish Peshin'},
    {name:'Nikhil Gupta'},
    {name:'Vinay Jha'},
    {name:'Nitin Agrawal'},
    {name:'Nitin Harjai'},
    {name:'Nitin Mittal'},
    {name:'Sukanto'},                                                     
    {name:'Sumit Joshi'},
    {name:'Tankeswar Baishya'},     
    {name:'Shankaranarayanan V'},                                     
    {name:'Vikas Malhotra'},
    ];
  constructor(private router: Router, private chat: ChatService) { }
  ngOnInit(): void {
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
      if(data == 'start_live'){
        this.liveMsg = true;
      }
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
      else{
        let getMsg = data.split('_');
        console.log('chats',getMsg);
        if(getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one"){
          let data = JSON.parse(localStorage.getItem('virtual'));
          if(getMsg[3]== data.id){
            this.senderName = getMsg[4];
            this.ChatMsg = true;
            setTimeout(() => {
              this.ChatMsg = false;
            }, 5000);
          }
        }
      }
    }));
    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime=0;
        pauseVideo.pause();
      }
    }
  }
  showVideoPopup(){
    $('#playVideo').modal('show');
    let player: any = document.getElementById("video");
    player.play();
  }
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime=0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  videoEnded() {
    this.videoEnd = true;
  }
  openWtsapp(){
    $('.wtsappModal').modal('show');
  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  openCamera(){
    this.router.navigate(['/capturePhoto']);
  }

}
