import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
galleryimgs=[
  { morningImg: 'assets/lutron/Winner/1.jpg', eveningImg: 'assets/lutron/Winner/evening/1.jpg'},
  { morningImg: 'assets/lutron/Winner/2.jpg', eveningImg: 'assets/lutron/Winner/evening/2.jpg'},
  { morningImg: 'assets/lutron/Winner/3.jpg', eveningImg: 'assets/lutron/Winner/evening/3.jpg'},
  { morningImg: 'assets/lutron/Winner/4.jpg', eveningImg: 'assets/lutron/Winner/evening/4.jpg'},
  { morningImg: 'assets/lutron/Winner/5.jpg', eveningImg: 'assets/lutron/Winner/evening/5.jpg'},
  { morningImg: 'assets/lutron/Winner/6.jpg', eveningImg: 'assets/lutron/Winner/evening/6.jpg'},
  { morningImg: 'assets/lutron/Winner/7.jpg', eveningImg: 'assets/lutron/Winner/evening/7.jpg'},
  { morningImg: 'assets/lutron/Winner/8.jpg', eveningImg: 'assets/lutron/Winner/evening/8.jpg'},
  { morningImg: 'assets/lutron/Winner/9.jpg', eveningImg: 'assets/lutron/Winner/evening/9.jpg'},
  { morningImg: 'assets/lutron/Winner/10.jpg', eveningImg: 'assets/lutron/Winner/evening/10.jpg'},
  { morningImg: 'assets/lutron/Winner/11.jpg', eveningImg: 'assets/lutron/Winner/evening/11.jpg'},
  { morningImg: 'assets/lutron/Winner/12.jpg', eveningImg: 'assets/lutron/Winner/evening/12.jpg'},
  { morningImg: 'assets/lutron/Winner/13.jpg', eveningImg: 'assets/lutron/Winner/evening/13.jpg'},
  { morningImg: 'assets/lutron/Winner/14.jpg', eveningImg: 'assets/lutron/Winner/evening/14.jpg'},
  { morningImg: 'assets/lutron/Winner/15.jpg', eveningImg: 'assets/lutron/Winner/evening/15.jpg'},
  { morningImg: 'assets/lutron/Winner/16.jpg', eveningImg: 'assets/lutron/Winner/evening/16.jpg'},
  { morningImg: 'assets/lutron/Winner/17.jpg', eveningImg: 'assets/lutron/Winner/evening/17.jpg'},
  { morningImg: 'assets/lutron/Winner/18.jpg', eveningImg: 'assets/lutron/Winner/evening/18.jpg'},
  { morningImg: 'assets/lutron/Winner/19.jpg', eveningImg: 'assets/lutron/Winner/evening/19.jpg'},
  { morningImg: 'assets/lutron/Winner/20.jpg', eveningImg: 'assets/lutron/Winner/evening/20.jpg'},
  { morningImg: 'assets/lutron/Winner/21.jpg', eveningImg: 'assets/lutron/Winner/evening/21.jpg'},
  { morningImg: 'assets/lutron/Winner/22.jpg', eveningImg: 'assets/lutron/Winner/evening/22.jpg'},
  { morningImg: 'assets/lutron/Winner/23.jpg', eveningImg: 'assets/lutron/Winner/evening/23.jpg'},
  { morningImg: 'assets/lutron/Winner/24.jpg', eveningImg: 'assets/lutron/Winner/evening/24.jpg'},
  { morningImg: 'assets/lutron/Winner/25.jpg', eveningImg: 'assets/lutron/Winner/evening/25.jpg'},
];
profleImage;
  constructor() { }

  ngOnInit(): void {
  }
  showProfile(profile:any){
    let currentTime: any = new Date().getTime();
    let eventTime: any = new Date('December 17, 2020 18:00:00').getTime();
    if(currentTime>=eventTime){
      this.profleImage=profile.eveningImg;
    } else {
      this.profleImage=profile.morningImg;
    }
    $('.profiledata').modal('show');
  }
  closeProfile(){
    $('.profiledata').modal('hide');
  }
}
