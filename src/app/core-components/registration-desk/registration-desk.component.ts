import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration-desk',
  templateUrl: './registration-desk.component.html',
  styleUrls: ['./registration-desk.component.scss']
})
export class RegistrationDeskComponent implements OnInit {
  videoEnd=false;
  constructor() { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
}
