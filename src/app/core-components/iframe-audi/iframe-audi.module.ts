import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IframeAudiRoutingModule } from './iframe-audi-routing.module';
import { IframeAudiComponent } from './iframe-audi.component';


@NgModule({
  declarations: [IframeAudiComponent],
  imports: [
    CommonModule,
    IframeAudiRoutingModule
  ]
})
export class IframeAudiModule { }
