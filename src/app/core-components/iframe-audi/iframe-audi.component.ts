import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-iframe-audi',
  templateUrl: './iframe-audi.component.html',
  styleUrls: ['./iframe-audi.component.scss']
})
export class IframeAudiComponent implements OnInit, AfterViewInit {
  url;
  constructor(private chatService: ChatService, private fd: FetchDataService, private router: Router) { }

  ngOnInit(): void {
    var ckey = localStorage.getItem('iframeKey');
    var sname = JSON.parse(localStorage.getItem('virtual')).name;
    this.getcall(sname, ckey);
    this.chatService.getconnect('5fb4b1b07f9d8');
    this.chatService.getIframeConnection().subscribe(((data: any) => {
      var breakdata = data.split('*');
      var email = breakdata[0];
      var exit = breakdata[1];
      let emailCheck: any = JSON.parse(localStorage.getItem('virtual')).email;
      if ((email === emailCheck) && (exit === "exit")) {
        this.callstart(0);
      }
    }));  

  }

  ngAfterViewInit() {
    this.callstart(1);
  }

  callstart(flag) {
    // let userid:any;
    // if(flag===1){
    //   userid=JSON.parse(localStorage.getItem('virtual')).id;
    // }else{
    //   userid='';
    // }
    const formData = new FormData();
    formData.append('user_id', JSON.parse(localStorage.getItem('virtual')).id);
    formData.append('flag', flag);
    this.fd.disconnectCall(formData).subscribe(res => {
      if (flag === 0) {
        this.router.navigate(['/auditorium/one']);
      } else {
        return true;
      }
    });

  }

  getcall(name, key) {
    let url = 'https://www.vmixcall.com/call.aspx?Key=' + key + '&Name=' + name; //..get url from the textbox and load that url
    var iframe =
      '<iframe width="100%" height="550" allow="camera *;microphone *" src="' +
      url +
      '" frameborder="0" allowfullscreen=""></iframe>';
    $('#maincall').html(iframe);
    return false; // prevent the form from being submitted
  }
}
