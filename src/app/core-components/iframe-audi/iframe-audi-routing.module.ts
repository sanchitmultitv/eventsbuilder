import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IframeAudiComponent } from './iframe-audi.component';


const routes: Routes = [
  {path:'', component:IframeAudiComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IframeAudiRoutingModule { }
