import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  showPoster = false;
  // videoUrl;
  bgimgs = [
    'assets/philips/morningLobby.jpg',
    'assets/philips/eveningLobby.jpg',
  ]
  bgImage;
  bgmorning: boolean;
  currentDate = new Date();
  videoPlayer = 'https://lutron.multitvsolution.com/assets/lutron/video/lutronAV.mp4';

  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
  ngOnInit(): void {
 
    // if (localStorage.getItem('user_guide') === 'start') {
    //   this.getUserguide();
    // }
    this.stepUpAnalytics('lobby');
    this.audiActive();
    this.chat.getconnect('toujeo-64');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));

    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
    this.getDatebg();
   
  }
  getDatebg() {
    let date: any = new Date().getHours();
    let hour: any = date;
    if ((hour >= 6) && (hour < 18)) {
      this.bgImage = this.bgimgs[0];
      this.bgmorning = true;
    } else {
      this.bgImage = this.bgimgs[1];
      this.bgmorning = false;
    }

  }
  endPlayscreen() {
    this.showPoster = true;
  }
  playScreenOnImg() {
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();
  }
  ngAfterViewInit() {
    this.playAudio();
  }
  openModalVideo() {
    $('#playVideo').modal('show');
    let vid: any = document.getElementById("video");
    vid.play();
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  closePopup() {
    $('.yvideo').modal('hide');
  }
  openpopup() {
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    } else {
      $('.feebackModal').modal('show');
    }
  }

  closePopuptwo() {
    $('.NoFeedback').modal('hide');
  }

  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerWidth <= 767) {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });

    } else {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });
    }
  }
  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    this.receptionEnd = true;
    this.showVideo = true;
    let vid: any = document.getElementById("recepVideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  receptionEndVideo() {
    this.router.navigate(['/welcome']);

  }
  gotoKeyPortfolio() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  onEndKeyPortfolio() {
    this.router.navigate(['/keyportfolio']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    this.auditoriumLeft = true;
    this.showVideo = true;
    let vid: any = document.getElementById("audLeftvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    // if (this.actives[3].status == true) {
    //   this.auditoriumLeft = true;
    //   this.showVideo = true;
    //   let vid: any = document.getElementById("audLeftvideo");
    //   vid.play();
    //   let pauseVideo: any = document.getElementById("video");
    //   pauseVideo.currentTime = 0;
    //   pauseVideo.pause();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/auditorium/one']);
  }
  playAuditoriumRight() {
    if (this.actives[2].status == true) {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
      let pauseVideo: any = document.getElementById("video");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
    }
    else {
      $('.audiModal').modal('show');
    }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/two']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();

  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/exhibitionHall']);
  }
  // playRegistrationDesk() {
  //   this.registrationDesk = true;
  //   this.showVideo = true;
  //   let vid: any = document.getElementById("regDeskvideo");
  //   vid.play();
  // }
  // gotoRegistrationDeskOnVideoEnd() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();

  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    if (virtual.company===null || virtual.company===undefined || virtual.company===''){
      formData.append('company', 'others');
    }
    else{
      formData.append('company', virtual.company);
    }
    if (virtual.designation===null || virtual.designation===undefined || virtual.designation===''){
      formData.append('designation', 'others');
    }
    else{
      formData.append('designation', virtual.designation);
    }
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }

  getUserguide() {   
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelectorAll("#reception_pulse")[0],
          intro: "<div style='text-align:center'>For any queries Relating to the event please visit the Help desk.</div>"
        },
        {
          element: document.querySelectorAll("#aud")[0],
          intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
        },
        {
          element: document.querySelectorAll("#selfiepulse")[0],
          intro: "<div style='text-align:center'>Click a selfie to capture the memory for the event</div>"
        },
        // {
        //   element: document.querySelectorAll("#game_pulse")[0],
        //   intro: "<div style='text-align:center'>Go to Game Zone to play Games</div>"
        // },
        {
          element: document.querySelectorAll("#playAV")[0],
          intro: "<div style='text-align:center'>Click Here to play AV</div>"
        },
        // {
        //   element: document.querySelectorAll("#networking_pulse")[0],
        //   intro: "<div style='text-align:center'>Click here to chat with Leadership Lounge</div>"
        // },
        {
          element: document.querySelectorAll("#heighlight_pulse")[0],
          intro: "<div style='text-align:center'>The agenda for the event can be viewed in agenda section.</div>"
        },
        {
          element: document.querySelectorAll("#heighlight_pulseright")[0],
          intro: "<div style='text-align:center'>The agenda for the event can be viewed in agenda section.</div>"
        },
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");
    

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  fullscreen() {
    var elem: any = document.getElementById("video");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
      elem.msRequestFullscreen();
    }
  }
  ngOnDestroy() {
    // if (localStorage.getItem('user_guide') === 'start') {
    //   let stop = () => this.intro.exit();
    //   stop();
    // }
    localStorage.removeItem('user_guide');
  }


}
