import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-two',
  templateUrl: './auditorium-two.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumTwoComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_17';
  serdia_room = localStorage.getItem('serdia_room');

  videoEnd = false;
  videoPlayerTwo = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream1.smil/playlist.m3u8';
  //videoPlayer = '../assets/video/networking_lounge_video.mp4';

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    // this.chatGroupTwo();
  } 
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatTwo').modal('show')
    this.loadData();
  }

  loadData(): void {
    this.chatGroupTwo();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data => {
    //   if (data == 'group_chat') {
    //     this.chatGroupTwo();
    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 2){
          this.messageList.push(msgs);
        }        
        console.log('demo', this.messageList);
      });
  }
  chatGroupTwo() {
    this._fd.groupchatingtwo().subscribe(res => {
      console.log('res', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatTwo').modal('hide');
  }
  postMessageTwo(value) {
    // console.log(value);
    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(data.name);
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    // console.log(this.roomName);
    this.textMessage.reset();
    // this.chatGroupTwo();
    //this.newMessage.push(this.msgs);
  }
}
