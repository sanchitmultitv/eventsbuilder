import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  videoPlayer = 'https://d331fr94c21vyl.cloudfront.net/streamline/final_lutron_movie/playlist.m3u8';
  // videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session3.smil/playlist.m3u8';
  // videoPlayer = '../assets/video/networking_lounge_video.mp4';
  bgimgs = [
    '../../../../../assets/philips/morningStage.jpg',
    '../../../../../assets/philips/morningStage.jpg',
  ];
  bgImage;
  bgmorning: boolean;
  constructor(private chatService: ChatService, private _fd: FetchDataService, private router: Router) { }
  timer: any = 10;
  cleartimer;
  showtimer=false;
  ngOnInit(): void {
    this.getDatebg();
   
  }
  ngAfterViewInit() {
    this.chatService.getconnect('5fb4b1b07f9d8');
    this.chatService.getIframeConnection().subscribe(((data: any) => {
      console.log('test', data);
      let breakdata:any = data.split('*');
      let email:any = breakdata[0];
      let keyC:any = breakdata[1];
      localStorage.setItem('iframeKey', keyC);
      let emailCheck:any = JSON.parse(localStorage.getItem('virtual')).email;
      if ((email === emailCheck)&&(keyC !== "exit")) {
        // store(LKey, keyC);
        console.log('testinside', data);
        this.showtimer=true;
        this.startTimer();
      }
    }));
  }

  getDatebg() {
    let date: any = new Date().getHours();
    let hour: any = date;
    if ((hour >= 6) && (hour < 18)) {
      this.bgImage = this.bgimgs[0];
      this.bgmorning = true;
    } else {
      this.bgImage = this.bgimgs[1];
      this.bgmorning = false;
    }

  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_16';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-60');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.serdia_room).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  startTimer() {
    this.timer = 10;
    this.cleartimer = setInterval(() => {
      this.timer--;
      if (this.timer === 0) {
        this.showtimer=false;
        // this.timer=10;
        clearInterval(this.cleartimer);
        this.router.navigate(['/ifaudi']);
      }
    }, 1000);
  }
  ngOnDestroy() {
    // this.chatService.disconnect();
    clearInterval(this.cleartimer);
  }
  fullscreen() {
    var elem: any = document.getElementById("video");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
      elem.msRequestFullscreen();
    }
  }

}

