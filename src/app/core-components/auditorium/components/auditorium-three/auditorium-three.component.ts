import { Component, OnInit } from '@angular/core';
import { GroupChatThreeComponent } from '../../../../layout/group-chat-three/group-chat-three.component';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-three',
  templateUrl: './auditorium-three.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumThreeComponent implements OnInit {

  videoEnd = false;
  videoPlayerThree = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream2.smil/playlist.m3u8';

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit() {
    // this.chatGroup();
  }

  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatThree').modal('show')
    this.loadData();
  }

  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_18';
  serdia_room = localStorage.getItem('serdia_room');

  loadData(): void {
    this.chatGroup();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data => {
    //  if (data == 'group_chat') {

    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 3){
          this.messageList.push(msgs);
        }        
        console.log('demo', this.messageList);
      });
  }
  chatGroup() {
    this._fd.groupchatingthree().subscribe(res => {
      console.log('three', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatThree').modal('hide');
  }
  postMessageThree(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
}


