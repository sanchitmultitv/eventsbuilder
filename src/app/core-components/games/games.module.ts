import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GamesRoutingModule } from './games-routing.module';
import { GamesComponent } from './games/games.component';
import { PuzzleComponent } from './components/puzzle/puzzle.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ShootingComponent } from './components/shooting/shooting.component';


@NgModule({
  declarations: [GamesComponent, PuzzleComponent, SpinnerComponent, ShootingComponent],
  imports: [
    CommonModule,
    GamesRoutingModule
  ]
})
export class GamesModule { }
