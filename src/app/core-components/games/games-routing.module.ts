import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PuzzleComponent } from './components/puzzle/puzzle.component';
import { ShootingComponent } from './components/shooting/shooting.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { GamesComponent } from './games/games.component';


const routes: Routes = [
  {
    path: '', component: GamesComponent,
    children: [
      { path: 'puzzle', component: PuzzleComponent },
      { path: 'spinner', component: SpinnerComponent },
      { path: 'shooting', component: ShootingComponent },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesRoutingModule { }
