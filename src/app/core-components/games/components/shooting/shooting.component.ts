import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-shooting',
  templateUrl: './shooting.component.html',
  styleUrls: ['./shooting.component.scss']
})
export class ShootingComponent implements OnInit {

  score: any;
  startEnd = false;
  constructor() { }
  public sayHello = (name) => {
    console.log(name + ' says hello');
  };
  ngOnInit(): void {

    //Ref : http://www.html5rocks.com/en/tutorials/canvas/notearsgame/
    $(() => {
      let canvasElem: any = $(".myCanvas");
      let canvas: any = canvasElem.get(0).getContext("2d");
      let canvasWidth: any = canvasElem[0].width;
      let canvasHeight: any = canvasElem[0].height;
      let playerImg: any = $(".playerImg")[0];
      let enemyImg: any = $(".enemyImg")[0];
      let boomImg: any = $(".enemyBoomImg")[0];
      let playerBullets: any = [];
      let playerEnemys: any = [];
      let startTimer: any;
      let score: any = 0;
      let num: any = Number;
      num.prototype.clamp = function (min, max) { // http://stackoverflow.com/a/11409944/4733275
        return Math.min(Math.max(this, min), max);
      };
      let FPS = 31;

      let startGame = () => {
        startTimer = setInterval(function () {
          update();
          draw();
        }, 1000 / FPS);
      }

      let endGame = () => {
        clearInterval(startTimer);
        FPS = 0;
        this.score = score;
        score = 0;
        console.log("Your score is " + score);
        $(".scoreInput").val("");
        playerBullets = [];
        playerEnemys = [];
        player.img = playerImg;
        canvas.clearRect(0, 0, canvasWidth, canvasHeight);
      }

      $(".startBtn").click(() => {
        startGame();
      });

      $("body").bind({
        keydown: (e) => {
          let key = e.keyCode;
          player.update(key);
        }
      });

      function draw() {
        canvas.clearRect(0, 0, canvasWidth, canvasHeight);
        player.draw();
        playerBullets.forEach(function (bullet) {
          bullet.draw();
        });
        playerEnemys.forEach(function (enemy) {
          enemy.draw();
        });
      }
      let update = () => {
        $(".scoreInput").val(score);
        this.score = score;
        playerBullets.forEach(function (bullet, index, object) {
          bullet.y -= bullet.speed;
          if (bullet.y < 0 || bullet.active) {
            object.splice(index, 1);
          }
        });
        playerEnemys.forEach(function (enemy, index, object) {
          enemy.y += enemy.speed;
          if (enemy.y > canvasHeight || enemy.active) {
            score++;
            object.splice(index, 1);
          }
        });
        if (Math.random() < 0.02) {
          playerEnemys.push(enemy(event));
        }
        handleCollisions();
      }
      let player: any = {
        width: 30,
        height: 30,
        x: (canvasWidth / 2) - (20 / 2),
        y: canvasHeight - 30,
        color: "#0ff",
        img: playerImg,
        draw: function () {
          //canvas.fillStyle = this.color;
          //canvas.fillRect(this.x, this.y, this.width, this.height);
          canvas.drawImage(this.img, this.x, this.y, this.width, this.height);
        },
        update: function (key) {
          switch (key) {
            case 32:
              this.shoot();
              break;
            case 37:
              this.x -= 10;
              break;
            case 39:
              this.x += 10;
              break;
          }
          player.x = player.x.clamp(0, canvasWidth - player.width);
        },
        shoot: function () {
          let midX = player.x + (player.width / 2) - 2.5;
          let midY = player.y + 20;
          playerBullets.push(bullet({
            x: midX - 10,
            y: midY,
            speed: 3
          }),
            bullet({
              x: midX + 10,
              y: midY,
              speed: 3
            }));
        },
        explode: function () {
          this.img = boomImg;
          setTimeout(endGame, 200);
        }
      };
      function bullet(B) {
        B.width = 3;
        B.height = 3;
        B.color = "#000";
        B.draw = function () {
          canvas.fillStyle = this.color;
          canvas.fillRect(this.x, this.y, this.width, this.height);
        };
        B.explode = function () {
          setTimeout(function () {
            B.active = true;
          }, 100);
        };
        B.active = false;
        return B;
      }
      function enemy(E: any) {
        E = E || {};
        E.width = 20;
        E.height = 20;
        E.color = "#000";
        E.speed = 1;
        E.x = canvasWidth / 4 + Math.random() * canvasWidth / 2;
        E.y = 0;
        E.img = enemyImg;
        E.draw = function () {
          canvas.drawImage(this.img, this.x, this.y, this.width, this.height);
        };
        E.explode = function () {
          this.img = boomImg;
          setTimeout(function () {
            E.active = true;
          }, 200);
        };
        E.active = false;
        return E;
      }
      function collides(a, b) {
        return a.x < b.x + b.width &&
          a.x + a.width > b.x &&
          a.y < b.y + b.height &&
          a.y + a.height > b.y;
      }
      function handleCollisions() {
        playerBullets.forEach(function (bullet) {
          playerEnemys.forEach(function (enemy) {
            if (collides(bullet, enemy)) {
              bullet.explode();
              enemy.explode();
            }
          });
        });

        playerEnemys.forEach(function (enemy) {
          if (collides(enemy, player)) {
            enemy.explode();
            player.explode();
          }
        });
      }
    });
  }

}
