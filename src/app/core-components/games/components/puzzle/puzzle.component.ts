import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-puzzle',
  templateUrl: './puzzle.component.html',
  styleUrls: ['./puzzle.component.scss']
})
export class PuzzleComponent implements OnInit, OnDestroy {
  btns = [];
  secondsTime = 0;
  move = 0;
  winner = 'Play';
  checkbtns = [];
  timer;
  showPlay = 'Play';
  constructor() { }

  ngOnInit(): void {
    this.checkbtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
  }
  startReset() {
    this.btns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
    this.shuffle(this.btns);
    if (this.showPlay === 'Play') {
      this.showPlay = 'Reset';
      this.timer = setInterval(() => {
        this.secondsTime = this.secondsTime + 1;
      }, 1000);
    } else {
      this.showPlay = 'Play';
      this.secondsTime = 0;
      this.move = 0;
      clearInterval(this.timer);
    }
  }
  shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  btnarr = [];

  puzzleup(index, btn) {
    this.winner = 'Play';
    let zeroIndex = this.btns.indexOf(0);
    if ((zeroIndex === (index + 1)) || (zeroIndex === (index + 4)) || (zeroIndex === (index - 1)) || (zeroIndex === (index - 4))) {
      this.btns[this.btns.indexOf(0)] = btn;
      this.btns[index] = 0;
      this.move = this.move + 1;
    }
    this.btnarr = this.btns.filter(e=>this.checkbtns.indexOf(e)===this.btns.indexOf(e));
    console.log('testing', this.btnarr)
    if(this.btnarr.length === 14){
      this.winner = 'You Won!';
    }
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }
}
