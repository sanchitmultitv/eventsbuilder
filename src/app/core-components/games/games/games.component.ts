import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {
  links = [
    { 'link': 'wheel', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/wheel.jpg', 'name':'Wheel' },
    { 'link': 'puzzle', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/puzzle.webp', 'name':'Puzzle' },
    { 'link': 'eggBucket', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/eggCatch.PNG', 'name':'Egg bucket' },
    { 'link': 'spinner', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/spinner.png', 'name':'Spinner' },
    { 'link': 'bubble', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/bubbleShooter.jpg', 'name':'Bubble' },
    { 'link': 'collectSquare', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/collectSquare.jpeg', 'name':'Collect square' },
    { 'link': 'tictac', 'img': 'https://starmyanmar.multitvsolution.com/assets/icons/games_thumb/Galaxy_Sky_Shooting.png', 'name':'Shoot plane' },];
  showLinks = false;
  selectedLink;
  constructor(public router: Router) { }

  ngOnInit(): void {
   
  }
  
  gotoGame(link) {
    this.showLinks = true;
    this.selectedLink = link;
    this.router.navigateByUrl('/games/' + link);
  }
}
