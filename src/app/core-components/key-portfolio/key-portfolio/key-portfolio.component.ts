import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-key-portfolio',
  templateUrl: './key-portfolio.component.html',
  styleUrls: ['./key-portfolio.component.scss']
})
export class KeyPortfolioComponent implements OnInit {
  player;
  videoName;
  liveMsg=false;
  constructor(private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
      if(data == 'start_live'){
        this.liveMsg = true;
      }
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
     
    }));
    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime=0;
        pauseVideo.pause();
        console.log('event', event);
      }
    }
  }
  playDiabetesVideo() {
    $('#playVideo').modal('show');
    this.player = '../../../../assets/serdia_myanmar/videos/key_portfolio/Diabetes.mp4';
    this.videoName = 'Diabetes';
  }
  playCardiologyVideo() {
    $('#playVideo').modal('show');
    this.player = '../../../../assets/serdia_myanmar/videos/key_portfolio/Cardiology.mp4';
    this.videoName = 'Cardiology';
  }
  playHypertensionVideo() {
    $('#playVideo').modal('show');
    this.player = '../../../../assets/serdia_myanmar/videos/key_portfolio/Hypertension.mp4';
    this.videoName = 'Hypertension';
  }
  playVenousDiseaseVideo() {
    $('#playVideo').modal('show');
    this.player = '../../../../assets/serdia_myanmar/videos/key_portfolio/Venous disease.mp4';
    this.videoName = 'VenousDisease';
  }
  closePopup() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
}
